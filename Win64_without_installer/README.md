# Sylvaccess #
## Utilisation sans droit d'administrateur / Use without admin rights ##

&nbsp;  

![Logo_FR](../Source_code/FR.png?raw=true)      
Pour utiliser Sylvaccess sans les droits administrateur il suffit de dézipper "Sylvaccess_vXX.zip" à l'emplacement voulu sur son PC.  

Ensuite il faut double cliquer sur l'executable "Sylvaccess_XX.exe" au sein du dossier dézippé et le tour est joué.  

N.B : Pour se faciliter la vie on peut créer un raccourci bureau en faisant clic droit sur "Sylvaccess_XX.exe"  puis créer un raccourci.


&nbsp;  
------------------------------------

![Logo_EN](../Source_code/UK.png?raw=true)   

If you want to use Sylvaccess and do not have admin rights on your computer, you can unzip "Sylvaccess_vXX.zip" wherever you want in your computer.  

Then, double click on the exec file "Sylvaccess_XX.exe" present in the unzipped folder and you are done.  

Foe easy use you can create a direct shorcut on your desktop.  

&nbsp;  



