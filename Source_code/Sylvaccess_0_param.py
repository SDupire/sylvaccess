# -*- coding: utf8 -*-
"""
Software: Sylvaccess
File: 0_param.py
Copyright (C) Sylvain DUPIRE 2021
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@inrae.fr
Version: 3.5.1
Date: 2021/12/17
License :  GNU-GPL V3

-----------------------------------------------------------------------
Français: (For english see above)

Ce fichier fait partie de Sylvaccess qui est un programme informatique 
servant à cartographier automatiquement les forêts accessibles en fonction de 
différents modes d'exploitations (skidder, porteur, débardage par câble aérien). 

Ce logiciel est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
suivant les termes de la GNU General Public License telle que publiée par la 
Free Software Foundation ; soit la version 3 de la licence, soit (à votre gré) 
toute version ultérieure.
Sylvaccess est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE 
GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION 
à UN BUT PARTICULIER. Consultez la GNU General Public License pour plus de 
détails.
Vous devez avoir reçu une copie de la GNU General Public License en même temps 
que Sylvaccess ; si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.

-----------------------------------------------------------------------
English

This file is part of Sylvaccess which is a computer program whose purpose 
is to automatically map forest accessibility according to different forest 
operation systems (skidder, forwarder, cable yarding)..

Sylvaccess is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sylvaccess.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys,os,gc,math,datetime,time,traceback
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline
import scipy.integrate
from sylvaccess_fonctions import *
from Sylvaccess_1_skidder import *
from Sylvaccess_2_cable import *
from Sylvaccess_3_forwarder import *
import sylvaccess_cython3 as fc

##############################################################################
### General parameters
##############################################################################

### 1 pour lancer le script, 0 sinon

test_Skidder = 1           # Lancer skidder
test_Forwarder = 1         # Lancer Porteur
test_Cable = 0             # Lancer Cable
test_cable_optimise = 0    # Lancer selection meilleur ligne de cable

Pente_max_bucheron = 110   # Pente maximale pour le bucheronage manuel (%)

language = 'FR'

##############################################################################
### Spatial inputs
##############################################################################

# Espace de travail
Wspace = "H:/3_Modeles/2_Sylvaccess/Retour_bug_ONF/RE Sylvaccess/Cellier/1_Data_Source/"
# Dossier de resultat
Rspace = "H:/3_Modeles/2_Sylvaccess/Retour_bug_ONF/RE Sylvaccess/Cellier/res/"    # Dossier de resultat

# Couches SIG
file_MNT = 'H:/3_Modeles/2_Sylvaccess/Retour_bug_ONF/RE Sylvaccess/Cellier/1_Data_Source/Raster/MNT_5m_Alig.tif'    # MNT
file_shp_Desserte =  'H:/3_Modeles/2_Sylvaccess/Retour_bug_ONF/RE Sylvaccess/Cellier/1_Data_Source/Vecteur/Celliers_TRC.shp'    # Desserte
file_shp_Cable_dep = ""    # Depart de cable
file_shp_Foret = 'H:/3_Modeles/2_Sylvaccess/Retour_bug_ONF/RE Sylvaccess/Cellier/1_Data_Source/Vecteur/Celliers_FRT.shp'    # Foret
file_Vol_ha = 'H:/3_Modeles/2_Sylvaccess/Retour_bug_ONF/RE Sylvaccess/Cellier/1_Data_Source/Raster/Vprd_5m_Alig.tif'    # volume hectare
file_Vol_AM = ""    # volume arbre moyen
file_Htree = ""    # Hauteur du peuplement

# Dossiers avec couches obstacles
Dir_Full_Obs_skidder =  'H:/3_Modeles/2_Sylvaccess/Retour_bug_ONF/RE Sylvaccess/Cellier/1_Data_Source/Obstacles/'   # Skidder: trainage et debusquage impossible
Dir_Partial_Obs_skidder = ""   # Skidder: zone seul trainage impossible
Dir_Obs_cable = ""    #Cable
Dir_Obs_forwarder = ""     #Porteur

##############################################################################
### Skidder parameters
##############################################################################

Dtreuil_max_up = 50             #Distance treuillage en amont (m)
Dtreuil_max_down = 150          #Distance treuillage en aval (m)
Dmax_train_near_for = 50       #Distance trainage hors desserte et hors foret (m)
Pente_max_skidder = 30          #Pente maximale pour sortir de desserte (%)
Pmax_amont = 75                 #Pente pour laquel Dup = Dupmax (%)
Pmax_aval = 20                  #Pente pour laquel Down = Ddownmax (%)
Option_Skidder=1                #1: reste sur desserte 
#                               #2: s'approche au maximum du tronc 
Skid_Debclass = "0;250;500;1000;1500;2000"

##############################################################################
### Forwarder parameters
##############################################################################
Forw_angle_incl = 15            #Pente en travers max (%)
Forw_angle_up = 30              #Pente max en remontant les bois (%)
Forw_angle_down = 25            #Pente max en descendant les bois (%)
Forw_portee = 8                 #Portee du bras (m)
Forw_Lmax = 300                 #Distance max hors desserte (m)
Forw_Dmax_out_for = 200         #Distance max hors hors foret (m)
Forw_Debclass = "0;250;500;1000;1500;2000"
##############################################################################
### Cable parameters
##############################################################################
Cable_type = 1
# 0: Cable mat sur tracteur
# 1: Cable mat sur remorque
# 2: Cable mat sur camion
# 3: Cable long

Carriage_type = 0
# 0: Charriot classique
# 1: Charriot automoteur

precision = 1
# 1: tous les pixels et azimuts sont testes
# 2: tous les pixels et et 1 azimtut sur 2 sont testes
# 3: 1 pixel sur 2 et 1 azimut sur 2 sont testes

Lhor_max=40                     #Distance laterale de pechage (m)

if Cable_type==0:            
    Lmax=550                    #Longueur max du cable porteur (m)
    Lmin=150                    #Longueur min du cable porteur (m)
    Htower=8                    #Hauteur du mat (m)
    d=16.0                      #Diametre du cable porteur (mm)
    sup_max=2                   #Nombre max de supports intermediaires
    rupt_res=30000              #Tension de rupture du cbale porteur (kgF)
    q1=1.35                     #Masse lineique du cable porteur (kg/m)
elif Cable_type==1:
    Lmax=750
    Lmin=150
    Htower=10.5
    d=18.0
    sup_max=3
    rupt_res=35000
    q1=1.85
elif Cable_type==2:
    Lmax=1200
    Lmin=200
    Htower=14.0
    d=22.0
    sup_max=3
    rupt_res=48000
    q1=2.6
else:
    Lmax=1500
    Lmin=300
    Htower=8.0
    d=22.0
    sup_max=3
    rupt_res=48000
    q1=2.6
    Lhor_max=40

if Carriage_type==0:
    Pchar=400                    #Masse du chariot (kg)
    slope_grav = 15              #pente min pour descente par gravite (%)
    slope_Wliner_up = 15         #pente max pour automoteur en amont (%)
    slope_Wliner_down = 100      #pente max pour automoteur en aval(%)
else:
    Pchar=1200
    slope_grav = 15
    slope_Wliner_up = 15
    slope_Wliner_down = 100

Load_max = 2500         #Masse maximale suspendue (kg)
E = 100000              #Elasticite (N/mm²)
Hintsup = 12            #Hauteur des supports intermediaires (m)
Hend = 12               #Hauteur du mat terminal (m)
Hline_min = 3.5         #Hauteur minimale du cable en tout point (m)
Hline_max = 50          #Hauteur maximale du cable en tout point (m)
safe_fact = 2.5         #Facteur de securite
Max_angle = 30          #Angle max du cable entre deux support inter (°)
LminSpan = 50.          #Distance minimale entre deux pylone (m)  
q2 = 0.5                #Masse lineique du cable tracteur (kg/m) 
q3 = 0.5                #Masse lineique du cable retour (kg/m)   
coeff_frot = 0.15       #Angle de frottement (°)  
prelevement = 0.35      #Taux applique pour prelevement du volume hectare (%)
angle_transv = 60       #Angle minimin de la ligne par rapport a 
                        #la perpendiculaire de la direction de plus grande pente(°)
slope_trans= 30         #Pente max quand l'azimut de la ligne est quasi 
                        #perpendiculaire a la direction de plus grande pente [%]
Lslope = 75             #Distance max en devers [m]
PropSlope = 0.15        #Proportion de la longueur max en devers [m]

VariaH = 0              #0: Non --- 1: Oui

##############################################################################
### Parameters for automatic selection of cable lines
#############################################################################

if test_Cable and test_cable_optimise:    
    Dir_cable_sel = Wspace
    Cabsel_For = file_shp_Foret
else:
    #Pour faire tourner juste la selection de ligne, renseigner le dossier ou se 
    #trouve la base de donnee
    Dir_cable_sel = "/media/sylvain/Externe_Sylvain/3_Modeles/2_Sylvaccess/Retour_bug_ONF/0_PetitBornand/Petit Bornand/Resultats/Cable_3/"

#Pour faire tourner juste la selection de ligne, renseigner le dossier ou se 
#trouve la base de donnee
Cabsel_For = file_shp_Foret  #Modifier si different de simulation cable
Rast_cable_Vha = file_Vol_ha       #Modifier si different de simulation cable
Rast_cable_VAM = file_Vol_AM       #Modifier si different de simulation cable

cab_Lhor_max = Lhor_max            #Modifier si different de simulation cable

#Preciser si les caracteristiques forestieres des lignes doivent etre recalcules
# (par exemple si nouvelles couche foret/nouvelle couche volume)
new_calc=0 #0 non, 1 oui 

#nombre de critere a prendre en compte pour la selection


#Limites par critere
lim_Sfor =  0.4         # ha   - Surface de forêt impactee (maximiser)
lim_Nsup =  sup_max     #      - Nombre de support intermediaires (minimiser)
lim_Sens =  0           #      - Sens debardage a privilegier -1 Vers le bas, 1 Vers le Haut, 0 Peu importe
lim_Ltot =  100         # m    - Longueur de ligne (maximiser)
lim_Vtot =  100          # m3   - Volume total par ligne (maximiser)
lim_IPC =   0.5         # m3/m - Indice de prelevement câble (maximiser)
lim_VAM =   0           # m3   - Volume de l'arbre moyen (maximiser)
lim_Dchar = 2000        # m    - Longueur moyenne parcourue par le charriot (minimiser)
lim_Cost =  300         # €/m3 - Coût du debardage (minimiser) 

#Poids par critere
w_Sfor =  0       
w_Nsup =  2   
w_Sens =  0          
w_Ltot =  0        
w_Vtot =  4         
w_IPC =   3         
w_VAM =   0           
w_Dchar = 0        
w_Cost =  0    

w_list = [w_Sfor,w_Nsup,w_Sens,w_Ltot,w_Vtot,w_IPC,w_VAM,w_Dchar,w_Cost]
lim_list = [lim_Sfor,lim_Nsup,lim_Sens,lim_Ltot,lim_Vtot,lim_IPC,lim_VAM,lim_Dchar,lim_Cost]


##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################

##############################################################################
### Automatic verification
##############################################################################

#Add '/' add the end of directories path if they are not present 
if Wspace[-1]!='/':Wspace+="/"
if Rspace[-1]!='/':Rspace+="/"
if Dir_cable_sel[-1]!='/':Dir_cable_sel+="/"
if  Dir_Obs_cable!='':
    if Dir_Obs_cable[-1]!='/' : Dir_Obs_cable+="/"
if Dir_Partial_Obs_skidder!='':
    if Dir_Partial_Obs_skidder[-1]!='/' : Dir_Partial_Obs_skidder+="/"
if Dir_Full_Obs_skidder!='':
    if Dir_Full_Obs_skidder[-1]!='/' : Dir_Full_Obs_skidder+="/"
if Dir_Obs_forwarder!='':
    if Dir_Obs_forwarder[-1]!='/' : Dir_Obs_forwarder+="/"

try:os.mkdir(Rspace)
except:pass 
##############################################################################
### Run the Sylvaccess submodules
##############################################################################
try:
    #Check raster entries before processing
    test,mess = check_files(file_MNT,file_shp_Desserte,file_shp_Foret,file_Vol_ha,file_Vol_AM,file_Htree,language,
                                    test_Cable,test_Skidder,test_Forwarder,test_cable_optimise,new_calc,Cabsel_For,
                                    file_shp_Cable_dep)

    if not test:
        # There is a problem with spatial inputs
        print(mess)
    else:  
        #Run Sylvaccess 
        if (test_Skidder + test_Forwarder) > 0:
            # Verifie si une partie de la desserte correspond a un projet
            testExist = check_field_EXIST(file_shp_Desserte,"EXIST") 
            
            ###################################################################################################################
            ### Si pas de projet desserte
            ###################################################################################################################
            if not testExist: 
                if test_Skidder:  
                    process_skidder(Wspace,Rspace,file_MNT,Dtreuil_max_up,Dtreuil_max_down,
                                    Dmax_train_near_for,Pente_max_skidder,Pmax_amont,Pmax_aval,
                                    Option_Skidder,Pente_max_bucheron,Dir_Full_Obs_skidder,
                                    Dir_Partial_Obs_skidder,language,file_shp_Desserte,file_shp_Foret,
                                    file_Vol_ha,Skid_Debclass)                    
                    gc.collect()
                
                if test_Forwarder:
                    process_forwarder(Wspace,Rspace,file_MNT,Pente_max_bucheron,Forw_angle_incl,Forw_angle_up,
                                      Forw_angle_down,Forw_portee,language,file_shp_Desserte,file_shp_Foret,
                                      Dir_Obs_forwarder,Forw_Lmax,Forw_Dmax_out_for, file_Vol_ha,Forw_Debclass)
                    gc.collect()
                    
            ###################################################################################################################
            ### Si projet desserte
            ###################################################################################################################
            else:                        
                file_shp_Desserte_Exist = create_new_road_network(file_shp_Desserte,Wspace)
                
                #Premiere simu sans projet
                if language=="FR":
                    print("\nSIMULATION DEPUIS LA DESSERTE EXISTANTE")
                else:
                    print("\nSIMULATION FROM EXISTING ROAD NETWORK")
                if test_Skidder:   
                    try:os.mkdir(Rspace+"Skidder/")
                    except:pass
                    process_skidder(Wspace,Rspace+"Skidder/",file_MNT,Dtreuil_max_up,Dtreuil_max_down,
                                    Dmax_train_near_for,Pente_max_skidder,Pmax_amont,Pmax_aval,
                                    Option_Skidder,Pente_max_bucheron,Dir_Full_Obs_skidder,
                                    Dir_Partial_Obs_skidder,language,file_shp_Desserte_Exist,file_shp_Foret,
                                    file_Vol_ha,Skid_Debclass)                    
                    gc.collect()
                    if language=="FR":
                        projdir = Rspace+"Skidder/1_Existant/"
                    else:
                        projdir = Rspace+"Skidder/1_Existing/"
                    
                    os.rename(Rspace+"Skidder/Skidder/",projdir)
                
                if test_Forwarder:
                    if language=='EN':
                        Rspace_f = Rspace+"Forwarder/"
                        old=Rspace+"Forwarder/Forwarder/"
                        new= Rspace+"Forwarder/1_Existing/"
                    else:
                        Rspace_f = Rspace+"Porteur/"
                        old=Rspace+"Porteur/Porteur/"
                        new= Rspace+"Porteur/1_Existant/"  
                    try:os.mkdir(Rspace_f)
                    except:pass
                    process_forwarder(Wspace,Rspace_f,file_MNT,Pente_max_bucheron,Forw_angle_incl,Forw_angle_up,
                                      Forw_angle_down,Forw_portee,language,file_shp_Desserte_Exist,file_shp_Foret,
                                      Dir_Obs_forwarder,Forw_Lmax,Forw_Dmax_out_for,file_Vol_ha,Forw_Debclass)
                    gc.collect()
                    os.rename(old,new)  
                    
                os.remove(Wspace+"Temp/Lien_piste.npy")
                os.remove(Wspace+"Temp/Lien_RF.npy")
                os.remove(Wspace+"Temp/Tab_res_pub.npy")
                os.remove(Wspace+"Temp/Route_for.npy")
                os.remove(Wspace+"Temp/Piste.npy")
                
                #Deuxieme simu avec projet
                if language=="FR":
                    print("\nSIMULATION INCLUANT LE PROJET DE DESSERTE")
                else:
                    print("\nSIMULATION INCLUDING PROJECT OF ROADS")
                if test_Skidder: 
                    process_skidder(Wspace,Rspace+"Skidder/",file_MNT,Dtreuil_max_up,Dtreuil_max_down,
                                    Dmax_train_near_for,Pente_max_skidder,Pmax_amont,Pmax_aval,
                                    Option_Skidder,Pente_max_bucheron,Dir_Full_Obs_skidder,
                                    Dir_Partial_Obs_skidder,language,file_shp_Desserte,file_shp_Foret,
                                    file_Vol_ha,Skid_Debclass)                    
                    gc.collect()
                    if language=="FR":
                        projdir = Rspace+"Skidder/2_Projet/"
                    else:
                        projdir = Rspace+"Skidder/2_Project/"
                    
                    os.rename(Rspace+"Skidder/Skidder/",projdir)
                    make_dif_files(language,Rspace,0)     
                    
                if test_Forwarder:
                    if language=='EN':
                        Rspace_f = Rspace+"Forwarder/"
                        old=Rspace+"Forwarder/Forwarder/"
                        new= Rspace+"Forwarder/2_Project/"
                    else:
                        Rspace_f = Rspace+"Porteur/"
                        old=Rspace+"Porteur/Porteur/"
                        new= Rspace+"Porteur/2_Projet/"                                
                    process_forwarder(Wspace,Rspace_f,file_MNT,Pente_max_bucheron,Forw_angle_incl,Forw_angle_up,
                                      Forw_angle_down,Forw_portee,language,file_shp_Desserte,file_shp_Foret,
                                      Dir_Obs_forwarder,Forw_Lmax,Forw_Dmax_out_for,file_Vol_ha,Forw_Debclass)
                    gc.collect()         
                    os.rename(old,new)   
                    make_dif_files(language,Rspace,1)    
                    
                
        if test_Cable:
            process_cable(Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Cable_dep,Dir_Obs_cable,
                          file_Vol_ha,file_Vol_AM,file_Htree,Pente_max_bucheron,Lmax,Lmin,LminSpan,
                          Htower,Hintsup,Hend,Lhor_max,Hline_min,Hline_max,sup_max,Carriage_type,
                          Cable_type,slope_grav,Pchar,slope_Wliner_up,slope_Wliner_down,q1,rupt_res,
                          safe_fact,E,d,Load_max,q2,q3,Max_angle,coeff_frot,language,
                          precision,prelevement,slope_trans,angle_transv,test_cable_optimise,
                          w_list,lim_list,VariaH,Lslope,PropSlope)
                          
            test_cable_optimise=0
            gc.collect()
        if test_cable_optimise:
            line_selection(Dir_cable_sel,w_list,lim_list,new_calc,Cabsel_For,Cabsel_Vha,Cabsel_Vam,
                           language,Cabsel_Lhor_max,prelevement,Pente_max_bucheron) 
             
            gc.collect()
        try:
            shutil.rmtree(Wspace+"Temp")
        except:
            pass
except Exception:    
    log = open(Rspace + 'error_log.txt', 'w')
    traceback.print_exc(file=log)
    log.close()  
#    try:
#        shutil.rmtree(Wspace+"Temp")
#    except:
#        pass

