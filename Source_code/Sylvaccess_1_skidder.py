# -*- coding: utf8 -*-
"""
Software: Sylvaccess
File: Sylvaccess_1_skidder.py
Copyright (C) Sylvain DUPIRE 2021
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@inrae.fr
Version: 3.5.1
Date: 2021/12/17
License :  GNU-GPL V3

-----------------------------------------------------------------------
Français: (For english see above)

Ce fichier fait partie de Sylvaccess qui est un programme informatique 
servant à cartographier automatiquement les forêts accessibles en fonction de 
différents modes d'exploitations (skidder, porteur, débardage par câble aérien). 

Ce logiciel est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
suivant les termes de la GNU General Public License telle que publiée par la 
Free Software Foundation ; soit la version 3 de la licence, soit (à votre gré) 
toute version ultérieure.
Sylvaccess est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE 
GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION 
à UN BUT PARTICULIER. Consultez la GNU General Public License pour plus de 
détails.
Vous devez avoir reçu une copie de la GNU General Public License en même temps 
que Sylvaccess ; si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.

-----------------------------------------------------------------------
English

This file is part of Sylvaccess which is a computer program whose purpose 
is to automatically map forest accessibility according to different forest 
operation systems (skidder, forwarder, cable yarding)..

Sylvaccess is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sylvaccess.  If not, see <https://www.gnu.org/licenses/>.
"""

import os,math,datetime,sys
import numpy as np
from sylvaccess_fonctions import *
import sylvaccess_cython3 as fc
import gc

def prepa_data(Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Desserte,Dir_Full_Obs_skidder,Dir_Partial_Obs_skidder,language):
    print("")
    if language=='EN':
        print("Pre-processing of the inputs for skidder model")
    else:
        print("Preparation des entrees pour le modele skidder")
    ### Make directory for temporary files
    Dir_temp = Wspace+"Temp/"
    try:os.mkdir(Dir_temp)
    except:pass 
    Rspace_s = Rspace+"Skidder/"
    try:os.mkdir(Rspace_s)
    except:pass
    ##############################################################################################################################################
    ### Initialization
    ##############################################################################################################################################
    names,values,proj,Extent = raster_get_info(file_MNT)
    Csize,ncols,nrows = values[4],int(values[0]),int(values[1])  
    road_network_proj=get_proj_from_road_network(file_shp_Desserte)
    ##############################################################################################################################################
    ### Forest : shapefile to raster
    ##############################################################################################################################################
    Foret = shapefile_to_np_array(file_shp_Foret,Extent,Csize,"FORET")
    np.save(Dir_temp+"Foret",np.int8(Foret))    
    del Foret
    gc.collect()
    if language=='EN':
        print("    - Forest raster processed")
    else:
        print("    - Raster de foret cree")
        
    ##############################################################################################################################################
    ### Calculation of a slope raster and a cost raster of slope
    ##############################################################################################################################################
    # Slope raster
    MNT,Extent,Csize,proj = load_float_raster(file_MNT,Dir_temp)
    np.save(Dir_temp+"MNT",np.float32(MNT))
    Pente = fc.pente(MNT,Csize,-9999)
    np.save(Dir_temp+"Pente",np.float32(Pente))    
    # Cost raster of slope
    Pond_pente = np.sqrt((Pente*0.01*Csize)**2+Csize**2)/float(Csize)
    Pond_pente[Pente==-9999] = 10000
    np.save(Dir_temp+"Pond_pente",np.float32(Pond_pente))
    # Report a success message   
    del Pente,MNT
    gc.collect()
    if language=='EN':        
        print("    - Slope raster processed")  
    else:
        print("    - Raster de pente cree")  
    ##############################################################################################################################################
    ### Road network processing
    ##############################################################################################################################################
    Res_pub,Route_for,Piste= create_arrays_from_roads(file_shp_Desserte,Extent,Csize)
    np.save(Dir_temp+"Res_pub",Res_pub)  
    ##############################################################################################################################################
    ### Forest road network processing
    ##############################################################################################################################################
    pixels = np.argwhere(Res_pub==1) 
    # Give an identifiant to each public network pixel    
    ID = 1    
    Tab_res_pub = np.zeros((pixels.shape[0]+1,2),dtype=np.int32) 
    for pixel in pixels:
        Tab_res_pub[ID,0],Tab_res_pub[ID,1]=pixel[0],pixel[1]
        ID +=1         
    np.save(Dir_temp+"Tab_res_pub",Tab_res_pub)
    pixels = np.argwhere(Route_for==1)
    #num_ligne = id_RF, Y, X, Dtransp,Lien_Respub
    Lien_RF = np.zeros((pixels.shape[0]+1,5),dtype=np.float32)     
    ID = 1
    for pixel in pixels:
        Lien_RF[ID,0],Lien_RF[ID,1]=pixel[0],pixel[1]
        Lien_RF[ID,3]=-9999
        if Pond_pente[pixel[0],pixel[1]]==10000:
            Lien_RF[ID,2]=-9999
        else:
            Lien_RF[ID,2]=100001
        ID +=1 
    # Link RF with res_pub and calculate transportation distance
    Lien_RF=fc.Link_RF_res_pub(Tab_res_pub,Pond_pente,Route_for,Res_pub, Lien_RF,Csize) 
    Lien_RF[:,2]=np.int_(Lien_RF[:,2]+0.5)
    Lien_RF=Lien_RF.astype('int')
    Temp = (Lien_RF[:,3]>0)*(Lien_RF[:,2]==0)
    Lien_RF=Lien_RF[Temp==0]    
    np.save(Dir_temp+"Lien_RF",Lien_RF)
    # Check if all Forest road are linked to public network    
    if np.max(Lien_RF[:,2])==100001:
        RF_bad = np.zeros((nrows,ncols),dtype=np.int8)
        pixels = np.argwhere(Lien_RF[:,2]==100001)
        for pixel in pixels:
            ind = pixel[0]            
            RF_bad[Lien_RF[ind,0],Lien_RF[ind,1]]=1        
        if language=='EN':  
            ArrayToGtiff(RF_bad,Rspace_s+'Forest_road_not_connected',Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')
            print("    - Some forest road are not connected to public network. To see where, check raster "+Rspace_s+"Forest_road_not_connected.tif")
        else:
            ArrayToGtiff(RF_bad,Rspace_s+'Routes_forestieres_non_connectees',Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')
            print("    - Certaines routes forestieres ne sont pas connectees au reseau public. Pour voir ou elles se trouvent, ouvrir le raster "+Rspace_s+"Routes_forestieres_non_connectees.tif")
    else:
        if language=='EN':    
            print("    - Forest road processed") 
        else:
            print("    - Routes forestieres traitees") 
            
    ##############################################################################################################################################
    ### Forest tracks network processing
    ##############################################################################################################################################
    # Tracks
    pixels = np.argwhere(Piste==1)
    #num_ligne = id_piste, Y, X, Dpiste,Dtransp,Lien_RF, Lien_Respub,
    Lien_piste = np.zeros((pixels.shape[0]+1,7),dtype=np.float32)    
    ID = 1
    for pixel in pixels:
        Lien_piste[ID,0],Lien_piste[ID,1]=pixel[0],pixel[1]
        Lien_piste[ID,4]=-9999
        if Pond_pente[pixel[0],pixel[1]]==10000:
            Lien_piste[ID,2]=-9999
        else:
            Lien_piste[ID,2]=100001
        ID +=1
    Lien_piste=fc.Link_tracks_res_pub(Tab_res_pub,Lien_RF,Pond_pente,Piste,Route_for,Res_pub,Lien_piste,Csize)
    Lien_piste[:,2]=np.int_(Lien_piste[:,2]+0.5)
    Lien_piste=Lien_piste.astype('int')
    Temp = (Lien_piste[:,5]>0)*(Lien_piste[:,2]==0)
    Lien_piste=Lien_piste[Temp==0]    
    ind = np.lexsort((Lien_piste[:,1],Lien_piste[:,2]))
    Lien_piste=Lien_piste[ind]
    np.save(Dir_temp+"Lien_piste",Lien_piste) 
    if np.max(Lien_piste[:,2])==100001:
        RF_bad = np.zeros((nrows,ncols),dtype=np.int8)
        pixels = np.argwhere(Lien_piste[:,2]==100001)
        for pixel in pixels:
            ind = pixel[0]            
            RF_bad[Lien_piste[ind,0],Lien_piste[ind,1]]=1   
            Piste[Lien_piste[ind,0],Lien_piste[ind,1]]=0
        if language=='EN':    
            ArrayToGtiff(RF_bad,Rspace_s+'Forest_tracks_not_connected',Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')
            print("    - Some forest tracks are not connected to public network or forest road.")
            print("      To see where, check raster "+Rspace_s+"Forest_tracks_not_connected.tif")
            print("      These linears will be removed from the analysis.")
        else:
            ArrayToGtiff(RF_bad,Rspace_s+'Pistes_non_connectees',Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')
            print("    - Certaines pistes forestieres ne sont pas connectees a des routes ou au reseau public.")
            print("      Pour voir ou elles se trouvent ouvrir le raster "+Rspace_s+"Pistes_non_connectees.tif")
            print("      Ces lineaires sont exclus de l'analyse.")
    else:
        if language=='EN': 
            print("    - Forest tracks processed")    
        else:
            print("    - Pistes forestieres traitees")  
    Route_for[Res_pub==1]=0
    Piste[Res_pub==1]=0
    np.save(Dir_temp+"Route_for",Route_for) 
    np.save(Dir_temp+"Piste",np.int8(Piste))
    del Tab_res_pub,Lien_RF,Lien_piste,Res_pub  
    gc.collect()     
    ##############################################################################################################################################
    ### Create a raster of total obstacle for skidder
    ##############################################################################################################################################
    if Dir_Full_Obs_skidder!="":
        Full_Obstacles_skidder = prepa_obstacle_skidder(Dir_Full_Obs_skidder,Extent,Csize,ncols,nrows,((Route_for>0)*1+(Piste>0)*1))
    else:
        Full_Obstacles_skidder = np.zeros((nrows,ncols),dtype=np.int8)
    np.save(Dir_temp+"Full_Obstacles_skidder",np.int8(Full_Obstacles_skidder))  
    if language=='EN':      
        print("    - Skidder total obstacle raster processed")
    else:
        print("    - Raster d'obstacles complet cree")
    ##############################################################################################################################################
    ### Create a raster of partial obstacle for skidder
    ##############################################################################################################################################
    if Dir_Partial_Obs_skidder!="":
        Partial_Obstacles_skidder = prepa_obstacle_skidder(Dir_Partial_Obs_skidder,Extent,Csize,ncols,nrows,((Route_for>0)*1+(Piste>0)*1))        
    else:
        Partial_Obstacles_skidder = np.zeros((nrows,ncols),dtype=np.int8)
    np.save(Dir_temp+"Partial_Obstacles_skidder",np.int8(Partial_Obstacles_skidder))  
    if language=='EN':        
        print("    - Skidder partial obstacle raster processed")
        print("Input data processing achieved")
    else:
        print("    - Raster d'obstacles partiels cree")
        print("Pre-traitement des donnees d'entree termine")
    ##############################################################################################################################################
    ### Close the script
    ##############################################################################################################################################
    clear_big_nparray()
    gc.collect()


def process_skidder(Wspace,Rspace,file_MNT,Dtreuil_max_up,Dtreuil_max_down,Dmax_train_near_for,
                    Pente_max_skidder,Pmax_amont,Pmax_aval,Option_Skidder,Pente_max_bucheron,
                    Dir_Full_Obs_skidder,Dir_Partial_Obs_skidder,language,file_shp_Desserte,file_shp_Foret,
                    file_Vol_ha,Skid_Debclass):
    print("")
    if language=='EN':  
        print("Sylvaccess skidder starts")
    else:
        print("Debut du modele skidder")
    
    Hdebut = datetime.datetime.now()
    
    # Create a folder for process results
    Rspace_s = Rspace+"Skidder/"
    try:os.mkdir(Rspace_s)
    except:pass
    Dir_temp = Wspace+"Temp/"
    
    # Check if temporary files have been generated and have the same extent
    try:
        names,values,proj,Extent = raster_get_info(file_MNT)
    except:
        if language=='EN':  
            return "Error: please define a projection for the DTM raster"
        else:
            return "Erreur: veuillez definir une projection pour le raster MNT"
    try: 
        n,v1=read_info(Dir_temp+'info_extent.txt')
        for i,item in enumerate(values):
            if v1[i]!=round(item,2):
                prepa_data(Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Desserte,Dir_Full_Obs_skidder,Dir_Partial_Obs_skidder,language)
            if i+1>4:break
    except:
        prepa_data(Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Desserte,Dir_Full_Obs_skidder,Dir_Partial_Obs_skidder,language)
    
    # Inputs
    try:
        Foret = np.int8(np.load(Dir_temp+"Foret.npy"))
        Piste = np.int8(np.load(Dir_temp+"Piste.npy"))
        Route_for = np.int8(np.load(Dir_temp+"Route_for.npy"))    
        Res_pub = np.int8(np.load(Dir_temp+"Res_pub.npy"))    
        Lien_piste = np.load(Dir_temp+"Lien_piste.npy")
        Lien_RF = np.load(Dir_temp+"Lien_RF.npy")
        Pente = np.load(Dir_temp+"Pente.npy")
        Pond_pente = np.load(Dir_temp+"Pond_pente.npy")
        MNT = np.load(Dir_temp+"MNT.npy")
        Full_Obstacles_skidder = np.int8(np.load(Dir_temp+"Full_Obstacles_skidder.npy"))
        Partial_Obstacles_skidder = np.int8(np.load(Dir_temp+"Partial_Obstacles_skidder.npy"))
    except: 
        prepa_data(Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Desserte,Dir_Full_Obs_skidder,Dir_Partial_Obs_skidder,language)
        Foret = np.int8(np.load(Dir_temp+"Foret.npy"))
        Piste = np.int8(np.load(Dir_temp+"Piste.npy"))
        Route_for = np.int8(np.load(Dir_temp+"Route_for.npy"))   
        Res_pub = np.int8(np.load(Dir_temp+"Res_pub.npy")) 
        Lien_piste = np.load(Dir_temp+"Lien_piste.npy")
        Lien_RF = np.load(Dir_temp+"Lien_RF.npy")
        Pente = np.load(Dir_temp+"Pente.npy")
        Pond_pente = np.load(Dir_temp+"Pond_pente.npy")
        MNT = np.load(Dir_temp+"MNT.npy")
        Full_Obstacles_skidder = np.int8(np.load(Dir_temp+"Full_Obstacles_skidder.npy"))
        Partial_Obstacles_skidder = np.int8(np.load(Dir_temp+"Partial_Obstacles_skidder.npy"))
    
    nrows,ncols = MNT.shape[0],MNT.shape[1]
    road_network_proj=get_proj_from_road_network(file_shp_Desserte)
    
    # Calculation of useful variable for the model process
    Pmax_up = float(abs(Pmax_amont))/100.0
    Pmax_down = -float(abs(Pmax_aval))/100.0
    deniv_up = math.sqrt(float(Dtreuil_max_up*Dtreuil_max_up)/float(1+1.0/float(Pmax_up*Pmax_up)))
    deniv_down = -math.sqrt(float(Dtreuil_max_down*Dtreuil_max_down)/float(1+1.0/float(Pmax_down*Pmax_down)))
    coeff = float(Dtreuil_max_up-Dtreuil_max_down)/float(deniv_up-deniv_down)
    orig = Dtreuil_max_up - coeff*deniv_up
    Csize = values[4]
    Pond_pente[Full_Obstacles_skidder==1] = 1000
    Pente_max = fc.focal_stat_max(np.float_(Pente),-9999,1)
    Pente_ok_buch = np.int8((Pente_max<=Pente_max_bucheron))
    del Pente_max
    gc.collect()
    Pente_ok_skid = np.int8((Pente <= Pente_max_skidder)*(Pente > -9999))
    MNT_OK = np.int8((MNT!=values[5]))
    Zone_OK = np.int8(MNT_OK*(Foret==1)*(Full_Obstacles_skidder==0)*Pente_ok_buch==1)
    
    Surf_foret = np.sum((Foret==1)*MNT_OK)*Csize*Csize*0.0001
    Surf_foret_non_access = int(np.sum((Pente_ok_buch==0)*(Foret==1)*MNT_OK*Csize*Csize*0.0001)+0.5)
    
    Row_line,Col_line,D_line,Nbpix_line=create_buffer_skidder(Csize,Dtreuil_max_up,Dtreuil_max_down)
            
    if file_Vol_ha != "":
        Vol_ha = load_float_raster_simple(file_Vol_ha)
        Vol_ha[np.isnan(Vol_ha)]=0
        Temp = ((Vol_ha>0)*(Foret==1)*MNT_OK)>0
        Vtot = np.mean(Vol_ha[Temp])*np.sum(Temp)*Csize*Csize*0.0001
        Temp = ((Vol_ha>0)*(Pente_ok_buch==0)*(Foret==1)*MNT_OK)>0
        Vtot_non_buch = np.mean(Vol_ha[Temp])*np.sum(Temp)*Csize*Csize*0.0001
        del Vol_ha,Temp
    else:
        Vtot=0    
        Vtot_non_buch =0
       
    if language == 'FR':
        ArrayToGtiff(Pente_ok_buch,Rspace_s+'Pente_ok_buch',Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')   
        print("    - Initialisation terminee")  
    else:
        ArrayToGtiff(Pente_ok_buch,Rspace_s+'Manual_harvesting',Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')
        print("    - Initialization achieved")  
        
    del Pente,Pente_ok_buch
    gc.collect()     
    
    ###############################################################################################################################################    
    ### Calculation of skidding distance inside the forest stands
    ###############################################################################################################################################                  
    # Identify the forest area that may be run through by the skidder
    zone_rast = Pente_ok_skid*(Foret==1)
    zone_rast[Full_Obstacles_skidder==1]=0
    zone_rast[Partial_Obstacles_skidder==1]=0
    zone_rast[Res_pub==1]=0   
    zone_rast[MNT_OK==0]=0   
    from_rast = np.int8(((Piste==1)+(Route_for==1))>0)
    from_rast[Res_pub==1]=0
    Zone_for,Out_alloc = fc.calcul_distance_de_cout(from_rast,Pond_pente,zone_rast,Csize) 
    Zone_for[Zone_for>=0]=1
    Zone_for[from_rast==1]=1
    Zone_for=np.int8(Zone_for)
    
    # Create a buffer of Dmax_out_forest around these area taking into account slope and obstacles
    from_rast = fc.focal_stat_nb(np.float_(Zone_for==1),0,1)
    from_rast = np.int8((from_rast<9)*(from_rast>0))
    zone_rast = np.copy(Pente_ok_skid)
    zone_rast[Full_Obstacles_skidder==1]=0
    zone_rast[Partial_Obstacles_skidder==1]=0
    zone_rast[Res_pub==1]=0   
    zone_rast[MNT_OK==0]=0 
    Zone_for2,Out_alloc = fc.calcul_distance_de_cout(from_rast,Pond_pente,zone_rast,Csize,Dmax_train_near_for) 
    Pente_ok_skidder = np.int8(Zone_for2>0)
    Pente_ok_skidder[Zone_for==1]=1 
    
    del Zone_for,Zone_for2,Out_alloc
    gc.collect()
    
    #Stick all forest with pente_ok_skidder to the area
    from_rast = fc.focal_stat_nb(np.float_(Pente_ok_skidder==1),0,1)
    from_rast = np.int8((from_rast<9)*(from_rast>0))
    zone_rast = np.int8(1*Pente_ok_skid*(Foret==1))
    zone_rast[Full_Obstacles_skidder==1]=0
    zone_rast[Partial_Obstacles_skidder==1]=0
    zone_rast[Res_pub==1]=0   
    zone_rast[MNT_OK==0]=0   
    Zone_for,Out_alloc = fc.calcul_distance_de_cout(from_rast,Pond_pente,zone_rast,Csize) 
    Pente_ok_skidder[Zone_for>=0]=1    
           
    del Zone_for,from_rast,zone_rast,Out_alloc,Pente_ok_skid
    gc.collect()     
    
    
    D_foret_piste,L_Piste,D_piste=fc.Dfwd_flat_forest_tracks(Lien_piste, Pond_pente, Pente_ok_skidder*(Route_for==0)*1, Csize)    
    D_foret_RF,L_RF = fc.Dfwd_flat_forest_road(Lien_RF,Pond_pente,Pente_ok_skidder*1*(Piste==0),Csize)
    
    del Pente_ok_skidder,Pond_pente
    gc.collect() 
      
    if language == 'FR':        
        print("    - Distance de trainage depuis la desserte forestieres calculee")  
    else:
        print("    - Skidding distance from forest roads network calculated")
    
    ###############################################################################################################################################
    ### Calculation of winching distance from forest roads
    ###############################################################################################################################################
    
    DebRF_D,DebRF_LRF=fc.skid_debusq_RF(Lien_RF,MNT,Row_line,Col_line,D_line,Nbpix_line,coeff,orig,Pmax_up,Pmax_down,
                                        Dtreuil_max_up,Dtreuil_max_down,Csize,nrows,ncols,Zone_OK*(Route_for==0)*1*(Piste==0))
    
    if language == 'FR':
        print("    - Distance de debusquage depuis les routes forestieres calculee")  
    else:
        print("    - Winching distance from forest roads calculated")
    
    ###############################################################################################################################################
    #### Calculation of winching distance from forest tracks
    ###############################################################################################################################################                 
    
    Debp_D,Debp_LP,Debp_Dtrpiste=fc.skid_debusq_Piste(Lien_piste,MNT,Row_line,Col_line,D_line,Nbpix_line,coeff,orig,Pmax_up,Pmax_down,
                                                      Dtreuil_max_up,Dtreuil_max_down,Csize,nrows,ncols,Zone_OK*(Piste==0)*1*(Route_for==0))

    if language == 'FR':
        print("    - Distance de debusquage depuis les pistes forestieres calculee")  
    else:
        print("    - Winching distance from forest tracks calculated")
    
    gc.collect()
    
    ###############################################################################################################################################    
    ### Concatenation of the resultats (1)
    ###############################################################################################################################################
    DTrain_piste = np.ones((nrows,ncols),dtype=np.int32)*-9999
    DTrain_foret = np.ones((nrows,ncols),dtype=np.int32)*-9999
    Ddebusquage = np.ones((nrows,ncols),dtype=np.int32)*-9999
    Lien_foret_piste = np.ones((nrows,ncols),dtype=np.int32)*-9999
    Lien_foret_RF = np.ones((nrows,ncols),dtype=np.int32)*-9999
             
    if Option_Skidder==1:
        # Option 1 : Skidder is forced to stay as much possible as possible on forest road networks
        # Get value of winching distance from forest roads
        Temp = (DebRF_D>=0)
        DTrain_piste[Temp] = 0
        DTrain_foret[Temp] = 0
        Ddebusquage[Temp] = DebRF_D[Temp]
        Lien_foret_RF[Temp] = DebRF_LRF[Temp]
            
        # Get value of winching distance from forest tracks  
        Temp = Debp_D>=0
        Temp[(Debp_D+0.1*Debp_Dtrpiste)>Ddebusquage]=0
        DTrain_piste[Temp] = Debp_Dtrpiste[Temp]
        DTrain_foret[Temp] = 0
        Ddebusquage[Temp] = Debp_D[Temp]
        Lien_foret_piste[Temp] = Debp_LP[Temp]
        Lien_foret_RF[Temp] = -9999
        Temp = ((Ddebusquage<0)*(Debp_D>=0))>0
        DTrain_piste[Temp] = Debp_Dtrpiste[Temp]
        DTrain_foret[Temp] = 0
        Ddebusquage[Temp] = Debp_D[Temp]
        Lien_foret_piste[Temp] = Debp_LP[Temp]
        Lien_foret_RF[Temp] = -9999
        
        # Get value of skidding distance within forest stand from forest tracks
        Temp = (Ddebusquage<0)*(D_foret_piste>=0)
        DTrain_piste[Temp] = D_piste[Temp]
        DTrain_foret[Temp] = D_foret_piste[Temp]
        Ddebusquage[Temp] = 0
        Lien_foret_piste[Temp] = L_Piste[Temp]
        
        # Get value of skidding distance within forest stand from forest roads
        Temp = (Ddebusquage==0)*(D_foret_RF>=0)
        Temp[(DTrain_foret+0.1*DTrain_piste)<D_foret_RF]=0
        DTrain_piste[Temp] = 0
        DTrain_foret[Temp] = D_foret_RF[Temp]
        Ddebusquage[Temp] = 0
        Lien_foret_RF[Temp] = L_RF[Temp]  
        Temp = (Ddebusquage<0)*(D_foret_RF>=0)
        DTrain_piste[Temp] = 0
        DTrain_foret[Temp] = D_foret_RF[Temp]
        Ddebusquage[Temp] = 0
        Lien_foret_RF[Temp] = L_RF[Temp]   
         
        
    else:
        # Option 2 : Limit the skidding : skidder goes as close as possible to the timber before using winching possibility

        # Get value of skidding distance within forest stand from forest tracks
        Temp = (D_foret_piste>=0)
        DTrain_piste[Temp] = D_piste[Temp]
        DTrain_foret[Temp] = D_foret_piste[Temp]
        Ddebusquage[Temp] = 0
        Lien_foret_piste[Temp] = L_Piste[Temp]   
        
        # Get value of skidding distance within forest stand from forest roads       
        Temp = (Ddebusquage==0)*(D_foret_RF>=0)
        Temp[(DTrain_foret+0.1*DTrain_piste)<D_foret_RF]=0
        DTrain_piste[Temp] = 0
        DTrain_foret[Temp] = D_foret_RF[Temp]
        Ddebusquage[Temp] = 0
        Lien_foret_RF[Temp] = L_RF[Temp]     
        Temp = (D_foret_RF>=0)*(DTrain_foret<0)
        DTrain_piste[Temp] = 0
        DTrain_foret[Temp] = D_foret_RF[Temp]
        Ddebusquage[Temp] = 0
        Lien_foret_RF[Temp] = L_RF[Temp]                  
        
        # Get value of winching distance from forest roads
        Temp = (DebRF_D>=0)*(DTrain_foret<0)
        DTrain_piste[Temp] = 0
        DTrain_foret[Temp] = 0
        Ddebusquage[Temp] = DebRF_D[Temp]
        Lien_foret_RF[Temp] = DebRF_LRF[Temp]   
        
        # Get value of winching distance from forest tracks
        Temp = Debp_D>=0
        Temp[(Debp_D+0.1*Debp_Dtrpiste)>Ddebusquage]=0
        DTrain_piste[Temp] = Debp_Dtrpiste[Temp]
        DTrain_foret[Temp] = 0
        Ddebusquage[Temp] = Debp_D[Temp]
        Lien_foret_piste[Temp] = Debp_LP[Temp] 
        Lien_foret_RF[Temp] = -9999        
        Temp = ((DTrain_foret<0)*(Debp_D>=0))>0
        DTrain_piste[Temp] = Debp_Dtrpiste[Temp]
        DTrain_foret[Temp] = 0
        Ddebusquage[Temp] = Debp_D[Temp]
        Lien_foret_piste[Temp] = Debp_LP[Temp] 
        Lien_foret_RF[Temp] = -9999
        
        
    # Calculation of the forest area accessible with a skidder
    zone_tracteur = np.zeros_like(MNT,dtype=np.int8)
    zone_tracteur[((D_foret_piste>=0)*(Foret==1))>0]=1
    zone_tracteur[((D_foret_RF>=0)*(Foret==1))>0]=1    
    
    if language == 'FR':
        ArrayToGtiff(zone_tracteur,Rspace_s+'Zone_parcourable_par_skidder',Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')
        print("    - Concatenation 1 terminee")  
    else:
        ArrayToGtiff(zone_tracteur,Rspace_s+'Free_access_of_the_skidder',Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')  
        print("    - First concatenation done")
    
   
    del DebRF_D,DebRF_LRF,Debp_D,Debp_LP,Debp_Dtrpiste
    gc.collect() 
    
    ################################################################################################################################################  
    ### Calculation of winching distance from contours
    ################################################################################################################################################
    contour = fc.focal_stat_nb(np.float_((zone_tracteur==1)*1),0,1)
    contour = ((contour<9)*(contour>0)>0)*1
    contour[Full_Obstacles_skidder==1]=0
    contour[Partial_Obstacles_skidder==1]=0
    contour[Res_pub==1]=0   
    contour[MNT_OK==0]=0  
    
    pixels=np.argwhere(contour>0)
    del zone_tracteur,Full_Obstacles_skidder,Partial_Obstacles_skidder,Res_pub,MNT_OK,contour
    gc.collect()
    
    #line=ID_contour, Y, X,L_RF,L_piste,Dtrain,Dpis
    
    Lien_contour = np.zeros((pixels.shape[0]+1,6),dtype=np.int)    
    ID = 1
    for pixel in pixels:
        Lien_contour[ID,0],Lien_contour[ID,1]=pixel[0],pixel[1]
        testpiste=0
        if D_foret_RF[pixel[0],pixel[1]]<0:
            testpiste=1
        elif D_piste[pixel[0],pixel[1]]>=0:
            if (D_foret_piste[pixel[0],pixel[1]]+0.1*D_piste[pixel[0],pixel[1]])<D_foret_RF[pixel[0],pixel[1]]:
                testpiste=1
        if testpiste:
            Lien_contour[ID,2],Lien_contour[ID,3]=-9999,L_Piste[pixel[0],pixel[1]]
            Lien_contour[ID,4],Lien_contour[ID,5]=D_foret_piste[pixel[0],pixel[1]],D_piste[pixel[0],pixel[1]]
        else:
            Lien_contour[ID,2],Lien_contour[ID,3]=L_RF[pixel[0],pixel[1]],-9999
            Lien_contour[ID,4],Lien_contour[ID,5]=D_foret_RF[pixel[0],pixel[1]],0     
        ID += 1
    del D_foret_piste,L_Piste,D_piste,D_foret_RF,L_RF
    gc.collect()    
    
    # Get the contour of traversable area
    Ddebus,L_RF,L_Piste,Dpis,Dfor=fc.skid_debusq_contour(Lien_contour,MNT,Row_line,Col_line,D_line,Nbpix_line,coeff,orig,Pmax_up,Pmax_down,
                                                         Dtreuil_max_up,Dtreuil_max_down,Csize,nrows,ncols,Zone_OK*1*(Ddebusquage<=0))
                                                      
    del Lien_contour,pixels,MNT
    gc.collect()
    
    if language == 'FR':
        print("    - Distance de debusquage depuis le contour de la zone parcourable calculee")  
    else:
        print("    - Winching distance from contour of accessible area calculated")    
    
    ################################################################################################################################################  
    ### Concatenation (2)
    ################################################################################################################################################    
    
    Temp = (Ddebusquage<0)*(Ddebus>=0)
    DTrain_piste[Temp] = Dpis[Temp]
    DTrain_foret[Temp] = Dfor[Temp]
    Ddebusquage[Temp] = Ddebus[Temp]
    Lien_foret_RF[Temp] = L_RF[Temp]
    Lien_foret_piste[Temp] = L_Piste[Temp]
        
    del Ddebus,L_RF,L_Piste,Dpis,Dfor
    gc.collect()
    
    Temp = (Foret==0)
    DTrain_piste[Temp] = -9999
    DTrain_foret[Temp] = -9999
    Ddebusquage[Temp] = -9999
    Lien_foret_RF[Temp] = -9999
    Lien_foret_piste[Temp] = -9999
    
    del Temp
    gc.collect()
    
    # Fill Lien foret respub and Lien foret RF
    Lien_foret_Res_pub,Lien_foret_RF,Keep=fc.fill_Link(Lien_foret_piste,Lien_piste,Lien_RF, Lien_foret_RF, nrows,ncols)
    
    Temp = (Keep<1)*((Piste==1)+(Route_for==1))>0
    DTrain_piste[Temp] = -9999
    DTrain_foret[Temp] = -9999
    Ddebusquage[Temp] = -9999
    Lien_foret_RF[Temp] = -9999
    Lien_foret_piste[Temp] = -9999
    Lien_foret_Res_pub[Temp] = -9999
        
    del Temp,Keep
    gc.collect()
    
    # Calculation of the total skidding distance
    Dtotal = np.ones((nrows,ncols),dtype=np.int32)*-9999
    zone_accessible = np.zeros((nrows,ncols),dtype=np.int8)
    Temp = (Ddebusquage>=0)
    Dtotal[Temp]= DTrain_piste[Temp] + DTrain_foret[Temp] + Ddebusquage[Temp]
    zone_accessible[Temp] = 1    
   
    del Temp
    gc.collect()
    
    if language == 'FR':
        print("    - Concatenation des resultats terminee. Sauvegarde en cours...")          
    else:
        print("    - Concatenation of results achieved. Saving in progress...")   
        
    
    ### Saving all rasters
    ##################################################################################################################################################
    ### Create a summary table of accessible area    
    make_summary_surface_vol(Skid_Debclass,file_Vol_ha,Surf_foret,Surf_foret_non_access,Csize,Dtotal,Vtot,Vtot_non_buch,Rspace_s,"Skidder",language)
     
    ### Save output rasters
    if language == 'FR':
        ArrayToGtiff(zone_accessible,Rspace_s+'Foret_accessible',Extent,nrows,ncols,Csize,road_network_proj,0,raster_type='UINT8')
        ArrayToGtiff(Foret-zone_accessible,Rspace_s+'Foret_inaccessible',Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')
        ArrayToGtiff(DTrain_piste,Rspace_s+'Distance_trainage_piste',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT16')
        ArrayToGtiff(DTrain_foret,Rspace_s+'Distance_trainage_foret',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT16')
        ArrayToGtiff(Ddebusquage,Rspace_s+'Distance_debusquage',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT16')
        ArrayToGtiff(Dtotal,Rspace_s+'Distance_totale_foret_route_forestiere',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT32')        
        ArrayToGtiff(Lien_foret_piste,Rspace_s+'Lien_foret_piste',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT32')
        ArrayToGtiff(Lien_foret_RF,Rspace_s+'Lien_foret_route_forestiere',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT32')
        ArrayToGtiff(Lien_foret_Res_pub,Rspace_s+'Lien_foret_Reseau_public',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT32')
    else:
        ArrayToGtiff(zone_accessible,Rspace_s+'Accessible_forest',Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')        
        ArrayToGtiff(Foret-zone_accessible,Rspace_s+'Unaccessible_forest',Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')
        ArrayToGtiff(DTrain_piste,Rspace_s+'Skidding_distance_on_tracks',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT16')
        ArrayToGtiff(DTrain_foret,Rspace_s+'Skidding_distance_in_forest',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT16')  
        ArrayToGtiff(Ddebusquage,Rspace_s+'Winching_distance',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT16')
        ArrayToGtiff(Dtotal,Rspace_s+'Total_distance',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT32')        
        ArrayToGtiff(Lien_foret_piste,Rspace_s+'Link_forest_ForestTrack',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT32')
        ArrayToGtiff(Lien_foret_RF,Rspace_s+'Link_forest_ForestRoads',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT32')
        ArrayToGtiff(Lien_foret_Res_pub,Rspace_s+'Link_forest_public_road_network',Extent,nrows,ncols,Csize,road_network_proj,-9999,'INT32')

    layer_name = 'Skidder_recap_accessibilite'
    if language=="EN":
        layer_name = 'Skidder_recap_accessibility'
    source_src=get_source_src(file_shp_Desserte)  
    create_access_shapefile(Dtotal,Rspace_s,zone_accessible,Foret,Skid_Debclass.split(";"),language,road_network_proj,source_src,Csize, Dir_temp,Extent,nrows,ncols,layer_name)
      
    del zone_accessible,DTrain_piste,DTrain_foret,Ddebusquage,Dtotal
    del Lien_foret_piste,Lien_foret_RF,Lien_foret_Res_pub
    
    str_duree,str_fin,str_debut=heures(Hdebut,language)
    
    ### Genere le fichier avec le resume des parametres de simulation
    if language == 'FR':
        file_name = str(Rspace_s)+"Parametres_simulation.txt"
        resume_texte = "Sylvaccess : CARTOGRAPHIE AUTOMATIQUE DES ZONES ACCESSIBLES PAR TRACTEUR FORESTIER\n\n\n"
        resume_texte = resume_texte+"Version du programme : 3.5.1 de 12/2021\n\n"
        resume_texte = resume_texte+"Resolution           : "+str(Csize)+" m\n\n"
        resume_texte = resume_texte+"Date et heure de lancement du script:             "+str_debut+"\n"
        resume_texte = resume_texte+"Date et heure a la fin de l'execution du script:  "+str_fin+"\n"
        resume_texte = resume_texte+"Temps total d'execution du script:                "+str_duree+"\n\n"
        resume_texte = resume_texte+"PARAMETRES UTILISES POUR LA MODELISATION:\n\n"
        resume_texte = resume_texte+"   - Distance maximale de debusquage en amont de la desserte :       "+str(Dtreuil_max_up)+" m\n"
        resume_texte = resume_texte+"   - Distance maximale de debusquage en aval de la desserte  :       "+str(Dtreuil_max_down)+" m\n"
        resume_texte = resume_texte+"   - Pente au-dela de laquelle le debusquage amont = distance max :  "+str(Pmax_amont)+" %\n"
        resume_texte = resume_texte+"   - Pente au-dela de laquelle le debusquage aval  = distance max :  "+str(Pmax_aval)+" %\n\n"
        resume_texte = resume_texte+"   - Distance maximale parcourable hors foret et hors desserte :     "+str(Dmax_train_near_for)+" m\n"
        resume_texte = resume_texte+"   - Pente maximale pour parcourir le terrain en skidder :           "+str(Pente_max_skidder)+" %\n"
        resume_texte = resume_texte+"   - Pente maximale pour l'abattage manuel des arbres :              "+str(Pente_max_bucheron)+" %\n\n"
        resume_texte = resume_texte+"   - Option de simulation :\n"    
        if Option_Skidder==1:
            resume_texte = resume_texte+"      * Limiter l'impact sur les sols : forcer le skidder a proceder autant que possible\n"    
            resume_texte = resume_texte+"        depuis le reseau de desserte forestiere\n" 
        else:
            resume_texte = resume_texte+"      * Limiter le debusquage : forcer le skidder a se rapprocher autant que possible\n"    
            resume_texte = resume_texte+"        des grumes de bois\n" 
        if Dir_Full_Obs_skidder!='':
            resume_texte = resume_texte+"      * Prise en compte de zones totalement interdites a l'exploitation par skidder\n"  
        if Dir_Partial_Obs_skidder!='':
            resume_texte = resume_texte+"      * Prise en compte de zones ou le trainage des bois est interdit\n"  
            
        if os.path.exists(Rspace_s+'Pistes_non_connectees.tif'):
            resume_texte = resume_texte+"\n\n"
            resume_texte = resume_texte+"      !!! Attention !!! Certaines pistes forestières ne sont pas connectée.\n"  
            resume_texte = resume_texte+"      Elles ont été exclues de l'analyse.\n"  
        if os.path.exists(Rspace_s+'Routes_forestieres_non_connectees.tif'):
            resume_texte = resume_texte+"\n\n      !!! Attention !!! Certaines routes forestières ne sont pas connectée.\n"  
        fichier = open(file_name, "w")
        fichier.write(resume_texte)
        fichier.close()
    else:
        file_name = str(Rspace_s)+"Parameters_of_simulation.txt"
        resume_texte = "Sylvaccess : AUTOMATIC MAPPING OF FOREST ACCESSIBILITY WITH SKIDDER\n\n\n"
        resume_texte = resume_texte+"Software version : 3.5.1 - 2021/12\n\n"
        resume_texte = resume_texte+"Resolution       : "+str(Csize)+" m\n\n"
        resume_texte = resume_texte+"Date and time when launching the script:              "+str_debut+"\n"
        resume_texte = resume_texte+"Date and time at the end of execution of the script:  "+str_fin+"\n"
        resume_texte = resume_texte+"Total execution time of the script:                   "+str_duree+"\n\n"
        resume_texte = resume_texte+"PARAMETERS USED FOR THE MODELING:\n\n"
        resume_texte = resume_texte+"   - Maximum uphill distance for winching:                        "+str(Dtreuil_max_up)+" m\n"
        resume_texte = resume_texte+"   - Maximum downhill distance for winching:                      "+str(Dtreuil_max_down)+" m\n"
        resume_texte = resume_texte+"   - Maximum slope to get maximum uphill winching distance:       "+str(Pmax_amont)+" %\n"
        resume_texte = resume_texte+"   - Maximum slope to get maximum downhill winching distance:     "+str(Pmax_aval)+" %\n\n"
        resume_texte = resume_texte+"   - Maximum distance outside forest and forest road network:     "+str(Dmax_train_near_for)+" m\n"
        resume_texte = resume_texte+"   - Maximum slope for a free access of the parcels with skidder: "+str(Pente_max_skidder)+" %\n"
        resume_texte = resume_texte+"   - Maximum slope for manual felling of the trees:               "+str(Pente_max_bucheron)+" %\n\n"
        resume_texte = resume_texte+"   - Simulation option:\n"    
        if Option_Skidder==1:
            resume_texte = resume_texte+"      * Limit soil damages: force the skidder to process as much as possible \n"    
            resume_texte = resume_texte+"        from the forest road network\n" 
        else:
            resume_texte = resume_texte+"      * Limit winching distances: force the skidder to go as close as possible\n"    
            resume_texte = resume_texte+"        to the timber\n" 
        if Dir_Full_Obs_skidder!='':
            resume_texte = resume_texte+"      * Simulation with areas where skidder operations are forbidden (Full obstacles)\n"  
        if Dir_Partial_Obs_skidder!='':
            resume_texte = resume_texte+"      * Simulation with areas where skidding is forbidden but winching possible (Partial obstacles)\n"  
            
        if os.path.exists(Rspace_s+"Forest_tracks_not_connected.tif"):
            resume_texte = resume_texte+"\n\n"
            resume_texte = resume_texte+"      !!! Warning !!! Some forest tracks are not connected to public network.\n"  
            resume_texte = resume_texte+"      They were removed from the analysis.\n"  
        if os.path.exists(Rspace_s+"Forest_road_not_connected.tif"):
            resume_texte = resume_texte+"\n\n      !!! Warning !!! Some forest roads are not connected to public network.\n"      
            
            
        fichier = open(file_name, "w")
        fichier.write(resume_texte)
        fichier.close()
    if language == 'FR':
        print("Modele skidder termine")
    else:    
        print("Skidder accessibility processed")
    clear_big_nparray()
    gc.collect()
    
