# -*- coding: utf8 -*-
"""
Software: Sylvaccess
File: Sylvaccess_2_cable.py
Copyright (C) Sylvain DUPIRE 2021
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@inrae.fr
Version: 3.5.1
Date: 2021/12/17
License :  GNU-GPL V3

-----------------------------------------------------------------------
Français: (For english see above)

Ce fichier fait partie de Sylvaccess qui est un programme informatique 
servant à cartographier automatiquement les forêts accessibles en fonction de 
différents modes d'exploitations (skidder, porteur, débardage par câble aérien). 

Ce logiciel est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
suivant les termes de la GNU General Public License telle que publiée par la 
Free Software Foundation ; soit la version 3 de la licence, soit (à votre gré) 
toute version ultérieure.
Sylvaccess est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE 
GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION 
à UN BUT PARTICULIER. Consultez la GNU General Public License pour plus de 
détails.
Vous devez avoir reçu une copie de la GNU General Public License en même temps 
que Sylvaccess ; si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.

-----------------------------------------------------------------------
English

This file is part of Sylvaccess which is a computer program whose purpose 
is to automatically map forest accessibility according to different forest 
operation systems (skidder, forwarder, cable yarding)..

Sylvaccess is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sylvaccess.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
import os,datetime,gc,sys
from scipy.interpolate import InterpolatedUnivariateSpline
from sylvaccess_fonctions import *
import sylvaccess_cython3 as fc
from math import radians,pi,sqrt,atan

def prepa_data_cable(Wspace,file_MNT,file_shp_Foret,file_shp_Cable_Dep,Dir_Obs_cable,Pente_max_bucheron,language):
    print("")
    if language=='EN':
        print("Pre-processing of the inputs for cable model\n")
    else:
        print("Pre-traitement des entrees pour le modele cable\n")
    ### Make directory for temporary files
    Dir_temp = Wspace+"Temp/"
    try:os.mkdir(Dir_temp)
    except:pass     
    ##############################################################################################################################################
    ### Initialization
    ##############################################################################################################################################
    MNT,Extent,Csize,proj = load_float_raster(file_MNT,Dir_temp)
    np.save(Dir_temp+"MNT",MNT)
    
    ##############################################################################################################################################
    ### Forest : shapefile to raster
    ##############################################################################################################################################
    Foret = shapefile_to_np_array(file_shp_Foret,Extent,Csize,"FORET")
    np.save(Dir_temp+"Foret",np.int8(Foret))    
    del Foret
    if language=='EN':
        print("    - Forest raster processed")    
    else:
        print("    - Raster de foret termine")    
    ### Forest : shapefile to raster 
    Exposition = fc.exposition(MNT,Csize,-9999)
    np.save(Dir_temp+"Aspect",np.uint16(Exposition+0.5))
    Pente = prepa_obstacle_cable(Dir_Obs_cable,file_MNT,Dir_temp)
    np.save(Dir_temp+"Pente",Pente)    
    Pond_pente = np.sqrt((Pente*0.01*Csize)**2+Csize**2)/float(Csize)
    Pond_pente[Pente==-9999] = 10000
    np.save(Dir_temp+"Pond_pente",Pond_pente)
    del Pente,MNT  
    
    if language=='EN':
        print("    - Cable obstacles processed")
    else:
        print("    - Obstacles pour le cable traites")

    ##############################################################################################################################################
    ### Cable start processing
    ##############################################################################################################################################
#    Desserte_temp = shapefile_to_np_array(file_shp_Cable_dep,Extent,Csize,"CL_SVAC","CL_SVAC",'ASC') 
#    # Public network
#    Res_pub = (Desserte_temp==3)*1    
#    # Forest road
#    Route_for = (Desserte_temp==2)*1
#    np.save(Dir_temp+"Route_for",np.int8(Route_for))    
#    # Give an identifiant to each public network pixel    
#    ID = 1
#    pixels = np.argwhere(Res_pub==1)
#    del Res_pub
#    gc.collect()
#    Tab_res_pub = np.zeros((pixels.shape[0]+1,2),dtype=np.int) 
#    for pixel in pixels:
#        Tab_res_pub[ID,0],Tab_res_pub[ID,1]=pixel[0],pixel[1]
#        ID +=1     
#    np.save(Dir_temp+"Tab_res_pub",Tab_res_pub) 
    #Cable start
    Cable_start = shapefile_to_np_array(file_shp_Cable_Dep,Extent,Csize,"CABLE","CABLE",'ASC') 
    testExist = check_field(file_shp_Cable_Dep,"EXIST") 
    if testExist:    
        Existing = shapefile_to_np_array(file_shp_Cable_Dep,Extent,Csize,"EXIST","EXIST",'ASC') 
    else:
        Existing = np.ones_like(Cable_start,dtype=np.int8)*2
        # if language=='EN':
        #     print('    - Field "EXIST" is missing from potential cable starts.')
        #     print('      All starts are considered as existing')
        # else:
        #     print('    - Champs "EXIST" absent de la couche des departs de cable potentiels.') 
        #     print('      Tous les departs sont donc consideres comme existant.') 
    pixels = np.argwhere(Cable_start>0)
    Lien_RF = np.zeros((pixels.shape[0]+1,5),dtype=np.int) 
    ID = 1
    for pixel in pixels:
        Lien_RF[ID,0],Lien_RF[ID,1]=pixel[0],pixel[1]
        Lien_RF[ID,3]=-9999
        Lien_RF[ID,4]=Cable_start[pixel[0],pixel[1]]
        if Pond_pente[pixel[0],pixel[1]]==10000:
            Lien_RF[ID,2]=-9999
        else:
            Lien_RF[ID,2]=Existing[pixel[0],pixel[1]]
        ID +=1         
    # Link RF with res_pub and calculate transportation distance
#    Lien_RF=fc.Link_RF_res_pub(Tab_res_pub,Pond_pente,Route_for, Lien_RF,Csize) 
#    Lien_RF=Lien_RF[Lien_RF[:,4]>0]
    Lien_RF=Lien_RF[Lien_RF[:,2]>-1]
    np.save(Dir_temp+"Lien_RF_c",Lien_RF)
    if language=='EN':
        print("    - Potential cable yarding starts processed")    
    else:
        print("    - Departs de cables potentiels identifies")    
    ### Creer les raster de coordonnees
    ##############################################################################################################################################
    names,values,proj,Extent = raster_get_info(file_MNT)
    Csize,ncols,nrows = values[4],int(values[0]),int(values[1])  
    TableX,TableY=create_coord_pixel_center_raster(names,values,nrows,ncols,Csize,Dir_temp)
    CoordRoute = np.zeros((Lien_RF.shape[0],2),dtype=np.float)
    for i,pixel in enumerate(Lien_RF):
        CoordRoute[i,0]=TableX[pixel[1]]
        CoordRoute[i,1]=TableY[pixel[0]] 
    np.save(Dir_temp+"CoordRoute.npy",CoordRoute)  
    if language=='EN':
        print("    - Table of coordinates created")   
    else:
        print("    - Table des coordonnees creee")  
    ### Close the script
    ##############################################################################################################################################
    if language=='EN':
        print("\nCable input data processing achieved\n")
    else:
        print("\nPre-traitement des entrees pour le cable termine\n")
    clear_big_nparray()

def line_selection(Rspace_c,w_list,lim_list,new_calc,file_shp_Foret,file_Vol_ha,file_Vol_AM,
                   language,Lhor_max,prelevement,Pente_max_bucheron):
    if language=='FR':
        print("Selection des meilleures lignes en fonction des criteres de l'utilisateur.")
    else:
        print("Start of the best lines selection according to user criteria.")
    ### Check if temporary files have been generated and have the same extent
    Rspace_sel = Rspace_c+"FilesForOptimisation/"
    try: 
       Tab = np.load(Rspace_sel+"Tab_all_lines.npy") 
    except:
        if language=='FR':
            print("Veuillez d'abord faire tourner le modele cable.")
        else:
            print("Please run first the Sylvaccess cable.")
        return ""
    Lmax = np.max(Tab[:,11])    
    names,values,Extent=loadrasterinfo_from_file(Rspace_sel)
    Csize,nrows,ncols=values[4],int(values[1]),int(values[0]) 
    if not new_calc:
        f = open(Rspace_sel+"info_Lhormax.txt","r")
        Lhor_max=f.readlines(0)
        f.close()
        Lhor_max= float(Lhor_max[0])
    
    Row_line,Col_line,D_line,Nbpix_line,Row_ext,Col_ext,D_ext,D_lat,Dir_list=create_buffer2(Csize,Lmax,Lhor_max)        
    Lien_RF = np.load(Rspace_sel+"Lien_RF_c.npy")
    ############################################
    ### Recompute cable line stats if necessary
    ############################################
    if new_calc:
        if language=='FR':
            print("    - Recalcule les caracteristiques des lignes avec les nouvelles couches...")
        else:
            print("    - Processing new line characteristics with new input data...")   
        #Couche foret
        if file_shp_Foret != "":
            Forest = np.int8(shapefile_to_np_array(file_shp_Foret,Extent,Csize,"FORET"))
        else:
            Forest=np.load(Rspace_sel+"Forest.npy")
        #Couche vol_ha
        if file_Vol_ha != "":
            Vol_ha = load_float_raster_simple(file_Vol_ha) 
            Vol_ha[Vol_ha<0]=0
            Vol_ha[np.isnan(Vol_ha)]=0
            test_vp = True
        else:
            test_vp = False            
            if file_Vol_AM != "":
                Vol_ha = np.zeros((Forest.shape),dtype=np.float)
            else:
                Vol_ha = np.zeros_like(Forest,dtype=np.int8)
        #Couche vol_am
        if file_Vol_AM != "":
            Vol_AM = load_float_raster_simple(file_Vol_AM) 
            Vol_AM[Vol_AM<0]=0
            Vol_AM[np.isnan(Vol_AM)]=0
            test_vam = True
        else:
            test_vam = False  
            if file_Vol_ha != "":
                Vol_AM = np.zeros((Forest.shape),dtype=np.float)   
            else:
                Vol_AM = np.zeros_like(Forest,dtype=np.int8)   
       
        if test_vp or test_vam:             
            Pente = np.load(Rspace_sel+"Pente.npy")
            Vol_AM[Pente>Pente_max_bucheron]=0
            Vol_ha[Pente>Pente_max_bucheron]=0
        nbline = Tab.shape[0]        
        Rast_couv = np.zeros_like(Forest,dtype=np.int8)
        for i in range(0,nbline):
            coordX=Lien_RF[Tab[i,0],1]
            coordY=Lien_RF[Tab[i,0],0]       
            az=Tab[i,1]
            Lline=sqrt((Tab[i,2]-Tab[i,6])**2+(Tab[i,3]-Tab[i,7])**2)                                                      
            if test_vp or test_vam:
                Distance_moyenne,Surface,Vtot,VAM,Rast_couv = fc.get_line_carac_vol(coordX,coordY,az,Csize,ncols,nrows,Lline,Row_ext,Col_ext,D_ext,Forest,Rast_couv,Vol_ha,Vol_AM)
            else:
                Distance_moyenne,Surface,Rast_couv = fc.get_line_carac_simple(coordX,coordY,az,Csize,ncols,nrows,Lline,Row_ext,Col_ext,D_ext,Forest,Rast_couv)
            #Surface foret Dmoy chariot
            Tab[i,13],Tab[i,14]=Surface,Distance_moyenne
            #Vtot IPC
            if test_vp:
                Tab[i,15]=Vtot
            #VAM               
            if test_vam:
                Tab[i,16]=VAM*10
   
    
    ############################################
    ### Calc IPC according to prelevement
    ############################################
    
    Tab2 = np.zeros((Tab.shape[0],Tab.shape[1]+4),dtype=np.int)
    Tab2[:,0:-4]=Tab
    Tab2[:,-4]=np.int_(100.0*Tab[:,15]*prelevement/Tab[:,11])
    del Tab
    gc.collect()    
    Tab2[:,-1]= Lien_RF[Tab2[:,0],0]
    Tab2[:,-2]= Lien_RF[Tab2[:,0],1]     
    del Lien_RF
    gc.collect()
    sup_max=np.max(Tab2[:,17])    
    
    
    lim_list[4]=lim_list[4]/prelevement #in order to account for prelevement in line selection
    
    #Modify selection criteria in case of no volume or vam        
    if np.max(Tab2[:,15])==0:        
        if w_list[4]>0:
            if language=='FR':
                print("Optimisation impossible sur le volume/ipc car aucune information disponible.")
            else:
                print("Impossible optimization on volum/ipc criteria (no information available)")        
        w_list[4],lim_list[4]=0,0
        w_list[5],lim_list[5]=0,0
        
    if np.max(Tab2[:,16])==0:
        if w_list[6]>0:
            if language=='FR':
                print("Optimisation impossible sur le volume de l'arbre moyen car aucune information disponible.")
            else:
                print("Impossible optimization on average tree volume criteria (no information available)")
        w_list[6],lim_list[6]=0,0
        
    Rast_couv,Tab_result,FR_Tab_name,EN_Tab_name=select_best_lines(w_list,lim_list,Tab2,
                                                                   nrows,ncols,Csize,
                                                                   Row_ext,Col_ext,D_ext,
                                                                   D_lat,Lhor_max,sup_max)
    del Tab2  
    gc.collect()
    #Get folder
    dirs = [d for d in os.listdir(Rspace_c) if os.path.isdir(os.path.join(Rspace_c, d))]
    list_dir = []
    for dire in dirs:
        if dire[:12]=='Optimisation':
            list_dir.append(dire)
    optnum = len(list_dir)+1
    Dir_result = Rspace_c+'Optimisation'+str(optnum) 
    ### Get best volume
    if language=='FR':
        header = 'ID_pixel Azimuth_deg X_debut Y_debut Alt_debut Hcable_debut X_fin Y_fin Alt_fin Hcable_fin '
        header +='Etat_RouteFor Longueur_reelle Configuration '
        header +='Surface_foret Distance_moy_chariot Volume_total VAM NB_int_sup'
        for num in range(1,sup_max+1):
            header +=' '+'Xcoord_intsup'+str(num)+' Ycoord_intsup'+str(num)+' Alt_intsup'+str(num)
            header +=' '+'Hcable_intsup'+str(num)+' Pression_intsup'+str(num)
        header +='IPC cout'
        Dir_result +=FR_Tab_name
    else:
        header = 'ID_pixel Azimuth_deg X_Start Y_Start Elevation_Start Hcable_Start X_End Y_End Elevation_End Hcable_End '
        header +='Existing_road Cable_length Configuration '
        header +='Forest_area Carriage_average_distance Volume_total ATV NB_int_sup'
        for num in range(1,sup_max+1):
            header +=' '+'Xcoord_intsup'+str(num)+' Ycoord_intsup'+str(num)+' Elevation_intsup'+str(num)
            header +=' '+'Hcable_intsup'+str(num)+' Pression_intsup'+str(num)
        header +='IPC cost'        
        Dir_result +=EN_Tab_name   
    header +='\n'
    try:os.mkdir(Dir_result)
    except:pass
    Dir_result+="/"
    filename = Dir_result+"Database_Optim_"+str(optnum)+".gzip" 
    shape_name = Dir_result+"CableLines_Optim_"+str(optnum)+".shp"
    rast_name = Dir_result+"CableArea_Optim_"+str(optnum)
    pyl_name = Dir_result+"Int_sup_Optim_"+str(optnum)+".shp"
    
    save_integer_ascii(filename,header,Tab_result)
    source_src=get_source_src(Rspace_sel+"info_proj.shp")  
    road_network_proj=get_proj_from_road_network(Rspace_sel+"info_proj.shp")
    Line_to_shapefile(Tab_result,shape_name,source_src,prelevement,language) 
    #New rast_couv to take into account project of cable start
    Rast_couv=create_rast_couv(Tab_result,Dir_result,source_src,Extent,Csize,Lhor_max)
    #Rast_couv[Rast_couv>0]=1
    if not new_calc:
        Forest=np.load(Rspace_sel+"Forest.npy")
        try:         
            Vol_ha=np.load(Rspace_sel+"Vol_ha.npy")
        except:
            Vol_ha=np.zeros_like(Forest)
        try:
            Vol_AM=np.load(Rspace_sel+"Vol_AM.npy")
        except:
            Vol_AM=np.zeros_like(Forest)    
    Rast_couv[Forest==0]=0
    ArrayToGtiff(Rast_couv,rast_name,Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')    
    Pylone_in_shapefile(Tab_result,pyl_name,source_src,prelevement)
    ### Summary of the choice
    generate_info_ligne(Dir_result,w_list,lim_list,Tab_result,language,Rast_couv,Vol_ha,Vol_AM,Csize,prelevement,Lhor_max) 
    if language=='FR':
        print("Selection des meilleures lignes de cable terminee.")
    else:
        print("Selection of the best cable lines achieved.")  
        


def process_cable(Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Cable_dep,Dir_Obs_cable,file_Vol_ha,file_Vol_AM,file_Htree,Pente_max_bucheron,
                  Lmax,Lmin,LminSpan,Htower,Hintsup,Hend,Lhor_max,Hline_min,Hline_max,sup_max,Carriage_type,Cable_type,slope_grav,
                  Pchar,slope_Wliner_up,slope_Wliner_down,q1,rupt_res,safe_fact,E,d,Load_max,q2,q3,Max_angle,coeff_frot,language,
                  precision,prelevement,slope_trans,angle_transv,test_cable_optimise,w_list,lim_list,VariaH,Lslope,PropSlope):
    print("")
    if language=='FR':
        print("Debut de Sylvaccess-Cable")
    else:
        print("Sylvaccess cable crane starts")
    
    Hdebut = datetime.datetime.now()
    Dir_temp = Wspace+"Temp/"  
    ### Check if temporary files have been generated and have the same extent
    try:
        names,values,proj,Extent = raster_get_info(file_MNT)
        Csize,ncols,nrows = values[4],int(values[0]),int(values[1])    
    except:
        if language=='EN':  
            print("Error: please define a projection for the DTM raster")
        else:
            print("Erreur: veuillez definir une projection pour le raster MNT")
        return ""
    try: 
        n,v1=read_info(Dir_temp+'info_extent.txt')
        for i,item in enumerate(values):
            if v1[i]!=round(item,2):
                prepa_data_cable(Wspace,file_MNT,file_shp_Foret,file_shp_Cable_dep,Dir_Obs_cable,Pente_max_bucheron,language)
            if i+1>4:break
    except:
        prepa_data_cable(Wspace,file_MNT,file_shp_Foret,file_shp_Cable_dep,Dir_Obs_cable,Pente_max_bucheron,language)
    
    # Inputs
    try:
        Forest = np.int8(np.load(Dir_temp+"Foret.npy"))                
        MNT= np.load(Dir_temp+"MNT.npy") 
        Pente = np.uint16(np.load(Dir_temp+"Pente.npy")+0.5)
        Lien_RF= np.load(Dir_temp+"Lien_RF_c.npy") 
        try:
            Aspect = np.uint16(np.load(Dir_temp+"Aspect.npy"))
        except:
            Aspect = np.uint16(fc.exposition(MNT,Csize,-9999))    
        try:
            CoordRoute= np.load(Dir_temp+"CoordRoute.npy") 
        except:
            TableX,TableY=create_coord_pixel_center_raster(names,values,nrows,ncols,Csize,Dir_temp)
            CoordRoute = np.zeros((Lien_RF.shape[0],2),dtype=np.float)
            for i,pixel in enumerate(Lien_RF):
                CoordRoute[i,0]=TableX[pixel[1]]
                CoordRoute[i,1]=TableY[pixel[0]] 
            np.save(Dir_temp+"CoordRoute.npy",CoordRoute) 
            del TableX,TableY
        try:
            Aerian_obs= np.int8(np.load(Dir_temp+"Obstacles_cables.npy"))  
        except:
            Pente = np.uint16(prepa_obstacle_cable(Dir_Obs_cable,file_MNT,Dir_temp))
            Aerian_obs= np.int8(np.load(Dir_temp+"Obstacles_cables.npy"))  
    except: 
        prepa_data_cable(Wspace,file_MNT,file_shp_Foret,file_shp_Cable_dep,Dir_Obs_cable,Pente_max_bucheron,language)
        Forest = np.int8(np.load(Dir_temp+"Foret.npy"))
        Aerian_obs= np.int8(np.load(Dir_temp+"Obstacles_cables.npy"))         
        MNT= np.load(Dir_temp+"MNT.npy") 
        Lien_RF= np.load(Dir_temp+"Lien_RF_c.npy")  
        CoordRoute= np.load(Dir_temp+"CoordRoute.npy")
        Aspect = np.uint16(np.load(Dir_temp+"Aspect.npy"))
        Pente = np.uint16(np.load(Dir_temp+"Pente.npy")+0.5)    

    Csize,ncols,nrows = values[4],int(values[0]),int(values[1])    
    
     
    ### Import optional files
    if file_Vol_ha != "":
        Vol_ha = load_float_raster_simple(file_Vol_ha) 
        Vol_ha[Pente>Pente_max_bucheron]=0 
        test_vp = True
    else:test_vp = False
    if file_Vol_AM != "":
        Vol_AM = load_float_raster_simple(file_Vol_AM) 
        Vol_AM[Pente>Pente_max_bucheron]=0
        test_vam = True
    else:test_vam = False 
    if file_Htree != "":
        Hfor = load_float_raster_simple(file_Htree) 
        test_hfor = True
    else:
        Hfor=0
        test_hfor = False
    if test_vp or test_vam:
        if not test_vp:
            Vol_ha = np.zeros_like(MNT)
        if not test_vam:
            Vol_AM = np.zeros_like(MNT)


    Lmax2 = round(Lmax-(sqrt(2)*(max(np.max(Hfor)*0.6666,Hend)+5)),0) #In order to take into account anchorage
    Row_line,Col_line,D_line,Nbpix_line, Row_ext,Col_ext,D_ext,Dir_list=create_buffer(Csize,Lmax2,Lhor_max)    
    road_network_proj=get_proj_from_road_network(file_shp_Cable_dep)
    Skid_direction = 0
    Rspace_c,filename,slope_min_up,slope_max_up,slope_min_down,slope_max_down=get_cable_configs(Rspace,slope_Wliner_up,
                                                                                       slope_Wliner_down,slope_grav,
                                                                                       Cable_type,Carriage_type,
                                                                                       Skid_direction,language) 
    try:os.mkdir(Rspace_c)
    except:pass
    Rspace_c+="/"
#    f = open(filename, 'w')
#    f.close()
    Rspace_sel = Rspace_c+"FilesForOptimisation"
    try:os.mkdir(Rspace_sel)
    except:pass
    Rspace_sel+="/"
    save_raster_info(values,Rspace_sel)
#    write_file(Rspace_c,Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Cable_dep,Dir_Obs_cable,file_Vol_ha,
#               file_Vol_AM,Pente_max_bucheron,Lmax,Lmin,LminSpan,Htower,Hintsup,Hend,Lhor_max,Hline_min,
#               Hline_max,sup_max,Carriage_type,Cable_type,slope_grav,Pchar,slope_Wliner_up,slope_Wliner_down,
#               q1,rupt_res,safe_fact,E,d,Load_max,q2,q3,Max_angle,coeff_frot,language,Skid_direction,precision,
#               prelevement,slope_trans,angle_transv)
    ### Calculation of useful variables
    g = 9.80665     # m.s-2
    angle_intsup = radians(Max_angle) 
    Fo =  g*(Load_max+Pchar)
    Lsans_foret = min(Lmax*0.1,Lmin)          # Longueur max contigue sans foret
    Ao = 0.25*pi*(d**2) 
    Tmax = float(rupt_res)*g/float(safe_fact)
    EAo = E*Ao
    idLinemin = np.max([1,int(LminSpan/Csize+1.5),int(10/Csize+0.5)])
    
    # D H diag slope fact indmin indmax LoL ThL TvL TupL TdownL LoUg ThUg TvUg ind_fin_span free xmidL zmidL 
    # 0 1 2    3     4    5      6      7   8   9   10   11     12   13   14   15           16   17    18  
    Span = np.zeros((sup_max+1,16),dtype=np.float)
    rastLosup,rastTh,rastTv= check_tabconv(Dir_temp,d,E,Tmax,Lmax2,Fo,q1,q2,q3,Csize)    
    
    ### Preparation of forest roads
    nbconfig = 5       
    if precision > 1:Dir_list = range(0,360,2)
    if precision == 3:
        step_route = 2
        nbconfig = 1       
    else:
        step_route = 1
    nb_pixel_route = int((Lien_RF.shape[0]-1)/float(step_route))    
    
    Fin_ligne_forcee = np.int8(np.greater(Aerian_obs+(MNT<0),0))
    
    if language=='FR':
        print("    - Initialisation achevee, debut de traitement...")
        str_nb_pixel_route=  " / "+str(nb_pixel_route-1)+ " pixels traites"
    else:
        print("    - Initialization achieved, processing...")
        str_nb_pixel_route=  " / "+str(nb_pixel_route-1)+ " pixels processed"
    
    Tab = np.zeros((min(1000000,int(nb_pixel_route*(360)/step_route)),18+5*sup_max),dtype=np.int)
    File_Tab = []
    Tab_nb=0
    
    testExist = False
    if np.sum(Lien_RF[:,2]==1)>0:
        testExist = True
    
    ##############################################################################################################################################
    ### 2. PROCESSING OF THE AREA: TEST ALL POSSIBLE LINES
    ##############################################################################################################################################
    
    # Loop on forest road pixels
    Route = range(1,Lien_RF.shape[0]-1,step_route)
    id_line = 0
    Rast_couv = np.zeros((nrows,ncols),dtype=np.int8)
    if testExist:
        Rast_couv2 = np.zeros((nrows,ncols),dtype=np.int8)
    test=0
    for idpix,pixel in enumerate(Route):  
        # Print process
        sys.stdout.write("\r%d" % idpix + str_nb_pixel_route)
        sys.stdout.flush()
        #Get point coordinates
        coordY = Lien_RF[pixel,0]
        coordX = Lien_RF[pixel,1]
        direction = Lien_RF[pixel,4]
        if MNT[coordY,coordX]>-9999 and not Aerian_obs[coordY,coordX]:             
            RoadState = Lien_RF[pixel,2]
            posiY = CoordRoute[pixel,1]
            posiX = CoordRoute[pixel,0]   
            for az in Dir_list:                                          
                test,Lline,Line = get_ligne3(coordX,coordY,posiX,posiY,az,MNT,Forest,Fin_ligne_forcee,Aspect,Pente,Hfor,test_hfor,Lmax2,Lmin,Csize,
                                             Row_line,Col_line,D_line,Nbpix_line,angle_transv,slope_trans,ncols,nrows,Lsans_foret,
                                             Fo,Tmax,q1,q2,q3,Htower,Hend,Hline_max,Hintsup,Lslope,PropSlope)   
                if test==1:           
                    Span*=0    
                    Falt = InterpolatedUnivariateSpline(Line[:,0],Line[:,1])
                    Alts = Falt(np.arange(0.,Lline,0.5))
                    ### Optimize line
                    if Line[0,1]+Htower>=np.max(Line[idLinemin:,1])+Hend:   
                        if direction==2:
                            continue
                        #print az,"up",Line[-1,0] 
                        if VariaH:
                            Span = fc.OptPyl_Up(Line,Alts,Span,Htower,Hintsup,Hend,q1,q2,q3,Fo,Hline_min,Hline_max,
                                                 Csize,angle_intsup,EAo,E,d,sup_max,rastLosup,rastTh,rastTv,Tmax,
                                                 LminSpan,slope_min_up,slope_max_up,Lmax2,test_hfor,nbconfig)
                        else:
                            Span = fc.OptPyl_Up_NoH(Line,Alts,Span,Htower,Hintsup,Hend,q1,q2,q3,Fo,Hline_min,Hline_max,
                                                     Csize,angle_intsup,EAo,E,d,sup_max,rastLosup,rastTh,rastTv,Tmax,
                                                     LminSpan,slope_min_up,slope_max_up,Lmax2,test_hfor,nbconfig)
                        config = 1
                    else:    
                        if direction==1:
                            continue
                        #print az,"down",Line[-1,0]
                        if VariaH:
                            Span = fc.OptPyl_Down_init(Line,Alts,Span,Htower,Hintsup, Hend,q1,q2,q3,Fo,
                                             Hline_min,Hline_max,Csize,angle_intsup,EAo, E, d,
                                             sup_max,rastLosup,rastTh,rastTv,Tmax,LminSpan,
                                             slope_min_down, slope_max_down,Lmax2,test_hfor)
                            if Span[0,0]==0 or np.sum(Span[:,2])<Lmin:
                                test=0
                                continue
                            indmax=min(int(np.max(Span[:,15]))+2,Line.shape[0])
                            Line2=return_profile(Line[:indmax+1])
                            Falt = InterpolatedUnivariateSpline(Line2[:,0],Line2[:,1])
                            Alts = Falt(np.arange(0.,Lline,0.5))    
                            Span = fc.OptPyl_Down(Line2,Alts,Span*0,Htower,Hintsup,Hend,q1,q2,q3,Fo,Hline_min,Hline_max,Csize,angle_intsup,
                                                   EAo,E,d,sup_max,rastLosup,rastTh,rastTv,Tmax,LminSpan, min(-slope_min_down,-slope_max_down),
                                                   max(-slope_min_down,-slope_max_down),Lmax2,test_hfor,nbconfig)
                            config=-1
                        else:
                            Span = fc.OptPyl_Down_init_NoH(Line,Alts,Span,Htower,Hintsup, Hend,q1,q2,q3,Fo,
                                             Hline_min,Hline_max,Csize,angle_intsup,EAo, E, d,
                                             sup_max,rastLosup,rastTh,rastTv,Tmax,LminSpan,
                                             slope_min_down, slope_max_down,Lmax2,test_hfor,nbconfig)
                            if Span[0,0]==0 or np.sum(Span[:,2])<Lmin:
                                test=0
                                continue
                            indmax=min(int(np.max(Span[:,15]))+2,Line.shape[0])
                            Line2=return_profile(Line[:indmax+1])
                            Falt = InterpolatedUnivariateSpline(Line2[:,0],Line2[:,1])
                            Alts = Falt(np.arange(0.,Lline,0.5))    
                            Span = fc.OptPyl_Down_NoH(Line2,Alts,Span*0,Htower,Hintsup,Hend,q1,q2,q3,Fo,Hline_min,Hline_max,Csize,
                                                       angle_intsup,EAo,E,d,sup_max,rastLosup,rastTh,rastTv,Tmax,LminSpan,min(-slope_min_down,-slope_max_down),
                                                       max(-slope_min_down,-slope_max_down),Lmax2,test_hfor,nbconfig)
                            config=-1
                    ind_max_Line = int(np.max(Span[:,15]))                    
                    if Span[0,0]==0 or np.sum(Span[:,2])<Lmin or Line[ind_max_Line,8]==0:
                        test=0
                        continue                    
                    nbintsup = np.sum(Span[:,0]>0)-1
                    ### Save Line carac
                    Line = Line[0:ind_max_Line+1]
                    Lline = Line[ind_max_Line,0]                    
                    if test_vp or test_vam:    
                        if RoadState==2:
                            Distance_moyenne,Surface,Vtot,VAM,Rast_couv = fc.get_line_carac_vol(coordX,coordY,az,Csize,ncols,nrows,Lline,Row_ext,Col_ext,D_ext,Forest,Rast_couv,Vol_ha,Vol_AM)
                        else: 
                            Distance_moyenne,Surface,Vtot,VAM,Rast_couv2 = fc.get_line_carac_vol(coordX,coordY,az,Csize,ncols,nrows,Lline,Row_ext,Col_ext,D_ext,Forest,Rast_couv2,Vol_ha,Vol_AM)
                    else:
                        if RoadState==2:
                            Distance_moyenne,Surface,Rast_couv = fc.get_line_carac_simple(coordX,coordY,az,Csize,ncols,nrows,Lline,Row_ext,Col_ext,D_ext,Forest,Rast_couv)
                        else:
                            Distance_moyenne,Surface,Rast_couv2 = fc.get_line_carac_simple(coordX,coordY,az,Csize,ncols,nrows,Lline,Row_ext,Col_ext,D_ext,Forest,Rast_couv2)
                    #pixel, direction
                    Tab[id_line,0],Tab[id_line,1] = pixel,az
                    #Xstart,Ystart,Zstart,Hcable_start
                    Tab[id_line,2],Tab[id_line,3],Tab[id_line,4],Tab[id_line,5]=Line[0,3],Line[0,4],Line[0,1],Htower
                    #Xend,Yend,Zend,Hcable_end
                    Tab[id_line,6],Tab[id_line,7],Tab[id_line,8],Tab[id_line,9] = Line[ind_max_Line,3],Line[ind_max_Line,4],Line[ind_max_Line,1],Span[nbintsup,14]
                    #Road existing or not,Ltot,Config
                    Tab[id_line,10],Tab[id_line,11],Tab[id_line,12] = int(RoadState),int(np.sum(Span[:,2])+0.5),config
                    #Surface foret Dmoy chariot
                    Tab[id_line,13],Tab[id_line,14]=Surface,Distance_moyenne
                    #Vtot IPC
                    if test_vp:
                        Tab[id_line,15]=Vtot
                    #VAM               
                    if test_vam:
                        Tab[id_line,16]=VAM*10
                    #Int sup info  
                    Tab[id_line,17]=nbintsup
                    for pyl in range(0,nbintsup):
                        Tab[id_line,18+5*pyl]=Line[int(Span[pyl,15]),3]#X
                        Tab[id_line,19+5*pyl]=Line[int(Span[pyl,15]),4]#Y
                        Tab[id_line,20+5*pyl]=Line[int(Span[pyl,15]),1]#Alts
                        Tab[id_line,21+5*pyl]=Span[pyl,14]#Hcable
                        Tab[id_line,22+5*pyl]=atan(abs(Span[pyl,3]-Span[pyl+1,3]))*Span[pyl,10]+Fo#Press
                    id_line+=1
                    if id_line == 1000000:                        
                        np.save(Dir_temp+"Tab"+str(Tab_nb)+".npy",Tab[Tab[:,11]>0])
                        File_Tab.append(Dir_temp+"Tab"+str(Tab_nb)+".npy")
                        Tab_nb +=1
                        id_line=0
                        Tab = np.zeros((1000000,18+5*sup_max),dtype=np.int)

   
    if language=='FR':
        print("\n    - Sauvegarde des resultats")
    else:
        print("\n    - Saving the results")
   
    ### Save Forest,Vol_ha,VolAm,Pente
    np.save(Rspace_sel+"Forest.npy",Forest)
    if test_vp:
        np.save(Rspace_sel+"Vol_ha.npy",Vol_ha)
    if test_vam:
        np.save(Rspace_sel+"Vol_AM.npy",Vol_AM)
    np.save(Rspace_sel+"Pente.npy" ,Pente)
    np.save(Rspace_sel+"Lien_RF_c.npy",Lien_RF)
    ### Del useless    
    try:
        del Line2
    except:
        pass
    del rastLosup,rastTh,rastTv,Row_line,Col_line,D_line,Nbpix_line,CoordRoute,Aerian_obs,Lien_RF
    gc.collect()
    ### Save results
    Tab = Tab[Tab[:,11]>0]
    if Tab_nb>0:
        for files in File_Tab:
            Tabbis = np.load(files)
            Tab = np.concatenate((Tab,Tabbis))
    np.save(Rspace_sel+"Tab_all_lines.npy",Tab)    
    
    if testExist :        
        Rast_couv += 10*Rast_couv2
        Rast_couv[Rast_couv==11]=2
        Rast_couv[Rast_couv==1]=2
        Rast_couv[Rast_couv==10]=1
    
    if not test_vp:
        Vol_ha=np.zeros_like(MNT)    
    else:
        Vol_ha = load_float_raster_simple(file_Vol_ha) 
    generate_info_cable_simu(Rspace_c,Tab,language,Rast_couv,Vol_ha,Csize,Forest,Pente,Pente_max_bucheron)
    
    ### Del useless   
    del Forest,Pente,Line,Alts,Span,MNT,Fin_ligne_forcee,Aspect,Falt
    try:
        del Vol_ha,Vol_AM,Rast_couv2 
    except:
        pass
    
    #Save Global res        
    if language=='FR':
        header = 'ID_pixel Azimuth X_debut Y_debut Alt_debut Hcable_debut X_fin Y_fin Alt_fin Hcable_fin '
        header +='Etat_RouteFor Longueur_reelle Configuration '
        header +='Surface_foret Distance_moy_chariot Volume_total VAM NB_int_sup'
        for num in range(1,sup_max+1):
            header +=' '+'Xcoord_intsup'+str(num)+' Ycoord_intsup'+str(num)+' Alt_intsup'+str(num)
            header +=' '+'Hcable_intsup'+str(num)+' Pression_intsup'+str(num)
        filename=Rspace_c+"Database_toutes_lignes.gzip"
        shape_name = Rspace_c+"Toutes_les_lignes.shp"
        rast_name = Rspace_c+'Zone_accessible'
        
    else:
        header = 'ID_pixel Azimuth X_Start Y_Start Elevation_Start Hcable_Start X_End Y_End Elevation_End Hcable_End '
        header +='Existing_road Cable_length Configuration '
        header +='Forest_area Carriage_average_distance Volume_total ATV NB_int_sup'
        for num in range(1,sup_max+1):
            header +=' '+'Xcoord_intsup'+str(num)+' Ycoord_intsup'+str(num)+' Elevation_intsup'+str(num)
            header +=' '+'Hcable_intsup'+str(num)+' Pression_intsup'+str(num)
        filename=Rspace_c+"Database_All_lines.gzip"
        shape_name = Rspace_c+"All_lines.shp"
        rast_name = Rspace_c+'Accessible_area'
            
    
    ArrayToGtiff(Rast_couv,rast_name,Extent,nrows,ncols,Csize,road_network_proj,0,'UINT8')
    header+='\n'
    save_integer_ascii(filename,header,Tab)
    source_src=get_source_src(file_shp_Cable_dep) 
    Line_to_shapefile(Tab[0:2],Rspace_sel+"info_proj.shp",source_src,0,language)
    if Tab.shape[0]<1000000:         
        Line_to_shapefile(Tab,shape_name,source_src,prelevement,language)
    
    ##############################################################################################################################################
    ### 3. CREATE SIMULATION PARAMETER FILE
    ##############################################################################################################################################
    str_duree,str_fin,str_debut=heures(Hdebut,language)
    
    if language == 'FR':
        if Carriage_type==1:
            carriage_name = 'Automoteur'
        else:
            carriage_name = 'Classique'
        if Cable_type <3:
            cable_name= 'Cable mat'
        else:
            cable_name= 'Cable long/conventionnel'
        
        file_name = str(Rspace_c)+"Parametre_simulation.txt"
        resume_texte = "SYLVACCESS - CABLE\n\n\n"
        resume_texte = resume_texte+"Version du programme: 3.5.1 de 12/2021\n"
        resume_texte = resume_texte+"Auteur: Sylvain DUPIRE. Irstea\n\n"
        resume_texte = resume_texte+"Date et heure de lancement du script:                                      "+str_debut+"\n"
        resume_texte = resume_texte+"Date et heure a la fin de l'execution du script:                           "+str_fin+"\n"
        resume_texte = resume_texte+"Temps total d'execution du script:                                         "+str_duree+"\n\n"
        resume_texte = resume_texte+"PROPRIETES DU MATERIEL MODELISE:\n"
        resume_texte = resume_texte+"   - Type de machine:                                                      "+str(cable_name)+"\n"
        resume_texte = resume_texte+"   - Hauteur du mat ou du cable porteur au niveau de la place de depot:    "+str(Htower)+" m\n"
        resume_texte = resume_texte+"   - Nombre maximum de support(s) intermediaire(s):                        "+str(sup_max)+"\n"
        resume_texte = resume_texte+"   - Longueur maximale du cable porteur:                                   "+str(Lmax)+" m\n"
        resume_texte = resume_texte+"   - Longueur minimale d'une ligne:                                        "+str(Lmin)+" m\n"
        resume_texte = resume_texte+"   - Longueur minimale entre deux supports:                                "+str(LminSpan)+" m\n"
        resume_texte = resume_texte+"   - Type de chariot:                                                      "+str(carriage_name)+"\n"
        resume_texte = resume_texte+"   - Masse a vide du chariot:                                              "+str(Pchar)+" kg\n"
        resume_texte = resume_texte+"   - Masse maximale de la charge:                                          "+str(Load_max)+" kg\n"
        if Carriage_type==1:   
            resume_texte = resume_texte+"   - Pente max du cable porteur pour un debardage vers l'amont:            "+str(slope_Wliner_up)+" %\n"    
            resume_texte = resume_texte+"   - Pente max du cable porteur pour un debardage vers l'aval:             "+str(slope_Wliner_down)+" %\n"   
        else: 
            resume_texte = resume_texte+"   - Pente min du cable porteur pour que le chariot descende par gravite:  "+str(slope_grav)+" %\n"  
        resume_texte = resume_texte+"\n"
        resume_texte = resume_texte+"PROPRIETES DU CABLE PORTEUR:\n"    
        resume_texte = resume_texte+"   - Diametre du cable porteur:                                            "+str(d)+" mm\n"
        resume_texte = resume_texte+"   - Masse lineique du cable porteur:                                      "+str(q1)+" kg.m-1\n"
        resume_texte = resume_texte+"   - Module de Young (Elasticite):                                         "+str(E)+" N.mm-2\n"
        resume_texte = resume_texte+"   - Tension de rupture du cable porteur                                   "+str(rupt_res)+" kgF\n\n"
        if Carriage_type!=1:
            resume_texte = resume_texte+"PROPRIETES DES CABLES TRACTEUR ET RETOUR:\n"  
            resume_texte = resume_texte+"   - Masse lineique du cable tracteur:                                     "+str(q2)+" kg.m-1\n"
            resume_texte = resume_texte+"   - Masse lineique du cable retour:                                       "+str(q3)+" kg.m-1\n"
            resume_texte = resume_texte+"\n"        
        resume_texte = resume_texte+"PARAMETRES DE MODELISATION:\n"
        resume_texte = resume_texte+"   - Distance laterale de pechage des bois:                                "+str(Lhor_max)+" m\n"
        resume_texte = resume_texte+"   - Hauteur du cable porteur au niveau des pylone intermediaire:          "+str(Hintsup)+" m\n"
        resume_texte = resume_texte+"   - Hauteur du cable porteur en fin de ligne:                             "+str(Hend)+" m\n"
        resume_texte = resume_texte+"   - Hauteur minimale du cable en tout point (en charge):                  "+str(Hline_min)+" m\n"
        resume_texte = resume_texte+"   - Hauteur maximale du cable en tout point:                              "+str(Hline_max)+" m\n"
        resume_texte = resume_texte+"   - Angle maximum du cable porteur au niveau d'un pylone intermediaire:   "+str(Max_angle)+" degres\n"
        resume_texte = resume_texte+"   - Facteur de securite:                                                  "+str(safe_fact)+"\n"
        resume_texte = resume_texte+"   - Valeur de l'angle de frottement:                                      "+str(coeff_frot)+" rad\n\n"
        resume_texte = resume_texte+"   - Resolution du MNT utilise:                                            "+str(Csize)+" m\n"
        resume_texte = resume_texte+"   - Prelevement du volume sur pied applique:                              "+str(prelevement*100)+" %\n"
        try:
            resume_texte = resume_texte+"   - Projection:                                                           "+str(proj.GetAttrValue("PROJCS", 0))+"\n"
        except:
            resume_texte = resume_texte+"   - Projection:                                                           inconnue\n"
        if Dir_Obs_cable=="":
            reponse = "Non"
        else:
            reponse = "Oui"
        resume_texte = resume_texte+"   - Prise en compte d'obstacle pour le cable:                             "+str(reponse)+"\n"
        if file_Vol_ha=="":
            reponse = "Non"
        else:
            reponse = "Oui"
        resume_texte = resume_texte+"   - Information sur le volume de bois fournie en entree:                  "+str(reponse)+"\n"
    
    elif language == 'EN':
        if Carriage_type==1:
            carriage_name = 'Self-motorized'
        else:
            carriage_name = 'Classical'
        if Cable_type <3:
            cable_name= 'Cable tower'
        else:
            cable_name= 'Long/conventional cable'
        
        file_name = str(Rspace_c)+"Parameter_of_simulation.txt"
        resume_texte = "SYLVACCESS - CABLE\n\n\n"
        resume_texte = resume_texte+"Program version: 3.5.1 - 2021/12\n"
        resume_texte = resume_texte+"Author: Sylvain DUPIRE. Irstea\n\n"
        resume_texte = resume_texte+"Date and time when launching the script:                          "+str_debut+"\n"
        resume_texte = resume_texte+"Date and time at the end of execution of the script:              "+str_fin+"\n"
        resume_texte = resume_texte+"Total execution time of the script:                               "+str_duree+"\n\n"
        resume_texte = resume_texte+"TYPE OF MATERIAL MODELED:\n"
        resume_texte = resume_texte+"   - Type of machine:                                             "+str(cable_name)+"\n"
        resume_texte = resume_texte+"   - Height of the skyline at landing place:                      "+str(Htower)+" m\n"
        resume_texte = resume_texte+"   - Maximum number of intermediate support(s):                   "+str(sup_max)+"\n"
        resume_texte = resume_texte+"   - Maximum length of the skyline:                               "+str(Lmax)+" m\n"
        resume_texte = resume_texte+"   - Minimum length of a line:                                    "+str(Lmin)+" m\n"
        resume_texte = resume_texte+"   - Minimal length between two supports:                         "+str(LminSpan)+" m\n"
        resume_texte = resume_texte+"   - Carriage type:                                               "+str(carriage_name)+"\n"
        resume_texte = resume_texte+"   - Carriage weight when empty:                                  "+str(Pchar)+" kg\n"
        resume_texte = resume_texte+"   - Maximum weight of the load:                                  "+str(Load_max)+" kg\n"
        if Carriage_type==1:   
            resume_texte = resume_texte+"   - Maximum slope of the skyline for an uphill yarding:          "+str(slope_Wliner_up)+" %\n"    
            resume_texte = resume_texte+"   - Maximum slope of the skyline for an downhill yarding:        "+str(slope_Wliner_down)+" %\n"   
        else:
            resume_texte = resume_texte+"   - Minimum slope for a gravity descent of the carriage:         "+str(slope_grav)+" %\n"  
        resume_texte = resume_texte+"\n"
        resume_texte = resume_texte+"SKYLINE PROPERTIES:\n"    
        resume_texte = resume_texte+"   - Skyline diameter:                                            "+str(d)+" mm\n"
        resume_texte = resume_texte+"   - Skyline self-weight:                                         "+str(q1)+" kg.m-1\n"
        resume_texte = resume_texte+"   - Young Modulus (Elasticity):                                  "+str(E)+" N.mm-2\n"
        resume_texte = resume_texte+"   - Skyline maximum tensile force                                "+str(rupt_res)+" kgF\n\n"
        if Carriage_type!=1:
            resume_texte = resume_texte+"MAINLINE AND HAULBACK LINE PROPERTIES:\n"  
            resume_texte = resume_texte+"   - Mainline self-weight:                                        "+str(q2)+" kg.m-1\n"
            resume_texte = resume_texte+"   - Haulback line self-weight:                                   "+str(q3)+" kg.m-1\n"
            resume_texte = resume_texte+"\n"        
        resume_texte = resume_texte+"MODELING PARAMETERS:\n"
        resume_texte = resume_texte+"   - Lateral skidding distance:                                   "+str(Lhor_max)+" m\n"
        resume_texte = resume_texte+"   - Skyline height at intermediaite support(s):                  "+str(Hintsup)+" m\n"
        resume_texte = resume_texte+"   - Skyline height at tailspar:                                  "+str(Hend)+" m\n"
        resume_texte = resume_texte+"   - Minimun height of the load along the profile:                "+str(Hline_min)+" m\n"
        resume_texte = resume_texte+"   - Maximun height of the skyline along the profile:             "+str(Hline_max)+" m\n"
        resume_texte = resume_texte+"   - Maximum angle of the skyline at intermediate support level:  "+str(Max_angle)+" degrees\n"
        resume_texte = resume_texte+"   - Safety coefficient:                                          "+str(safe_fact)+"\n"
        resume_texte = resume_texte+"   - Friction angle value:                                        "+str(coeff_frot)+" rad\n\n"
        resume_texte = resume_texte+"   - DTM resolution:                                              "+str(Csize)+" m\n"
        resume_texte = resume_texte+"   - Proportion of wood volume removed:                           "+str(prelevement*100)+" %\n"
        try:
            resume_texte = resume_texte+"   - Projection:                                                  "+str(proj.GetAttrValue("PROJCS", 0))+"\n"
        except:
            resume_texte = resume_texte+"   - Projection:                                                  Unknown\n"
        if Dir_Obs_cable=="":
            reponse = "No"
        else:
            reponse = "Yes"
        resume_texte = resume_texte+"   - Modeling done taking into account cable obstacles:           "+str(reponse)+"\n"
        if file_Vol_ha=="":
            reponse = "No"
        else:
            reponse = "Yes"
        resume_texte = resume_texte+"   - Wood volume information given as input:                      "+str(reponse)+"\n"
    
    fichier = open(file_name, "w")
    fichier.write(resume_texte)
    fichier.close()
    
    file_name = Rspace_sel+"info_Lhormax.txt"
    fichier = open(file_name, "w")
    fichier.write(str(Lhor_max))
    fichier.close()
    
    
    if language=='FR':
        print("\nToutes les lignes possibles ont ete testees.\n")     
    else:
        print("\nAll the possible cable line have been processed.\n")
    ##############################################################################################################################################
    ### 4. SELECTION OF BEST LINE IF CHECKED
    ##############################################################################################################################################
    if test_cable_optimise:
        line_selection(Rspace_c,w_list,lim_list,0,file_shp_Foret,file_Vol_ha,file_Vol_AM,language,Lhor_max,prelevement,Pente_max_bucheron)



