# -*- coding: utf8 -*-
"""
Software: Sylvaccess
File: Sylvaccess_0_main3.py
Copyright (C) Sylvain DUPIRE 2021
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@inrae.fr
Version: 3.5.1
Date: 2021/12/17
License :  GNU-GPL V3

-----------------------------------------------------------------------
Français: (For english see above)

Ce fichier fait partie de Sylvaccess qui est un programme informatique 
servant à cartographier automatiquement les forêts accessibles en fonction de 
différents modes d'exploitations (skidder, porteur, débardage par câble aérien). 

Ce logiciel est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
suivant les termes de la GNU General Public License telle que publiée par la 
Free Software Foundation ; soit la version 3 de la licence, soit (à votre gré) 
toute version ultérieure.
Sylvaccess est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE 
GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION 
à UN BUT PARTICULIER. Consultez la GNU General Public License pour plus de 
détails.
Vous devez avoir reçu une copie de la GNU General Public License en même temps 
que Sylvaccess ; si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.

-----------------------------------------------------------------------
English

This file is part of Sylvaccess which is a computer program whose purpose 
is to automatically map forest accessibility according to different forest 
operation systems (skidder, forwarder, cable yarding)..

Sylvaccess is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sylvaccess.  If not, see <https://www.gnu.org/licenses/>.
"""

import sys,os,gc,math,datetime,time,traceback
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QMainWindow, QApplication,QDialog
from sylvaccess_form_3 import Ui_Dialog
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline
import scipy.integrate
from sylvaccess_fonctions import *
from Sylvaccess_1_skidder import *
from Sylvaccess_2_cable import *
from Sylvaccess_3_forwarder import *
import sylvaccess_cython3 as fc

class Interface_sylvaccess(QDialog):
    def __init__(self):  
        QDialog.__init__(self)  
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)  
        self.ui.buttonBox.rejected.connect(self.reject)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    Interface = Interface_sylvaccess()    
    Interface.show()    
    def get_variable():
        # Get variable from general tab
        global test_Skidder,test_Cable,test_Forwarder,Pente_max_bucheron,test_cable_optimise
        test_Skidder = Interface.ui.YN_Skidder.isChecked()
        test_Cable = Interface.ui.YN_Cable.isChecked()
        test_Forwarder = Interface.ui.YN_Forwarder.isChecked()
        test_cable_optimise = Interface.ui.YN_Cab_optimise.isChecked()
        Pente_max_bucheron = Interface.ui.Pmax_buch.value()
        # Get path to spatial files            
        global Wspace,Rspace,file_MNT,file_shp_Desserte,file_shp_Foret,file_Vol_ha
        global Dir_Obs_cable,file_Htree,file_Vol_AM,language,Dir_Obs_forwarder
        global Dir_Full_Obs_skidder,Dir_Partial_Obs_skidder,file_shp_Cable_dep
        language = Interface.ui.language_value
        Wspace = str(Interface.ui.Wspace_path.text())
        if Wspace!='':Wspace+="/"
        Rspace = str(Interface.ui.Rspace_path.text())
        if Rspace!='':Rspace+="/"
        file_MNT = str(Interface.ui.MNT_path.text())
        file_shp_Desserte = str(Interface.ui.Desserte_path.text())
        file_shp_Foret = str(Interface.ui.Forest_path.text())
        file_Vol_ha = str(Interface.ui.Vol_path.text())
        file_Vol_AM = str(Interface.ui.vam_path.text())
        file_Htree = str(Interface.ui.Hmoy_path.text())
        file_shp_Cable_dep=str(Interface.ui.Cable_dep_path.text())
        Dir_Obs_cable = str(Interface.ui.Obs_cable_path.text())
        if Dir_Obs_cable!='':Dir_Obs_cable+="/"
        Dir_Partial_Obs_skidder = str(Interface.ui.Partial_Obs_skidder_path.text())
        if Dir_Partial_Obs_skidder!='':Dir_Partial_Obs_skidder+="/"
        Dir_Full_Obs_skidder = str(Interface.ui.Full_Obs_skidder_path.text())
        if Dir_Full_Obs_skidder!='':Dir_Full_Obs_skidder+="/"
        Dir_Obs_forwarder = str(Interface.ui.Obs_forwarder_path.text())
        if Dir_Obs_forwarder!='':Dir_Obs_forwarder+="/"
        # Get Skidder parameters  
        global Dtreuil_max_up,Dtreuil_max_down,Dmax_train_near_for,Pente_max_skidder
        global Pmax_amont,Pmax_aval,Option_Skidder,Skid_Debclass
        if test_Skidder:           
            Dtreuil_max_up = Interface.ui.Skid_Damont.value()
            Dtreuil_max_down = Interface.ui.Skid_Daval.value()
            Dmax_train_near_for = Interface.ui.Skid_Dmax_Hforest.value()
            Pente_max_skidder = Interface.ui.Skid_Pmax.value()
            Pmax_amont = Interface.ui.Skid_Pmax_amont.value()
            Pmax_aval = Interface.ui.Skid_Pmax_aval.value()
            if Interface.ui.Skidder_option1.isChecked(): Option_Skidder=1
            else:Option_Skidder=2
            Skid_Debclass = Interface.ui.Skid_borneDeb.toPlainText()
        
        # Get Forwarder parameters
        global Forw_angle_incl,Forw_angle_up,Forw_angle_down
        global Forw_portee,Forw_Lmax,Forw_Dmax_out_for,Forw_Debclass
        if test_Forwarder:            
            Forw_angle_incl = Interface.ui.Forw_angle_incl.value()
            Forw_angle_up = Interface.ui.Forw_angle_up.value()
            Forw_angle_down = Interface.ui.Forw_angle_down.value()
            Forw_portee = Interface.ui.Forw_portee.value()
            Forw_Lmax = Interface.ui.Forw_Lmax.value()
            Forw_Dmax_out_for = Interface.ui.Forw_Dmax_out_for.value()
            Forw_Debclass = Interface.ui.Forw_borneDeb.toPlainText()
        
        # Get Cable parameters    
        global Lmax,Lmin,Lhor_max,slope_Wliner_up,slope_Wliner_down,slope_grav,Cable_type
        global Htower,Pchar,slope_min,E,q1,d,rupt_res,Hintsup,Hend,Hline_min,Hline_max
        global safe_fact,angle_sup,precision,Pmax,Load_max,Max_angle,LminSpan,VariaH
        global q2,q3,coeff_frot,slope_trans,angle_transv,Carriage_type,sup_max,prelevement
        global Cabsel_Lhor_max,new_calc,Lslope,PropSlope
        global Dir_cable_sel,Cabsel_For,Cabsel_Vha,Cabsel_Vam,w_list,lim_list
        global lim_Sfor,lim_Nsup,lim_Sens,lim_Ltot,lim_Vtot,lim_IPC,lim_VAM,lim_Dchar,lim_Cost
        global w_Sfor,w_Nsup,w_Sens,w_Ltot,w_Vtot,w_IPC,w_VAM,w_Dchar,w_Cost
        new_calc=0
        Cabsel_For=""
        if test_Cable:  
            Cable_type = Interface.ui.Cable_type.currentIndex()            
            sup_max = Interface.ui.Cab_intsupmax.value()
            Lmax = Interface.ui.Cab_Lmax.value()
            Lmin = Interface.ui.Cab_Lmin.value()
            Htower = Interface.ui.Cab_Htower.value()
            Carriage_type = Interface.ui.Carriage_type.currentIndex()
            Pchar = Interface.ui.Pchar.value()
            slope_grav = Interface.ui.Cab_slope_min.value()
            slope_Wliner_up = Interface.ui.Cab_pente_max_amont.value()
            slope_Wliner_down = Interface.ui.Cab_pente_max_aval.value()
            E = Interface.ui.Cab_E.value()
            q1 = Interface.ui.Cabl_m.value()
            d = Interface.ui.Cab_d.value()
            rupt_res = Interface.ui.Cab_Trupt.value()
            Hintsup = Interface.ui.Cab_Hintsup.value()
            Hend = Interface.ui.Cab_Hfin.value()
            Lhor_max = Interface.ui.Cab_Lpech.value()
            Hline_min = Interface.ui.Cab_Hline_min.value()
            Hline_max = Interface.ui.Cab_Hline_max.value()
            safe_fact = Interface.ui.Cab_safety_fact.value()
            #Max_angle = Interface.ui.Cab_Max_angle.value()
            Max_angle = 30
            precision = Interface.ui.Cab_precision.value()
            Load_max = Interface.ui.Cab_Pmax.value()
            #LminSpan = Interface.ui.Cab_LminSpan.value()
            LminSpan = 50
            VariaH = Interface.ui.Check_Hcable.isChecked()
            ### Those are not in the interface yet                       
            q2 = 0.5            
            q3 = 0.5            
            coeff_frot = 0.15
            slope_trans= 30 #%
            angle_transv = 60     
            Lslope=75
            PropSlope=0.15           
                    
            Dir_cable_sel = str(Interface.ui.Wspace_path.text())
            if Dir_cable_sel!='':Dir_cable_sel+="/"            
            Cabsel_For = str(Interface.ui.Cable_forest_path.text())
            Cabsel_Vha = str(Interface.ui.cable_vha_path.text())
            Cabsel_Vam = str(Interface.ui.cable_vam_path.text())
            new_calc=Interface.ui.Cab_new_calc.isChecked()*1
            Cabsel_Lhor_max = Interface.ui.cable_sel_Lhor_max.value() 
            
            if test_cable_optimise:   
                prelevement = Interface.ui.prelevement.value()*0.01
            else:
                prelevement = 0.35
            
            #limits
            lim_Sfor =  Interface.ui.lim_surf.value()
            lim_Nsup =  Interface.ui.lim_intsup.value()
            lim_Sens =  Interface.ui.lim_debdir.value()
            lim_Ltot =  Interface.ui.lim_Long.value()
            lim_Vtot =  Interface.ui.lim_Vol.value()
            lim_IPC =   Interface.ui.lim_IPC.value()
            lim_VAM =   Interface.ui.lim_VAM.value()
            lim_Dchar = Interface.ui.lim_Dchar.value()
            #lim_Cost =  Interface.ui.lim_Cost.value()        
            
            #Weight
            w_Sfor =  Interface.ui.w_Surf.value()       
            w_Nsup =  Interface.ui.w_intsup.value()   
            w_Sens =  Interface.ui.w_debdir.value()          
            w_Ltot =  Interface.ui.w_Long.value()       
            w_Vtot =  Interface.ui.w_Vol.value()         
            w_IPC =   Interface.ui.w_IPC.value()         
            w_VAM =   Interface.ui.w_VAM.value()           
            w_Dchar = Interface.ui.w_Dchar.value()        
            #w_Cost =  Interface.ui.w_Cost.value()    

            w_list = [w_Sfor,w_Nsup,w_Sens,w_Ltot,w_Vtot,w_IPC,w_VAM,w_Dchar,0]
            lim_list = [lim_Sfor,lim_Nsup,lim_Sens,lim_Ltot,lim_Vtot,lim_IPC,lim_VAM,lim_Dchar,0]

        # Line optimisation only
        if not test_Cable and test_cable_optimise:           
        
            Dir_cable_sel = str(Interface.ui.Cable_dir_path.text())
            if Dir_cable_sel!='':Dir_cable_sel+="/"            
            Cabsel_For = str(Interface.ui.Cable_forest_path.text())
            Cabsel_Vha = str(Interface.ui.cable_vha_path.text())
            Cabsel_Vam = str(Interface.ui.cable_vam_path.text())
            new_calc=Interface.ui.Cab_new_calc.isChecked()*1
            Cabsel_Lhor_max = Interface.ui.cable_sel_Lhor_max.value()                           
            prelevement = Interface.ui.prelevement.value()*0.01
                        
            #limits
            lim_Sfor =  Interface.ui.lim_surf.value()
            lim_Nsup =  Interface.ui.lim_intsup.value()
            lim_Sens =  Interface.ui.lim_debdir.value()
            lim_Ltot =  Interface.ui.lim_Long.value()
            lim_Vtot =  Interface.ui.lim_Vol.value()
            lim_IPC =   Interface.ui.lim_IPC.value()
            lim_VAM =   Interface.ui.lim_VAM.value()
            lim_Dchar = Interface.ui.lim_Dchar.value()
            #lim_Cost =  Interface.ui.lim_Cost.value()     
            lim_Cost = 0
            
            #Weight
            w_Sfor =  Interface.ui.w_Surf.value()       
            w_Nsup =  Interface.ui.w_intsup.value()   
            w_Sens =  Interface.ui.w_debdir.value()          
            w_Ltot =  Interface.ui.w_Long.value()       
            w_Vtot =  Interface.ui.w_Vol.value()         
            w_IPC =   Interface.ui.w_IPC.value()         
            w_VAM =   Interface.ui.w_VAM.value()           
            w_Dchar = Interface.ui.w_Dchar.value()        
            #w_Cost =  Interface.ui.w_Cost.value()    
            w_Cost = 0

            w_list =   [w_Sfor  ,w_Nsup  ,w_Sens  ,w_Ltot  ,w_Vtot  ,w_IPC  ,w_VAM  ,w_Dchar,w_Cost]
            lim_list = [lim_Sfor,lim_Nsup,lim_Sens,lim_Ltot,lim_Vtot,lim_IPC,lim_VAM,lim_Dchar,lim_Cost]
        
        try:os.mkdir(Rspace)
        except:pass 
        Interface.close()
        try:           
            #Check file entries before processing
            test,mess = check_files(file_MNT,file_shp_Desserte,file_shp_Foret,file_Vol_ha,file_Vol_AM,
                                    file_Htree,language,test_Cable,test_Skidder,test_Forwarder,
                                    test_cable_optimise,new_calc,Cabsel_For,file_shp_Cable_dep)

            if not test:
                # There is a problem with spatial inputs
                print(mess)
            else:            
                #Run Sylvaccess 
                if (test_Skidder + test_Forwarder) > 0:
                    # Verifie si une partie de la desserte correspond a un projet
                    testExist = check_field_EXIST(file_shp_Desserte,"EXIST") 
                    
                    ###################################################################################################################
                    ### Si pas de projet desserte
                    ###################################################################################################################
                    if not testExist: 
                        if test_Skidder:  
                            process_skidder(Wspace,Rspace,file_MNT,Dtreuil_max_up,Dtreuil_max_down,
                                            Dmax_train_near_for,Pente_max_skidder,Pmax_amont,Pmax_aval,
                                            Option_Skidder,Pente_max_bucheron,Dir_Full_Obs_skidder,
                                            Dir_Partial_Obs_skidder,language,file_shp_Desserte,file_shp_Foret,
                                            file_Vol_ha,Skid_Debclass)                    
                            gc.collect()
                        
                        if test_Forwarder:
                            process_forwarder(Wspace,Rspace,file_MNT,Pente_max_bucheron,Forw_angle_incl,Forw_angle_up,
                                              Forw_angle_down,Forw_portee,language,file_shp_Desserte,file_shp_Foret,
                                              Dir_Obs_forwarder,Forw_Lmax,Forw_Dmax_out_for,file_Vol_ha,Forw_Debclass)
                            gc.collect()
                    
                    ###################################################################################################################
                    ### Si projet desserte
                    ###################################################################################################################
                    else:                        
                        file_shp_Desserte_Exist = create_new_road_network(file_shp_Desserte,Wspace)
                        
                        #Premiere simu sans projet
                        if language=="FR":
                            print("\nSIMULATION DEPUIS LA DESSERTE EXISTANTE")
                        else:
                            print("\nSIMULATION FROM EXISTING ROAD NETWORK")
                        if test_Skidder:   
                            try:os.mkdir(Rspace+"Skidder/")
                            except:pass
                            process_skidder(Wspace,Rspace+"Skidder/",file_MNT,Dtreuil_max_up,Dtreuil_max_down,
                                            Dmax_train_near_for,Pente_max_skidder,Pmax_amont,Pmax_aval,
                                            Option_Skidder,Pente_max_bucheron,Dir_Full_Obs_skidder,
                                            Dir_Partial_Obs_skidder,language,file_shp_Desserte_Exist,file_shp_Foret,
                                            file_Vol_ha,Skid_Debclass)                    
                            gc.collect()
                            if language=="FR":
                                projdir = Rspace+"Skidder/1_Existant/"
                            else:
                                projdir = Rspace+"Skidder/1_Existing/"
                            
                            os.rename(Rspace+"Skidder/Skidder/",projdir)
                        
                        if test_Forwarder:
                            if language=='EN':
                                Rspace_f = Rspace+"Forwarder/"
                                old=Rspace+"Forwarder/Forwarder/"
                                new= Rspace+"Forwarder/1_Existing/"
                            else:
                                Rspace_f = Rspace+"Porteur/"
                                old=Rspace+"Porteur/Porteur/"
                                new= Rspace+"Porteur/1_Existant/"  
                            try:os.mkdir(Rspace_f)
                            except:pass
                            process_forwarder(Wspace,Rspace_f,file_MNT,Pente_max_bucheron,Forw_angle_incl,Forw_angle_up,
                                              Forw_angle_down,Forw_portee,language,file_shp_Desserte_Exist,file_shp_Foret,
                                              Dir_Obs_forwarder,Forw_Lmax,Forw_Dmax_out_for,file_Vol_ha,Forw_Debclass)
                            gc.collect()
                            os.rename(old,new)  
                            
                        os.remove(Wspace+"Temp/Lien_piste.npy")
                        os.remove(Wspace+"Temp/Lien_RF.npy")
                        os.remove(Wspace+"Temp/Tab_res_pub.npy")
                        os.remove(Wspace+"Temp/Route_for.npy")
                        os.remove(Wspace+"Temp/Piste.npy")
                        
                        #Deuxieme simu avec projet
                        if language=="FR":
                            print("\nSIMULATION INCLUANT LE PROJET DE DESSERTE")
                        else:
                            print("\nSIMULATION INCLUDING PROJECT OF ROADS")
                        if test_Skidder: 
                            process_skidder(Wspace,Rspace+"Skidder/",file_MNT,Dtreuil_max_up,Dtreuil_max_down,
                                            Dmax_train_near_for,Pente_max_skidder,Pmax_amont,Pmax_aval,
                                            Option_Skidder,Pente_max_bucheron,Dir_Full_Obs_skidder,
                                            Dir_Partial_Obs_skidder,language,file_shp_Desserte,file_shp_Foret,
                                            file_Vol_ha,Skid_Debclass)                    
                            gc.collect()
                            if language=="FR":
                                projdir = Rspace+"Skidder/2_Projet/"
                            else:
                                projdir = Rspace+"Skidder/2_Project/"
                            
                            os.rename(Rspace+"Skidder/Skidder/",projdir)
                            make_dif_files(language,Rspace,0)     
                            
                        if test_Forwarder:
                            if language=='EN':
                                Rspace_f = Rspace+"Forwarder/"
                                old=Rspace+"Forwarder/Forwarder/"
                                new= Rspace+"Forwarder/2_Project/"
                            else:
                                Rspace_f = Rspace+"Porteur/"
                                old=Rspace+"Porteur/Porteur/"
                                new= Rspace+"Porteur/2_Projet/"                                
                            process_forwarder(Wspace,Rspace_f,file_MNT,Pente_max_bucheron,Forw_angle_incl,Forw_angle_up,
                                              Forw_angle_down,Forw_portee,language,file_shp_Desserte,file_shp_Foret,
                                              Dir_Obs_forwarder,Forw_Lmax,Forw_Dmax_out_for,file_Vol_ha,Forw_Debclass)
                            gc.collect()         
                            os.rename(old,new)   
                            make_dif_files(language,Rspace,1)                       
                
                if test_Cable:
                    process_cable(Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Cable_dep,Dir_Obs_cable,file_Vol_ha,
                                  file_Vol_AM,file_Htree,Pente_max_bucheron,Lmax,Lmin,LminSpan,Htower,Hintsup,Hend,
                                  Lhor_max,Hline_min,Hline_max,sup_max,Carriage_type,Cable_type,slope_grav,Pchar,
                                  slope_Wliner_up,slope_Wliner_down,q1,rupt_res,safe_fact,E,d,Load_max,q2,q3,
                                  Max_angle,coeff_frot,language,precision,prelevement,slope_trans,angle_transv,
                                  test_cable_optimise,w_list,lim_list,VariaH,Lslope,PropSlope)
                    test_cable_optimise=0
                    gc.collect()
                if test_cable_optimise:
                    line_selection(Dir_cable_sel,w_list,lim_list,new_calc,Cabsel_For,Cabsel_Vha,Cabsel_Vam,
                                   language,Cabsel_Lhor_max,prelevement,Pente_max_bucheron) 
                    gc.collect()  
                try:
                    shutil.rmtree(Wspace+"Temp")
                except:
                    pass
        except Exception:    
            log = open(Rspace + 'error_log.txt', 'w')
            traceback.print_exc(file=log)
            log.close()  
            try:
                shutil.rmtree(Wspace+"Temp")
            except:
                pass
        
        time.sleep(60.)
        
    Interface.ui.buttonBox.accepted.connect(get_variable)
    
    sys.exit(app.exec_()) 

