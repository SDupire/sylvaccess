# -*- coding: utf8 -*-
"""
Software: Sylvaccess
File: sylvaccess_fonctions.py
Copyright (C) Sylvain DUPIRE 2021
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@inrae.fr
Version: 3.5.1
Date: 2021/12/17
License :  GNU-GPL V3

-----------------------------------------------------------------------
Français: (For english see above)

Ce fichier fait partie de Sylvaccess qui est un programme informatique 
servant à cartographier automatiquement les forêts accessibles en fonction de 
différents modes d'exploitations (skidder, porteur, débardage par câble aérien). 

Ce logiciel est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
suivant les termes de la GNU General Public License telle que publiée par la 
Free Software Foundation ; soit la version 3 de la licence, soit (à votre gré) 
toute version ultérieure.
Sylvaccess est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE 
GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION 
à UN BUT PARTICULIER. Consultez la GNU General Public License pour plus de 
détails.
Vous devez avoir reçu une copie de la GNU General Public License en même temps 
que Sylvaccess ; si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.

-----------------------------------------------------------------------
English

This file is part of Sylvaccess which is a computer program whose purpose 
is to automatically map forest accessibility according to different forest 
operation systems (skidder, forwarder, cable yarding)..

Sylvaccess is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sylvaccess.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
import os,math,datetime,shutil
from osgeo import gdal,ogr,osr
#import sylvaccess_cython as fc
import sylvaccess_cython3 as fc
import gc
from math import degrees,atan,cos,sin,radians,sqrt
from scipy import spatial

#import multiprocessing
################################################################################################################################################
### General functions
################################################################################################################################################
 
#def exectimeout(timeout, fonc, *args, **kwargs):
#    """exécute fonc(args, kwargs), avec le temps maxi timeout"""
#    pool = multiprocessing.Pool(processes=1) # crée 1 processus
#    result = pool.apply_async(func=fonc, *args, **kwargs) # eval. asynchrone
#    try:
#        return result.get(timeout=timeout) 
#    except multiprocessing.TimeoutError:
#        raise # renvoi de l'exception à l'appelant

def heures(Hdebut,language):
    Hfin = datetime.datetime.now()
    duree = Hfin - Hdebut
    ts = duree.seconds
    nb_days = int(ts/3600./24.)
    ts -= nb_days*3600*24
    nb_hours = int(ts/3600)
    ts -= nb_hours*3600
    nb_minutes = int(ts/60)
    ts -= nb_minutes*60  
    if nb_days>0:
        if language=='FR': 
            str_duree = str(nb_days)+'j '+str(nb_hours)+'h '+str(nb_minutes)+'min '+str(ts)+'s'
        else:
            str_duree = str(nb_days)+'d '+str(nb_hours)+'h '+str(nb_minutes)+'min '+str(ts)+'s'
    elif nb_hours >0:
        str_duree = str(nb_hours)+'h '+str(nb_minutes)+'min '+str(ts)+'s'
    elif nb_minutes>0:
        str_duree = str(nb_minutes)+'min '+str(ts)+'s'
    else:
        str_duree = str(ts)+'s'        
    if language=='FR':
        str_debut = str(Hdebut.day)+'/'+str(Hdebut.month)+'/'+str(Hdebut.year)+' '+str(Hdebut.hour)+':'+str(Hdebut.minute)+':'+str(Hdebut.second)
        str_fin = str(Hfin.day)+'/'+str(Hfin.month)+'/'+str(Hfin.year)+' '+str(Hfin.hour)+':'+str(Hfin.minute)+':'+str(Hfin.second)
    else:
        str_debut = str(Hdebut.year)+'/'+str(Hdebut.month)+'/'+str(Hdebut.day)+' '+str(Hdebut.hour)+':'+str(Hdebut.minute)+':'+str(Hdebut.second)
        str_fin = str(Hfin.year)+'/'+str(Hfin.month)+'/'+str(Hfin.day)+' '+str(Hfin.hour)+':'+str(Hfin.minute)+':'+str(Hfin.second)
    return str_duree,str_fin,str_debut

# Get header from ASCII raster
def get_info_ascii(file_name):
    fs = open(file_name, 'r')
    head_text=''
    line = 1
    while line<7:
        head_text = head_text+fs.readline()
        line=line+1
    fs.close()
    Csize = np.genfromtxt(file_name, dtype=None,usecols=(1))[4]
    return head_text, Csize

# Save integer like ASCII raster
def save_integer_ascii(file_name,head_text,data):
    np.savetxt(file_name, data, fmt='%i', delimiter=' ')
    with open(file_name, "r+") as f:
        old = f.read()
        f.seek(0)
        f.write(head_text + old)
        f.close()

# Save float like ASCII raster
def save_float_ascii(file_name,head_text,data):
    np.savetxt(file_name, data, fmt='%f', delimiter=' ')
    with open(file_name, "r+") as f:
        old = f.read()
        f.seek(0)
        f.write(head_text + old)
        f.close()

# Replace all the terms contained in the dictionary 
def replace_all(text, dic):
    for i, j in dic.iteritems(): text = text.replace(i, j)
    return text

# Clear all globals numpy array over 100 Mo size
def clear_big_nparray():    
    """clear all globals over 100 Mo size and their associated memory space"""
    for uniquevar in [var for var in dir() if isinstance(globals()[var],np.ndarray)]:
        if globals()[uniquevar].nbytes/1000000>50:
            del globals()[uniquevar]
    gc.collect()

# Read info file contained header like raster Ascii 
def read_info(info_file):
    names = np.genfromtxt(info_file, dtype=None,usecols=(0),encoding ='latin1')
    values = np.genfromtxt(info_file, dtype=None,usecols=(1),encoding ='latin1')  
    return list(names),list(values) 


#Chech all spatial entries before processing
def check_files(file_MNT,file_shp_Desserte,file_shp_Foret,file_Vol_ha,file_Vol_AM,file_Htree,language,
                test_Cable,test_Skidder,test_Forwarder,test_cable_optimise,new_calc,Cabsel_For,file_shp_Cable_dep):
    test = 1
    if language=='EN':        
        mess="\nFOLLOWING PROBLEM(S) WERE NOTICED WHILE TESTING SPATIAL INPUTS: \n"
    else:
        mess="\nLES PROBLEMES SUIVANTS ONT ETE IDENTIFIES CONCERNANT LES ENTREES SPATIALES: \n"
    #Check MNT
    if test_Skidder+test_Forwarder+test_Cable>0:
        try:
            names,values,proj,Extent = raster_get_info(file_MNT)   
            if values[5]==None:
                test=0
                if language=='EN':        
                    mess+=" -   DEM Raster: NoData value is not defined\n"
                else:
                    mess+=" -   Raster MNT: Aucune valeur de NoData definie\n" 
        except:
            test=0
            if language=='EN':        
                mess+=" -   DEM Raster: The path is missing or not correct. This raster is compulsory to run Sylvaccess\n"
            else:
                mess+=" -   Raster MNT:  Le chemin d'acces est manquant ou incorrect. Ce raster est obligatoire pour lancer Sylvaccess\n" 
            
    #Check file_shp_Desserte   
    if test_Skidder+test_Forwarder>0:
        try:    
            if not check_field(file_shp_Desserte,"CL_SVAC"):
                test=0
                if language=='EN':        
                    mess+=" -   Road network shapefile: Field 'CL_SVAC' is missing\n"
                else:
                    mess+=" -   Couche desserte: Le champs 'CL_SVAC' est manquant\n"  
            
        except:
            test=0
            if language=='EN':        
                mess+=" -   Road network shapefile: The path is missing or not correct. This shapefile is compulsory to run skidder and forwarder modules\n"
            else:
                mess+=" -   Couche desserte: Le chemin d'acces est manquant ou incorrect. Cette couche est obligatoire pour les modules skidder et porteur\n" 
        

    #Check file_shp_Cable_Dep    
    if test_Cable:   
        try: 
            if not check_field(file_shp_Cable_dep,"CABLE"):
                test=0
                if language=='EN':        
                    mess+=" -   Road network shapefile: Field 'CABLE' is missing\n"
                else:
                    mess+=" -   Couche desserte: Le champs 'CABLE' est manquant\n"  
        except:
            test=0
            if language=='EN':        
                mess+=" -   Potential cable starts shapefile: The path is missing or not correct. This shapefile is compulsory to run cable module\n"
            else:
                mess+=" -   Couche départs de cable potentiels: Le chemin d'acces est manquant ou incorrect. Cette couche est obligatoire pour le module cable\n" 

        
    #Check file_shp_Foret   
    if test_Skidder+test_Forwarder+test_Cable>0:    
        try:     
            if not check_field(file_shp_Foret,"FORET"):
                test=0
                if language=='EN':        
                    mess+=" -   Forest shapefile: Field 'FORET' is missing\n"
                else:
                    mess+=" -   Couche foret: Le champs 'FORET' est manquant\n" 
        except:
            test=0
            if language=='EN':        
                mess+=" -   Forest shapefile: The path is missing or not correct. This shapefile is compulsory to run Sylvaccess\n"
            else:
                mess+=" -   Couche foret: Le chemin d'acces est manquant ou incorrect. Cette couche est obligatoire pour lancer Sylvaccess\n"     
                
    #Check Cabsel_For for cable optim
    if not test_Cable and test_cable_optimise and new_calc and Cabsel_For!="":
        try:     
            if not check_field(Cabsel_For,"FORET"):
                test=0
                if language=='EN':        
                    mess+=" -   Forest shapefile (cable optimisation tab): Field 'FORET' is missing\n"
                else:
                    mess+=" -   Couche foret (onglet optimisation cable): Le champs 'FORET' est manquant\n" 
        except:
            test=0
            if language=='EN':        
                mess+=" -   Forest shapefile (cable optimisation tab): The path is missing or not correct.\n"
            else:
                mess+=" -   Couche foret (onglet optimisation cable): Le chemin d'acces est manquant ou incorrect. \n"     
        
    
    EN_name = ["Raster Volume/ha","Raster Average Tree Volume","Raster Tree Height"]
    FR_name = ["Raster Volume/ha","Raster volume arbre moyen","Raster hauteur des arbres"]
    for i,f in enumerate([file_Vol_ha,file_Vol_AM,file_Htree]):
        if f!="":
            try:
                names2,values2,proj2,Extent2 = raster_get_info(f)    
                if values2[5]==None:
                    test=0
                    if language=='EN':        
                        mess+=" -   "+EN_name[i]+": NoData value is not defined\n"
                    else:
                        mess+=" -   "+FR_name[i]+": Aucune valeur de NoData definie\n" 
                if not values[4]==values2[4]:
                    test=0
                    if language=='EN':        
                        mess+=" -   "+EN_name[i]+": Raster cell size should be the same than DEM raster\n"
                    else:
                        mess+=" -   "+FR_name[i]+": La taille de cellules du raster doit etre la meme que celle du MNT\n" 
                if not np.all(Extent==Extent2):
                    test=0
                    if language=='EN':        
                        mess+=" -   "+EN_name[i]+": Raster extent should be the same than DEM raster\n"
                    else:
                        mess+=" -   "+FR_name[i]+": L'etendue du raster doit etre la meme que celle du MNT\n" 
            except:
                test=0
                if language=='EN':        
                    mess+=" -   "+EN_name[i]+": The path is not correct\n"
                else:
                    mess+=" -   "+FR_name[i]+": Le chemin d'access est incorrect\n"     

    if not test:
        mess+="\n"
        if language=='EN':        
            mess+="PLEASE CORRECT BEFORE RUNNING SYLVACCESS\n"
        else:
            mess+="MERCI DE CORRIGER AVANT DE RELANCER SYLVACCESS\n"
    return test,mess
        
    
################################################################################################################################################
### GIS functions
################################################################################################################################################

# Check if field name exist in shapefile
def check_field(filename,fieldname):    
    test=0
    source_ds = ogr.Open(filename)
    layer = source_ds.GetLayer()    
    ldefn = layer.GetLayerDefn()
    for n in range(ldefn.GetFieldCount()):
        fdefn = ldefn.GetFieldDefn(n)
        if fdefn.name==fieldname:
            test=1
            break    
    source_ds.Destroy() 
    return test

def check_field_EXIST(filename,fieldname):    
    test=0
    source_ds = ogr.Open(filename)
    layer = source_ds.GetLayer()    
    ldefn = layer.GetLayerDefn()
    for n in range(ldefn.GetFieldCount()):
        fdefn = ldefn.GetFieldDefn(n)
        if fdefn.name==fieldname:
            test=1
            break
    if test:
        featureCount = layer.GetFeatureCount()
        vals = []
        for feat in layer:
            val = feat.GetField(fieldname)
            if val is not None:
                vals.append(feat.GetField(fieldname))      
        nbval = np.unique(vals).shape[0]
        if nbval==1:
            test=0
    source_ds.Destroy() 
    return test

# Get information from a raster file
def raster_get_info(in_file_name):
    source_ds = gdal.Open(in_file_name)    
    src_proj = osr.SpatialReference(wkt=source_ds.GetProjection())
    src_ncols = source_ds.RasterXSize
    src_nrows = source_ds.RasterYSize
    xmin,Csize_x,a,ymax,b,Csize_y = source_ds.GetGeoTransform()
    ymin = ymax+src_nrows*Csize_y
    nodata = source_ds.GetRasterBand(1).GetNoDataValue()
    names = ['ncols', 'nrows', 'xllcorner', 'yllcorner', 'cellsize','NODATA_value']
    values = [src_ncols,src_nrows,xmin,ymin,Csize_x,nodata]
    Extent = [xmin,xmin+src_ncols*Csize_x,ymin,ymax]
    return names,values,src_proj,Extent

# Generate ASCII raster headtext
def generate_HeadText(names,values):    
    head_text=""
    for i,item in enumerate(names):
        head_text = head_text+item+(14-len(item))*" "+str(values[i])+"\n"
    return head_text
    
# Generate ASCII raster headtext
def save_raster_info(values,Rspace_c):   
    np.savetxt(Rspace_c+"Area_extent.txt", values, fmt='%f', delimiter=';')
    
def loadrasterinfo_from_file(Rspace_c):
    values = list(np.loadtxt(Rspace_c+"Area_extent.txt"))
    names = ['ncols', 'nrows', 'xllcorner', 'yllcorner', 'cellsize','NODATA_value']    
    ncols = values[0]
    nrows = values[1]
    xmin=values[2]
    ymin = values[3]    
    Csize = values[4] 
    Extent = [xmin,xmin+ncols*Csize,ymin,ymin+nrows*Csize]
    return names,values,Extent

# Load float raster
def load_float_raster(raster_file,Dir_temp):
    dataset = gdal.Open(raster_file,gdal.GA_ReadOnly)
    cols = dataset.RasterXSize
    rows = dataset.RasterYSize    
    geotransform = dataset.GetGeoTransform()
    xmin = geotransform[0]
    xmax = xmin + geotransform[1]*cols
    ymax = geotransform[3]
    ymin = geotransform[3] + geotransform[5]*rows
    Extent = [xmin,xmax,ymin,ymax]
    Csize = abs(geotransform[1])
    proj = dataset.GetProjection()
    dataset_val = dataset.GetRasterBand(1)
    nodatavalue = dataset_val.GetNoDataValue()
    names = ['ncols', 'nrows', 'xllcorner', 'yllcorner', 'cellsize','NODATA_value']
    values = [round(cols,2), round(rows,2), round(xmin,2), round(ymin,2), round(Csize,2),round(nodatavalue,2)]
    head_text=""
    for i,item in enumerate(names):
        head_text = head_text+item+(14-len(item))*" "+str(values[i])+"\n"
    f = open(Dir_temp+'info_extent.txt',"w")
    f.write(head_text)
    f.close()
    Array = dataset_val.ReadAsArray()
    Array[Array==nodatavalue]=-9999
    dataset.FlushCache()
    return np.float_(Array),Extent,Csize,proj  

def load_float_raster_simple(raster_file):
    dataset = gdal.Open(raster_file,gdal.GA_ReadOnly)    
    dataset_val = dataset.GetRasterBand(1)
    nodatavalue = dataset_val.GetNoDataValue()    
    Array = dataset_val.ReadAsArray()
    Array[Array==nodatavalue]=-9999
    dataset.FlushCache()
    return np.float_(Array)

# Get spatial projection from shapefile
def get_source_src(file_name):
    source_ds = ogr.Open(file_name)
    source_layer = source_ds.GetLayer()
    return source_layer.GetSpatialRef()

# Convert shapefile to numpy array
def shapefile_to_np_array(file_name,Extent,Csize,attribute_name,order_field=None,order=None):
    """
    Convert shapefile to numpy array
    ----------
    Parameters
    ----------
    file_name:              string      Complete name of the shapefile to convert
    Extent:                 list        Extent of the array : [xmin,xmax,ymin,ymax]
    Csize:                  int, float  Cell resolution of the output array
    attribute_name:         string      Attribute name of the field used for rasterize
    order_field (optional): string      Attribute name of the field used to order the rasterization
    order (optional):       string      Sorting type : 'ASC' for ascending or 'DESC' descending

    Returns
    -------
    mask_array :            ndarray int32
    ----------
    Examples
    --------
    >>> import ogr,gdal
    >>> import numpy as np
    >>> mask_array = shapefile_to_np_array("Route.shp",[0,1000,0,2000],5,"Importance","Importance",'ASC')
    """
    #Recupere les dimensions du raster ascii
    xmin,xmax,ymin,ymax = Extent[0],Extent[1],Extent[2],Extent[3]
    nrows,ncols = int((ymax-ymin)/float(Csize)+0.5),int((xmax-xmin)/float(Csize)+0.5)
    # Get information from source shapefile
    orig_data_source = ogr.Open(file_name)
    source_ds = ogr.GetDriverByName("Memory").CopyDataSource(orig_data_source, "")
    source_layer = source_ds.GetLayer()
    if order:
        source_layer_ordered = source_ds.ExecuteSQL('SELECT * FROM '+str(source_layer.GetName())+' ORDER BY '+order_field+' '+order)
    else:source_layer_ordered=source_layer
    source_srs = source_layer.GetSpatialRef()
    # Initialize the new memory raster
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)    
    target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
    target_ds.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds, [maskvalue], source_layer_ordered,options=["ATTRIBUTE="+attribute_name,"ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds.FlushCache()
        mask_arr = target_ds.GetRasterBand(1).ReadAsArray()
        return mask_arr

# Select element in shapefile
def select_in_shapefile(source_shapefile,out_Shape_Path,expression):
    """
    Select a part of the shapefile and make new shapefile with it
    ----------
    Parameters
    ----------
    source_shapefile:    string      Complete name of the source shapefile
    out_Shape_Path:      string      Complete name of the output shapefile
    expression:          string      SQL expression used for the selection
    -------
    Examples
    --------
    >>> import ogr,gdal
    >>> select_in_shapefile("Desserte.shp","Route.shp",'WHERE Importance=2')
    """
    #Recupere le driver
    driver = ogr.GetDriverByName('ESRI Shapefile')
    #Get information from source shapefile
    source_ds = ogr.Open(source_shapefile)
    source_layer = source_ds.GetLayer()    
    source_srs = source_layer.GetSpatialRef()
    source_type = source_layer.GetGeomType()
    try: source_layer_bis = source_ds.ExecuteSQL('SELECT * FROM '+str(source_layer.GetName())+' '+expression)   
    except: print("Erreur de syntaxe dans l'expression")
    # Initialize the output shapefile
    if os.path.exists(out_Shape_Path):driver.DeleteDataSource(out_Shape_Path)
    target_ds = driver.CreateDataSource(out_Shape_Path)
    layerName = os.path.splitext(os.path.split(out_Shape_Path)[1])[0]
    layer = target_ds.CreateLayer(layerName, source_srs, source_type)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('Route', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind=0
    for feat in source_layer_bis:
        geometry = feat.GetGeometryRef()
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(geometry)
        feature.SetFID(ind)
        feature.SetField('Route',1)
        # Save feature
        layer.CreateFeature(feature)
        # Cleanup
        feature.Destroy()
        ind +=1
    # Cleanup
    target_ds.Destroy()
    source_ds.Destroy()

# Extract all vertex from a line shapefile
def linestring_to_point(Line_shapefile,Point_Shape_Path):
    """
    Extract all vertex from line shapefile and create a point shapefile from this extraction
    Mark at the same time the lines extremities
    ----------
    Parameters
    ----------
    Line_shapefile:    string           Complete name of the source shapefile containing lines
    Point_Shape_Path:  string           Complete name of the output shapefile containing points

    Returns
    -------
    geoLocations :     ndarray float    Matrix containing as colums X Y and line ID
    source_srs:        string           Coordinate system of the source
    ----------
    Examples
    --------
    >>> import ogr,gdal
    >>> linestring_to_point("Desserte.shp","Point_desserte.shp")
    """
    #Get driver
    driver = ogr.GetDriverByName('ESRI Shapefile')
    # Get line info
    source_ds = ogr.Open(Line_shapefile)
    source_layer = source_ds.GetLayer()
    source_srs = source_layer.GetSpatialRef()
    geoLocations = []
    ind = 1
    for feat in source_layer:
          geom = feat.GetGeometryRef()
          points = geom.GetPointCount()          
          for p in range(points):
                lon, lat,z = geom.GetPoint(p)
                geoLocations.append([lon,lat,ind])
          ind +=1
    geoLocations = np.array(geoLocations)
    fins_ligne = fin_ligne(geoLocations)
    # Create output point shapefile
    if os.path.exists(Point_Shape_Path):driver.DeleteDataSource(Point_Shape_Path)
    target_ds = driver.CreateDataSource(Point_Shape_Path)
    layerName = os.path.splitext(os.path.split(Point_Shape_Path)[1])[0]
    layer = target_ds.CreateLayer(layerName, source_srs, ogr.wkbPoint)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('IND_LINE', ogr.OFTInteger)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('FIN_LIGNE', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind = 0
    for pointIndex, geoLocation in enumerate(geoLocations):
        # Create point
        geometry = ogr.Geometry(ogr.wkbPoint)
        geometry.SetPoint(0, geoLocation[0], geoLocation[1])
        # Create feature
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(geometry)
        feature.SetFID(pointIndex)
        feature.SetField('IND_LINE',int(geoLocation[2]))
        if pointIndex==fins_ligne[ind]:
            feature.SetField('FIN_LIGNE',1)
            ind +=1
            if ind > len(fins_ligne)-1:
                ind-=1
        else:
            feature.SetField('FIN_LIGNE',0)
        # Save feature
        layer.CreateFeature(feature)
        # Cleanup
        geometry.Destroy()
        feature.Destroy()
    # Cleanup
    target_ds.Destroy()
    return geoLocations,source_srs

# Create line shapefile from point shapefile
def points_to_lineshape(point_coords,Line_Shape_Path,projection):
    """
    Convert a file of point coordinate to a line shapefile
    ----------
    Parametres
    ----------
    point_coords:     ndarray float    Matrix contenaing positiosn X Y and line ID
    Line_Shape_Path:  string           Complete name of the output shapefile containing lines
    projection:       string           Spatial projection 

    Examples
    --------
    >>> import ogr,gdal
    >>> points_to_lineshape(point_coords,"Line.shp",projection)
    """
    #Recupere le driver
    driver = ogr.GetDriverByName('ESRI Shapefile')
    # Create output line shapefile 
    if os.path.exists(Line_Shape_Path):driver.DeleteDataSource(Line_Shape_Path)
    target_ds = driver.CreateDataSource(Line_Shape_Path)
    layerName = os.path.splitext(os.path.split(Line_Shape_Path)[1])[0]
    layer = target_ds.CreateLayer(layerName, projection, ogr.wkbLineString)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('DIRECTION', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind = 0
    while ind<point_coords.shape[0]-1:        
        if point_coords[ind+1,2]==point_coords[ind,2]:
            line = ogr.Geometry(ogr.wkbLineString)
            line.AddPoint(point_coords[ind,0],point_coords[ind,1])
            line.AddPoint(point_coords[ind+1,0],point_coords[ind+1,1])
            feature = ogr.Feature(layerDefinition)
            feature.SetGeometry(line)
            feature.SetFID(ind)
            direction = calculate_direction(point_coords[ind,0],point_coords[ind,1], point_coords[ind+1,0],point_coords[ind+1,1])
            if ind+2< point_coords.shape[0] and point_coords[ind+1,2]==point_coords[ind+2,2]:
                feature.SetField('DIRECTION',direction)
            else:
                if direction <0:feature.SetField('DIRECTION',direction%180)
                else:feature.SetField('DIRECTION',direction%(-180))
            layer.CreateFeature(feature)
            ind +=1
            line.Destroy()
            feature.Destroy()
        else:
            ind +=1
    target_ds.Destroy()

# Calculate azimuth of a line (angle comparing to GIS north)
def calculate_direction(x1,y1,x2,y2):
    """
    Calculate the azimuth between two points from their coordinates
    """
    DX = x2-x1
    DY = y2-y1
    Deuc = math.sqrt(DX**2+DY**2)
    if x2>x1:Fact=1
    else:Fact=-1
    Angle = math.degrees(math.acos(DY/Deuc))
    Angle *=Fact
    return int(Angle+0.5)

# Mark extremities of a line
def fin_ligne(point_coords):
    """
    Mark extremities of a line
    """
    fin_ligne = []
    for i in range(point_coords.shape[0]-1):
        ind = np.sum((point_coords[:,0]==point_coords[i,0])*(point_coords[:,1]==point_coords[i,1]))
        if ind ==1:
            if i>0 and i+1<point_coords.shape[0]:
                if point_coords[i-1,2]!=point_coords[i,2] or point_coords[i+1,2]!=point_coords[i,2]:
                     fin_ligne.append(i)
            elif i==0 and point_coords[i+1,2]==point_coords[i,2]:fin_ligne.append(i)
            elif i==point_coords.shape[0]-1 and point_coords[i-1,2]==point_coords[i,2]:fin_ligne.append(i)
    return fin_ligne  
    
# Get head text from ASCII raster
def get_head_text(ASCII_file):
    names = np.genfromtxt(ASCII_file, dtype=None,usecols=(0))[0:6]
    values = np.genfromtxt(ASCII_file, dtype=np.float,usecols=(1))[0:6]
    Extent = [values[2],values[2]+values[4]*values[0],values[3],values[3]+values[4]*values[1]]
    return names,values,Extent

# Generate a headtext for creating ASCII raster
def generate_head_text(names,values,Csize):
    rap = int(Csize/values[4])
    values[0],values[1],values[4]= int(values[0]/rap+0.5),int(values[1]/rap+0.5),Csize
    head_text=""
    for i,item in enumerate(names):
        head_text = head_text+item+(14-len(item))*" "+str(values[i])+"\n"
    return head_text   

# Create a buffer around a shapefile
def buffer_shp(infile,outfile,buffdist):
    """
    Create a buffer around a shapefile
    ----------
    Parametres
    ----------
    in_file:    string      Complete name of the input shapefile
    outfile:    string      Complete name of the output shapefile
    buffdist:   int/float   Buffer distance
    """
    try:
        ds_in=ogr.Open( infile )
        lyr_in=ds_in.GetLayer( 0 )
        drv=ds_in.GetDriver()
        if os.path.exists( outfile ):
            drv.DeleteDataSource(outfile)
        ds_out = drv.CreateDataSource( outfile )
        layer = ds_out.CreateLayer(lyr_in.GetLayerDefn().GetName(),lyr_in.GetSpatialRef(), ogr.wkbPolygon)
        for i in range ( lyr_in.GetLayerDefn().GetFieldCount() ):
            field_in = lyr_in.GetLayerDefn().GetFieldDefn( i )
            fielddef = ogr.FieldDefn( field_in.GetName(), field_in.GetType() )
            layer.CreateField ( fielddef )
        for feat in lyr_in:
            geom = feat.GetGeometryRef()
            feature = feat.Clone()
            feature.SetGeometry(geom.Buffer(float(buffdist)))
            layer.CreateFeature(feature)
            del geom
        ds_out.Destroy()
    except:
        return False
    return True

# Create a numpy array from shapefile contained in a directory
def shapefile_obs_to_np_array(file_list,Extent,Csize):
    """
    Create a numpy array from shapefile contained in a directory
    ----------
    Parameters
    ----------
    file_list:              string      List of .shp file to rasterize
    Extent:                 list        Extent of the area : [xmin,xmax,ymin,ymax]
    Csize:                  int, float  Cell resolution of the area  

    Returns
    -------
    mask_array :            ndarray int32
    """
    #Get raster dimension
    xmin,xmax,ymin,ymax = Extent[0],Extent[1],Extent[2],Extent[3]
    nrows,ncols = int((ymax-ymin)/float(Csize)+0.5),int((xmax-xmin)/float(Csize)+0.5)        
    #Create obstacle raster
    Obstacle = np.zeros((nrows,ncols),dtype=np.int)
    #Loop on all shaefile
    for shp in file_list:        
        # Get shapefile info
        source_ds = ogr.Open(shp)
        source_layer = source_ds.GetLayer()    
        source_srs = source_layer.GetSpatialRef()
        source_type = source_layer.GetGeomType()
        # Create copy
        target_ds1 = ogr.GetDriverByName("Memory").CreateDataSource("")
        layerName = os.path.splitext(os.path.split(shp)[1])[0]
        layer = target_ds1.CreateLayer(layerName, source_srs, source_type)
        layerDefinition = layer.GetLayerDefn()
        new_field = ogr.FieldDefn('Transfo', ogr.OFTInteger)
        layer.CreateField(new_field)
        ind=0
        for feat in source_layer:
            geometry = feat.GetGeometryRef()
            feature = ogr.Feature(layerDefinition)
            feature.SetGeometry(geometry)
            feature.SetFID(ind)
            feature.SetField('Transfo',1)
            # Save feature
            layer.CreateFeature(feature)
            # Cleanup
            feature.Destroy()
            ind +=1
        # Initialize raster
        maskvalue = 1    
        xres=float(Csize)
        yres=float(Csize)
        geotransform=(xmin,xres,0,ymax,0, -yres)         
        target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
        target_ds.SetGeoTransform(geotransform)
        if source_srs:
            # Make the target raster have the same projection as the source
            target_ds.SetProjection(source_srs.ExportToWkt())
        else:
            # Source has no projection (needs GDAL >= 1.7.0 to work)
            target_ds.SetProjection('LOCAL_CS["arbitrary"]')
        # Rasterize
        err = gdal.RasterizeLayer(target_ds, [maskvalue], layer,options=["ATTRIBUTE=Transfo","ALL_TOUCHED=TRUE"])
        if err != 0:
            raise Exception("error rasterizing layer: %s" % err)
        else:
            target_ds.FlushCache()
            mask_arr = target_ds.GetRasterBand(1).ReadAsArray()
        Obstacle = Obstacle + mask_arr
        target_ds1.Destroy()
        source_ds.Destroy()
    Obstacle = np.int8(Obstacle>0)
    return Obstacle

# Create a numpy array from shapefile contained in a directory
def shapefile_to_int8array(file_name,Extent,Csize):
    """
    Create a numpy array from shapefile contained in a directory
    ----------
    Parameters
    ----------
    file_list:              string      List of .shp file to rasterize
    Extent:                 list        Extent of the area : [xmin,xmax,ymin,ymax]
    Csize:                  int, float  Cell resolution of the area  

    Returns
    -------
    mask_array :            ndarray int32
    """
    #Get raster dimension
    xmin,xmax,ymin,ymax = Extent[0],Extent[1],Extent[2],Extent[3]
    nrows,ncols = int((ymax-ymin)/float(Csize)+0.5),int((xmax-xmin)/float(Csize)+0.5)        
    #Create obstacle raster
    Array = np.zeros((nrows,ncols),dtype=np.int8)      
    # Get shapefile info
    source_ds = ogr.Open(file_name)
    source_layer = source_ds.GetLayer()    
    source_srs = source_layer.GetSpatialRef()
    source_type = source_layer.GetGeomType()
    # Create copy
    target_ds1 = ogr.GetDriverByName("Memory").CreateDataSource("")
    layerName = "shp"
    layer = target_ds1.CreateLayer(layerName, source_srs, source_type)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('Transfo', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind=0
    for feat in source_layer:
        geometry = feat.GetGeometryRef()
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(geometry)
        feature.SetFID(ind)
        feature.SetField('Transfo',1)
        # Save feature
        layer.CreateFeature(feature)
        # Cleanup
        feature.Destroy()
        ind +=1
    # Initialize raster
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)         
    target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
    target_ds.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds, [maskvalue], layer,options=["ATTRIBUTE=Transfo","ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds.FlushCache()
        Array = target_ds.GetRasterBand(1).ReadAsArray()
    target_ds1.Destroy()
    source_ds.Destroy()
    return Array

# Convert a raster to an ASCII integer raster
def raster_to_ASCII_int(raster_name,ascii_name):
    """
    Convert a raster to an ASCII integer raster
    ----------
    Parameters
    ----------
    raster_name:  string      Complete name of the input raster
    ascii_name:   string      Complete name of the output ASCII raster

    ----------
    Examples
    --------
    >>> import gdal
    >>> raster_to_ASCII_int('MNT_1m.tif','MNT_1m.asc')
    """
    source_ds = gdal.Open(raster_name)
    content = source_ds.GetRasterBand(1).ReadAsArray()
    xmin,Csize_x,a,ymax,b,Csize_y = source_ds.GetGeoTransform() 
    ymin = ymax + Csize_y*source_ds.RasterYSize
    names = ['ncols','nrows','xllcorner','yllcorner','cellsize','NODATA_value']
    values = [source_ds.RasterXSize,source_ds.RasterYSize,xmin,ymin,Csize_x,-9999]
    nodata = source_ds.GetRasterBand(1).GetNoDataValue()
    content[content==nodata]=-10000
    head_text=generate_head_text(names,values,Csize_x)
    save_integer_ascii(ascii_name,head_text,np.int_(content+0.5))

# Convert a raster to an ASCII float raster
def raster_to_ASCII(raster_name,ascii_name):
    """
    Convert a raster to an ASCII float raster
    ----------
    Parameters
    ----------
    raster_name:  string      Complete name of the input raster
    ascii_name:   string      Complete name of the output ASCII raster

    ----------
    Examples
    --------
    >>> import gdal
    >>> raster_to_ASCII('MNT_1m.tif','MNT_1m.asc')
    """
    source_ds = gdal.Open(raster_name)
    content = source_ds.GetRasterBand(1).ReadAsArray()
    xmin,Csize_x,a,ymax,b,Csize_y = source_ds.GetGeoTransform()   
    ymin = ymax + Csize_y*source_ds.RasterYSize
    names = ['ncols','nrows','xllcorner','yllcorner','cellsize','NODATA_value']
    values = [source_ds.RasterXSize,source_ds.RasterYSize,xmin,ymin,Csize_x,-9999]
    nodata = source_ds.GetRasterBand(1).GetNoDataValue()
    content[content==nodata]=-9999    
    head_text=generate_head_text(names,values,Csize_x)
    save_float_ascii(ascii_name,head_text,content)

# Resample raster    
def resample_raster(in_file_name,out_file_name,newCsize,methode=gdal.GRA_Bilinear):
    """
    Resample a raster
    ----------
    Parameters
    ----------
    in_file_name:    string      Complete name of the input raster
    out_file_name:   string      Complete name of the output raster
    newCsize:        int, float  Cell resolution of the output raster
    methode:         string      Method: gdal.GRA_Bilinear,gdal.GRA_Cubic,gdal.GRA_CubicSpline,gdal.GRA_NearestNeighbour

    ----------
    Examples
    --------
    >>> import gdal
    >>> resample_raster('mnt1m.tif','mnt5m.tif',5,methode=gdal.GRA_Bilinear)
    """    
    # Get info from source
    source_ds = gdal.Open(in_file_name)    
    driver = source_ds.GetDriver()
    src_proj = source_ds.GetProjection()
    src_ncols = source_ds.RasterXSize
    src_nrows = source_ds.RasterYSize
    xmin,Csize_x,a,ymax,b,Csize_y = source_ds.GetGeoTransform()
    xmin,ymax = int(xmin+0.5),int(ymax+0.5)    
    Bandnb = source_ds.RasterCount    
    # Create ouptut raster
    xres=float(newCsize)
    yres=float(newCsize)
    geotransform=(xmin,xres,0,ymax,0, -yres)
    xmax,ymin = xmin+int(float(src_ncols)*float(Csize_x)+0.5),ymax+int(float(src_nrows)*float(Csize_y)-0.5)
    nrows,ncols = int((ymax-ymin)/float(newCsize)+0.5),int((xmax-xmin)/float(newCsize)+0.5) 
    geotransform=(xmin,xres,0,ymax,0,-yres)    
    if os.path.exists(out_file_name):driver.Delete(out_file_name)
    target_ds = driver.Create(out_file_name, int(ncols), int(nrows), Bandnb, gdal.GDT_Float32)    
    target_ds.SetGeoTransform(geotransform)
    if src_proj:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(src_proj)
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
        src_proj = 'LOCAL_CS["arbitrary"]'    
    gdal.ReprojectImage(source_ds, target_ds, src_proj, src_proj, methode)
    target_ds.GetRasterBand(1).SetNoDataValue(0)
    target_ds.GetRasterBand(1).GetStatistics(0,1)
    target_ds.FlushCache() # Flush    

# Get projection from road
def get_proj_from_road_network(road_network_file):
    source_ds = ogr.Open(road_network_file)
    source_layer = source_ds.GetLayer()    
    source_srs = source_layer.GetSpatialRef()
    return source_srs.ExportToWkt()


# Create Tiff raster from numpy array   
def ArrayToGtiff(Array,file_name,Extent,nrows,ncols,Csize,road_network_proj,nodata_value,raster_type='INT32'):
    """
    Create Tiff raster from numpy array   
    ----------
    Parameters
    ----------
    Array:             np.array    Array name
    file_name:         string      Complete name of the output raster
    Extent:            list        Extent of the area : [xmin,xmax,ymin,ymax]
    nrows:             int         Number of rows in the array
    ncols:             int         Number of columns in the array
    Csize:             int, float  Cell resolution of the array  
    road_network_proj: string      Spatial projection
    nodata_value:      int, float  Value representing nodata in the array
    raster_type:       string      'INT32' (default),'UINT8','UINT16','FLOAT32','FLOAT16'

    """
    xmin,xmax,ymin,ymax=Extent[0],Extent[1],Extent[2],Extent[3]
    xres=(xmax-xmin)/float(ncols)
    yres=(ymax-ymin)/float(nrows)
    geotransform=(xmin,xres,0,ymax,0, -yres)
    if raster_type=='INT32':
        #-2147483648 to 2147483647
        DataType = gdal.GDT_Int32    
    elif raster_type=='UINT8':
        #0 to 255
        DataType = gdal.GDT_Byte
    elif raster_type=='UINT16':
        #0 to 65535    
        DataType = gdal.GDT_UInt16
    elif raster_type=='INT16':
        #-32768 to 32767 
        DataType = gdal.GDT_Int16
    elif raster_type=='FLOAT32':
        #Single precision float: sign bit, 8 bits exponent, 23 bits mantissa
        DataType = gdal.GDT_Float32
    elif raster_type=='FLOAT16':
        #Half precision float: sign bit, 5 bits exponent, 10 bits mantissa
        DataType = gdal.GDT_Float16
    target_ds = gdal.GetDriverByName('GTiff').Create(file_name+'.tif', int(ncols), int(nrows), 1, DataType)
    target_ds.SetGeoTransform(geotransform)
    target_ds.SetProjection(road_network_proj)
    target_ds.GetRasterBand(1).WriteArray( Array )
    target_ds.GetRasterBand(1).SetNoDataValue(nodata_value)
    target_ds.GetRasterBand(1).GetStatistics(0,1)
    target_ds.FlushCache()

# Calculate local statistics from a raster
def focal_stat(in_file_name,out_file_name,methode='MEAN',nbcell=3):
    """
    Calculate local statistics from a raster
    ----------
    Parameters
    ----------
    in_file_name:    string      Complete name of the input raster
    out_file_name:   string      Complete name of the output raster
    methode:         string      Method : 'MEAN','MAX','MIN','NB','SUM'
    nbcell:          string      nb of cell to take into account in the analysis (default=3)

    ----------
    Examples
    --------
    >>> import gdal
    >>> import numpy as np
    >>> focal_stat('MNT_1m.tif','MNT_focal.tif',methode='MEAN')
    """    
    # Get info of the input raster
    source_ds = gdal.Open(in_file_name)
    nodata = source_ds.GetRasterBand(1).GetNoDataValue()
    driver = source_ds.GetDriver()
    src_proj = source_ds.GetProjection()
    nrows,ncols = source_ds.RasterYSize,source_ds.RasterXSize
    geotransform = source_ds.GetGeoTransform()
    Bandnb = source_ds.RasterCount   
    Data = source_ds.GetRasterBand(1).ReadAsArray()  
    #Make analysis
    if methode=='MEAN':
        outData = fc.focal_stat_mean(np.float_(Data),float(nodata),nbcell)
    elif methode=='MIN':
        outData = fc.focal_stat_min(np.float_(Data),float(nodata),nbcell)
    elif methode=='MAX':
        outData = fc.focal_stat_max(np.float_(Data),float(nodata),nbcell) 
    elif methode=='NB':
        outData = fc.focal_stat_nb(np.float_(Data),float(nodata),nbcell)   
    elif methode=='SUM':
        outData = fc.focal_stat_sum(np.float_(Data),float(nodata),nbcell)   
    #Inititialiaze output raster
    if os.path.exists(out_file_name):driver.Delete(out_file_name)
    target_ds = driver.Create(out_file_name, int(ncols), int(nrows), Bandnb, gdal.GDT_Float32)    
    target_ds.SetGeoTransform(geotransform)
    if src_proj:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(src_proj)
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
        src_proj = 'LOCAL_CS["arbitrary"]' 
    target_ds.GetRasterBand(1).WriteArray(outData)
    target_ds.GetRasterBand(1).SetNoDataValue(nodata)
    target_ds.GetRasterBand(1).GetStatistics(0,1)
    target_ds.FlushCache() # Flush


################################################################################################################################################
### Function for sylvaccess cable
################################################################################################################################################
def prep_rast(Dir_temp,d,E,Tmax,Lmax,Fo,q1,q2,q3,Csize):
    rastLosup,rastTh,rastTv = fc.Tabmesh(d,E,Tmax,Lmax,Fo,q1,q2,q3,Csize)
    np.save(Dir_temp+"rastLosup.npy",rastLosup)
    np.save(Dir_temp+"rastTh.npy",rastTh)
    np.save(Dir_temp+"rastTv.npy",rastTv)
    text  = "d    "+" "+str(round(d,2))+"\n"
    text += "E    "+" "+str(round(E,2))+"\n"
    text += "Tmax "+" "+str(round(Tmax,2))+"\n"
    text += "Lmax "+" "+str(round(Lmax,2))+"\n"
    text += "Fo   "+" "+str(round(Fo,2))+"\n"
    text += "Csize"+" "+str(round(Csize,2))+"\n"
    text += "q1   "+" "+str(round(q1,2))+"\n"
    text += "q2   "+" "+str(round(q2,2))+"\n"
    text += "q3   "+" "+str(round(q3,2))+"\n"
    f = open(Dir_temp+'info_config.txt',"w")
    f.write(text)
    f.close()
    return rastLosup,rastTh,rastTv

def check_tabconv(Dir_temp,d,E,Tmax,Lmax,Fo,q1,q2,q3,Csize):
    try:
        n1,v1=read_info(Dir_temp+"info_config.txt")
        if np.all(np.array([round(d,2),round(E,2),round(Tmax,2),round(Lmax,2),round(Fo,2),round(Csize,2),round(q1,2),round(q2,2),round(q3,2)])==v1):
            rastLosup = np.load(Dir_temp+"rastLosup.npy")
            rastTh = np.load(Dir_temp+"rastTh.npy")
            rastTv = np.load(Dir_temp+"rastTv.npy")
        else:
            rastLosup,rastTh,rastTv = prep_rast(Dir_temp,d,E,Tmax,Lmax,Fo,q1,q2,q3,Csize)
    except:
        rastLosup,rastTh,rastTv = prep_rast(Dir_temp,d,E,Tmax,Lmax,Fo,q1,q2,q3,Csize)        
    return rastLosup,rastTh,rastTv


def check_line(Line,Lmax,Lmin,nrows,ncols,Lsans_foret):
    indmax = 0
    npix = Line.shape[0]
    test = 1
    i=0
    Lline=Lmin-1
    Dsansforet=0.
    for i in range(0,npix): 
        if Line[i,5]<0:break
        if Line[i,5]>=ncols:break
        if Line[i,6]<0:break
        if Line[i,6]>=nrows:break
        if Line[i,7]==1:break
        if sqrt(Line[i,0]*Line[i,0]+(Line[i,1]-Line[0,1])*(Line[i,1]-Line[0,1]))>Lmax:break        
        if (Line[i,8]+Line[i,9])>0:                 
            if Line[i,2]==1:
                indmax = i 
                Dsansforet=0
            else:
                if i>0: Dsansforet+=Line[i,0]-Line[i-1,0]
                if Dsansforet>=Lsans_foret:break
        else:        
            break
    Lline = Line[indmax,0]
    if Lline <= Lmin:
        test=0
    return test,indmax+1,Lline

def get_ligne3(coordX,coordY,posiX,posiY,az,MNT,Forest,Fin_ligne_forcee,Aspect,Pente,Hfor,test_hfor,Lmax,Lmin,Csize,
              Row_line,Col_line,D_line,Nbpix_line,angle_transv,slope_trans,ncols,nrows,Lsans_foret,
              Fo,Tmax,q1,q2,q3,Htower,Hend,Hline_max,Hintsup,Lslope,PropSlope):
                  
    npix = Nbpix_line[az]
    npix = fc.get_npix(az,npix,coordY,coordX,ncols,nrows,Row_line,Col_line)   
    if D_line[az,npix-1]>Lmin:  
        Line=np.zeros((npix,11),dtype=np.float)
        inds = (Row_line[az,0:npix]+coordY,Col_line[az,0:npix]+coordX)        
        Line[:,0],Line[:,1],Line[:,2]=D_line[az,0:npix],MNT[inds],Forest[inds]
        Line[:,3],Line[:,4]=Csize*Col_line[az,0:npix]+posiX,-Csize*Row_line[az,0:npix]+posiY
        Line[:,5],Line[:,6]=Col_line[az,0:npix]+coordX,Row_line[az,0:npix]+coordY 
        Line[:,7],Line[:,8],Line[:,9]=Fin_ligne_forcee[inds],np.abs(((az-np.int_(Aspect[inds]))+180)%360-180),(np.int_(Pente[inds])<slope_trans)*1
        ### Check pente en devers
        Line[:,8] = (Line[:,8]>(90+angle_transv))*1+(Line[:,8]<(90-angle_transv))*1 
        if test_hfor:
            Line[:,10]=np.round(np.minimum(np.maximum(-7.76961+0.71858*Hfor[inds],0),26))
        else:
            Line[:,10]=Hintsup
        ####Raccourci pour ne pas depasser Hline_max
        indmax=Line.shape[0]-1
        for i in range(indmax,1,-1):  
            test=1
            D = Line[i,0]
            H = abs(Line[0,1]+Htower-(Line[i,1]+Hend))    
            if Line[0,1]+Htower>=Line[i,1]+Hend:
                Xup,Zup =0,Line[0,1]+Htower
                fact = 1. 
            else:    
                Xup,Zup = Line[i,0],Line[i,1]+Hend
                fact = -1.             
            L=sqrt(H*H+D*D)
            F = 0.5*(0.5*L*q2+0.5*L*q3)*9.80665 + Fo  
            fleche = 1.1*(F*L/(4*Tmax)+q1*9.80665*L*L/(8*Tmax))
            for j in range(1,i-1):
                droite = -fact*H/D*(Line[j,0]-Xup)+Zup-Line[j,1]
                if droite-fleche > Hline_max:
                    test=0
                    break
            if test:
                break
        Line=Line[0:i+1]
        test,indmax,Lline=fc.check_line(Line,Lmax,Lmin,nrows,ncols,Lsans_foret,Lslope,PropSlope)    
        return test,Lline,Line[0:indmax, [0, 1, 2, 3, 4, 5, 6,10,9]]
    else:
        return 0,0,0


#def get_ligne3(coordX,coordY,posiX,posiY,az,MNT,Forest,Fin_ligne_forcee,Aspect,Pente,Hfor,test_hfor,Lmax,Lmin,Csize,
#              Row_line,Col_line,D_line,Nbpix_line,angle_transv,slope_trans,ncols,nrows,Lsans_foret,
#              Fo,Tmax,q1,q2,q3,Htower,Hend,Hline_max,Hintsup,Lslope,PropSlope):
#                  
#    npix = Nbpix_line[az]
#    npix = fc.get_npix(az,npix,coordY,coordX,ncols,nrows,Row_line,Col_line)   
#    if D_line[az,npix-1]>Lmin:  
#        Line=np.zeros((npix,11),dtype=np.float)
#        inds = (Row_line[az,0:npix]+coordY,Col_line[az,0:npix]+coordX)        
#        Line[:,0],Line[:,1],Line[:,2]=D_line[az,0:npix],MNT[inds],Forest[inds]
#        Line[:,3],Line[:,4]=Csize*Col_line[az,0:npix]+posiX,-Csize*Row_line[az,0:npix]+posiY
#        Line[:,5],Line[:,6]=Col_line[az,0:npix]+coordX,Row_line[az,0:npix]+coordY 
#        Line[:,7],Line[:,8],Line[:,9]=Fin_ligne_forcee[inds],np.abs(((az-np.int_(Aspect[inds]))+180)%360-180),(np.int_(Pente[inds])<slope_trans)*1
#        ### Check pente en devers
#        Line[:,8] = (Line[:,8]>(90+angle_transv))*1+(Line[:,8]<(90-angle_transv))*1 
#        ### Check ou la pente en devers se trouve sur pente en long > slope_trans
#        tp = (Line[:,8]+Line[:,9])==0
#        Line[:,8]*=0       
#        Line[tp,8]=1         
#        ### Test la distance et proportion de pente en devers 
#        Line[:,9]*=0
#        Dcum=0
#        for i in range(1,Line.shape[0]):
#            if Line[i,8]==1:    
#                D = Line[i,0]-Line[i-1,0]
#                if (Dcum+D)>Lslope:
#                    Line = Line[:i]
#                    break
#                Dcum+=D
#            Line[i,9]=Dcum
#        try:
#            idmax =np.max(np.argwhere(Line[:,9]<PropSlope*Line[:,0]))   
#        except:
#            idmax=0       
#        if Line[idmax,0]<Lmin:
#            return 0,0,0
#        else:
#            Line=Line[:idmax]
#            if test_hfor:
#                Line[:,10]=np.round(np.minimum(np.maximum(-7.76961+0.71858*Hfor[inds],0),26))
#            else:
#                Line[:,10]=Hintsup
#            ####Raccourci pour ne pas depasser Hline_max
#            indmax=Line.shape[0]-1
#            for i in range(indmax,1,-1):  
#                test=1
#                D = Line[i,0]
#                H = abs(Line[0,1]+Htower-(Line[i,1]+Hend))    
#                if Line[0,1]+Htower>=Line[i,1]+Hend:
#                    Xup,Zup =0,Line[0,1]+Htower
#                    fact = 1. 
#                else:    
#                    Xup,Zup = Line[i,0],Line[i,1]+Hend
#                    fact = -1.             
#                L=sqrt(H*H+D*D)
#                F = 0.5*(0.5*L*q2+0.5*L*q3)*9.80665 + Fo  
#                fleche = 1.1*(F*L/(4*Tmax)+q1*9.80665*L*L/(8*Tmax))
#                for j in range(1,i-1):
#                    droite = -fact*H/D*(Line[j,0]-Xup)+Zup-Line[j,1]
#                    if droite-fleche > Hline_max:
#                        test=0
#                        break
#                if test:
#                    break
#            Line=Line[0:i+1]
#            test,indmax,Lline=fc.check_line(Line,Lmax,Lmin,nrows,ncols,Lsans_foret)    
#            return test,Lline,Line[0:indmax, [0, 1, 2, 3, 4, 5, 6, 10]]
#    else:
#        return 0,0,0

def return_profile(Line):
    Line2 = np.zeros_like(Line)
    indmax = Line.shape[0]-1
    Dmax =Line[indmax,0]
    for i,j in enumerate(range(indmax,-1,-1)):
        Line2[i]=Line[j]
        Line2[i,0]=Dmax-Line[j,0]
    return Line2
           
            
#file with param
def write_file(Rspace_c,Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Desserte,Dir_Obs_cable,file_Vol_ha,file_Vol_AM,Pente_max_bucheron,Lmax,Lmin,
               LminSpan,Htower,Hintsup,Hend,Lhor_max,Hline_min,Hline_max,sup_max,Carriage_type,Cable_type,slope_grav,Pchar,slope_Wliner_up,slope_Wliner_down,
               q1,rupt_res,safe_fact,E,d,Load_max,q2,q3,Max_angle,coeff_frot,language,Skid_direction,precision,prelevement,slope_trans,angle_transv):
    var_list= [Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Desserte,Dir_Obs_cable,file_Vol_ha,file_Vol_AM,Pente_max_bucheron,Lmax,Lmin,
               LminSpan,Htower,Hintsup,Hend,Lhor_max,Hline_min,Hline_max,sup_max,Carriage_type,Cable_type,slope_grav,Pchar,slope_Wliner_up,slope_Wliner_down,
               q1,rupt_res,safe_fact,E,d,Load_max,q2,q3,Max_angle,coeff_frot,language,Skid_direction,precision,prelevement,slope_trans,angle_transv]
    file_name = Rspace_c+"all_param.txt"
    text=var_list[0]
    for var in var_list[1:]:
        text+="\n"
        text+= str(var)
    fichier = open(file_name, "w")
    fichier.write(text)
    fichier.close()

  
def get_cable_param_from_file(file_name):
    Temp = open(file_name,"r")
    params = Temp.readlines()
    Temp.close()   
    #modify type
    int_list = [9,10,11,16,19,20,21,23,27,31,34,37,38,40,41]
    for i in int_list:
        params[i-1]=int(params[i-1])
    float_list = [12,13,14,15,17,18,22,24,25,26,28,29,30,32,33,35,39]
    for i in float_list:
        params[i-1]=float(params[i-1])
    global Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Desserte,Dir_Obs_cable,file_Vol_ha,file_Vol_AM,Pente_max_bucheron,Lmax,Lmin
    global LminSpan,Htower,Hintsup,Hend,Lhor_max,Hline_min,Hline_max,sup_max,Carriage_type,Cable_type,slope_grav,Pchar,slope_Wliner_up,slope_Wliner_down
    global q1,rupt_res,safe_fact,E,d,Load_max,q2,q3,Max_angle,coeff_frot,language,Skid_direction,precision,prelevement,slope_trans,angle_transv
    Wspace,Rspace,file_MNT,file_shp_Foret,file_shp_Desserte,Dir_Obs_cable,file_Vol_ha,file_Vol_AM=params[0:8]    
    Pente_max_bucheron,Lmax,Lmin,LminSpan,Htower,Hintsup,Hend,Lhor_max,Hline_min,Hline_max,sup_max=params[8:19]
    Carriage_type,Cable_type,slope_grav,Pchar,slope_Wliner_up,slope_Wliner_down,q1,rupt_res,safe_fact=params[19:28]
    E,d,Load_max,q2,q3,Max_angle,coeff_frot,language,Skid_direction,precision,prelevement,slope_trans,angle_transv=params[28:]

#azimuth between two points
def azimuth(X0,Y0,X1,Y1):
    dX=abs(X0-X1)
    dY=abs(Y0-Y1)
    #cas 1:cadran en haut a droite
    if (X1>X0) and (Y1>Y0):
        az=degrees(atan(dX*1.0/dY))
    #cas 2:cadran en bas a droite
    elif (X1>X0) and (Y1<Y0):
        az=180-degrees(atan(dX*1.0/dY))
    #cas 3:cadran en haut a gauche
    elif (X1<X0) and (Y1>Y0):
        az=360-degrees(atan(dX*1.0/dY)) 
    #cas 4:cadran en bas a gauche
    elif (X1<X0) and (Y1<Y0):
        az=180+degrees(atan(dX*1.0/dY)) 
    #cas 5:horizontal gauche
    elif (dY==0) and (X1>X0):
        az=90
    #cas 6:horizontal droite
    elif (dY==0) and (X1<X0):
        az=90*3
    #cas 7:vertical haut
    elif (Y1>=Y0) and (dX==0):
        az=0
    #cas 8:vertical bas
    elif (Y1<Y0) and (dX==0):
        az=180
    return az


def from_az_to_arr(xmin,xmax,ymin,ymax,Csize,Lmax,az):    
    X1 = sin(radians(az))*Lmax
    Y1 = cos(radians(az))*Lmax
    #Initialize raster info
    nrows,ncols = int((ymax-ymin)/float(Csize)+0.5),int((xmax-xmin)/float(Csize)+0.5)    
    #create polygon object:
    driver = ogr.GetDriverByName('Memory')
    datasource = driver.CreateDataSource('')
    source_srs=osr.SpatialReference()
    source_srs.ImportFromEPSG(2154)
    layer = datasource.CreateLayer('layerName',source_srs,geom_type=ogr.wkbLineString)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('ID', ogr.OFTInteger)
    layer.CreateField(new_field)
    line = ogr.Geometry(ogr.wkbLineString)
    line.AddPoint(0,0)
    line.AddPoint(X1,Y1)
    feature = ogr.Feature(layerDefinition)
    feature.SetGeometry(line)
    feature.SetFID(az)
    feature.SetField('ID',1)
    layer.CreateFeature(feature)    
    feature.Destroy()     
    # Initialize the new memory raster      
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)    
    target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int16)
    target_ds.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds, [maskvalue], layer,options=["ATTRIBUTE=ID","ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds.FlushCache()
        mask_arr = target_ds.GetRasterBand(1).ReadAsArray()
    datasource.Destroy()
    return X1,Y1,mask_arr
   
def check_common_line(item,Row_line,Col_line,D_line,nb_pix):
    i=0
    while (Row_line[item-1,i]==Row_line[item,i]) and (Col_line[item-1,i]==Col_line[item,i]) and i<nb_pix:
        i+=1
    return D_line[item,i-1]

def pt_emprise(X0,Y0,X1,Y1,Lhor_max):
    az=azimuth(X0,Y0,X1,Y1)
    #deb + 90
    X = [X0+sin(radians(az+90))*Lhor_max]
    Y = [Y0+cos(radians(az+90))*Lhor_max]
    #deb - 90
    X.append(X0+sin(radians(az-90))*Lhor_max)
    Y.append(Y0+cos(radians(az-90))*Lhor_max)
    #fin - 90
    X.append(X1+sin(radians(az-90))*Lhor_max)
    Y.append(Y1+cos(radians(az-90))*Lhor_max)
    #fin + 90
    X.append(X1+sin(radians(az+90))*Lhor_max)
    Y.append(Y1+cos(radians(az+90))*Lhor_max)
    return [X,Y] 

def point_line_to_line_ext(X0,Y0,X1,Y1,Lhor_max,xmin,xmax,ymin,ymax,Csize):
    ### Create mask_array
    X,Y = pt_emprise(X0,Y0,X1,Y1,Lhor_max)
    #Initialize raster info
    nrows,ncols = int((ymax-ymin)/float(Csize)+0.5),int((xmax-xmin)/float(Csize)+0.5)    
    #create polygon object:
    driver = ogr.GetDriverByName('Memory')
    datasource = driver.CreateDataSource('')
    source_srs=osr.SpatialReference()
    source_srs.ImportFromEPSG(2154)
    layer = datasource.CreateLayer('layerName',source_srs,geom_type=ogr.wkbPolygon)
    new_field = ogr.FieldDefn('ID', ogr.OFTInteger)
    layer.CreateField(new_field)
    # create polygon object:
    myRing = ogr.Geometry(ogr.wkbLinearRing)
    for i in range(0,len(X)):
        myRing.AddPoint(X[i],Y[i])
    myRing.AddPoint(X[0],Y[0])#close ring
    poly = ogr.Geometry(type=ogr.wkbPolygon)
    poly.AddGeometry(myRing)
    feature = ogr.Feature( layer.GetLayerDefn() )
    feature.SetGeometry(poly)
    feature.SetFID(1)
    feature.SetField('ID',1)
    layer.CreateFeature(feature)    
    feature.Destroy()     
    # Initialize the new memory raster      
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)    
    target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int16)
    target_ds.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds, [maskvalue], layer,options=["ATTRIBUTE=ID","ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds.FlushCache()
        mask_arr = target_ds.GetRasterBand(1).ReadAsArray()
    datasource.Destroy()
    return mask_arr

def get_car_dist(Row_line,Col_line,D_line,Nbpix_line,item,mat):
    A = np.zeros((Nbpix_line[item],2))
    A[:,0] = Row_line[item,0:Nbpix_line[item]]
    A[:,1] = Col_line[item,0:Nbpix_line[item]]
    Tree=spatial.cKDTree(A)
    for i,pixel in enumerate(mat):
        pt = [pixel[0],pixel[1]]
        distance,index = Tree.query(pt)
        mat[i,2]=D_line[item,index]
    return mat

def get_car_dist2(Row_line,Col_line,D_line,Nbpix_line,item,mat):
    A = np.zeros((Nbpix_line[item],2))
    A[:,0] = Row_line[item,0:Nbpix_line[item]]
    A[:,1] = Col_line[item,0:Nbpix_line[item]]
    Tree=spatial.cKDTree(A)
    for i,pixel in enumerate(mat):
        pt = [pixel[0],pixel[1]]
        distance,index = Tree.query(pt)
        mat[i,2]=D_line[item,index]
        mat[i,3]=distance
    return mat


def ligh_line(mat,Dmin):
    mat[0,3]=1
    for i in range(1,mat.shape[0]):
        if (mat[i,2]-mat[i-1,2])>Dmin:
            mat[i,3]=1
    return mat[mat[:,3]>0]

#create buffers
def create_buffer(Csize,Lmax,Lhor_max):
    Lcote = Lmax+Lhor_max+1.5*Csize
    xmin,xmax,ymin,ymax = -Lcote,Lcote,-Lcote,Lcote
    Buffer_cote = int((Lmax+Lhor_max)/Csize+1.5)
    Dir_list = range(0,360,1)
    Row_line = np.zeros((len(Dir_list),3*Buffer_cote),dtype=np.int16)
    Col_line = np.zeros((len(Dir_list),3*Buffer_cote),dtype=np.int16)
    D_line = np.ones((len(Dir_list),3*Buffer_cote),dtype=np.float)*-1
    Nbpix_line = np.zeros((len(Dir_list),),dtype=np.int16)
    Row_ext = np.zeros((len(Dir_list),50*Buffer_cote),dtype=np.int16)
    Col_ext = np.zeros((len(Dir_list),50*Buffer_cote),dtype=np.int16)
    D_ext = np.ones((len(Dir_list),50*Buffer_cote),dtype=np.float)*(Lmax+Lhor_max)
    Nbpix_ext = 0
    Dmin = sqrt(2)*Csize*0.5
    for az in Dir_list:
        #Fill line info
        X1,Y1,mask_arr=from_az_to_arr(xmin,xmax,ymin,ymax,Csize,Lmax,az)        
        inds=np.argwhere(mask_arr==1)-Buffer_cote
        mat = np.zeros((inds.shape[0],4))
        mat[:,:2] = inds
        mat[:,2] = Csize*np.sqrt(mat[:,0]**2+mat[:,1]**2)
        ind = np.lexsort((mat[:,1],mat[:,2]))
        mat = mat[ind]
        mat = ligh_line(mat,Dmin)
        nb_pix=mat.shape[0]
        Row_line[az,0:nb_pix]=mat[:,0]
        Col_line[az,0:nb_pix]=mat[:,1]
        D_line[az,0:nb_pix]=mat[:,2]
        Nbpix_line[az] = nb_pix
        #Fill extent info
        mask_arr=point_line_to_line_ext(0,0,X1,Y1,Lhor_max,xmin,xmax,ymin,ymax,Csize)        
        inds=np.argwhere(mask_arr==1)-Buffer_cote
        mat = np.zeros((inds.shape[0],inds.shape[1]+1))
        mat[:,:-1] = inds
        mat = get_car_dist(Row_line,Col_line,D_line,Nbpix_line,az,mat)
        ind = np.lexsort((mat[:,1],mat[:,2]))
        nb_pix=inds.shape[0]
        Row_ext[az,0:nb_pix]=mat[ind,0]
        Col_ext[az,0:nb_pix]=mat[ind,1]
        D_ext[az,0:nb_pix]=mat[ind,2]
        Nbpix_ext = max(nb_pix,Nbpix_ext)
    NbpixmaxL = np.max(Nbpix_line)
    return Row_line[:,0:NbpixmaxL],Col_line[:,0:NbpixmaxL],D_line[:,0:NbpixmaxL],Nbpix_line, Row_ext[:,0:Nbpix_ext],Col_ext[:,0:Nbpix_ext],D_ext[:,0:Nbpix_ext],Dir_list

def create_buffer2(Csize,Lmax,Lhor_max):
    Lcote = Lmax+Lhor_max+1.5*Csize
    xmin,xmax,ymin,ymax = -Lcote,Lcote,-Lcote,Lcote
    Buffer_cote = int((Lmax+Lhor_max)/Csize+1.5)
    Dir_list = range(0,360,1)
    Row_line = np.zeros((len(Dir_list),3*Buffer_cote),dtype=np.int16)
    Col_line = np.zeros((len(Dir_list),3*Buffer_cote),dtype=np.int16)
    D_line = np.ones((len(Dir_list),3*Buffer_cote),dtype=np.float)*-1
    Nbpix_line = np.zeros((len(Dir_list),),dtype=np.int16)
    Row_ext = np.zeros((len(Dir_list),50*Buffer_cote),dtype=np.int16)
    Col_ext = np.zeros((len(Dir_list),50*Buffer_cote),dtype=np.int16)
    D_ext = np.ones((len(Dir_list),50*Buffer_cote),dtype=np.float)*-1
    D_lat = np.ones((len(Dir_list),50*Buffer_cote),dtype=np.float)*(Lmax+Lhor_max)
    Nbpix_ext = 0
    Dmin = sqrt(2)*Csize*0.5
    for az in Dir_list:
        #Fill line info
        X1,Y1,mask_arr=from_az_to_arr(xmin,xmax,ymin,ymax,Csize,Lmax,az)        
        inds=np.argwhere(mask_arr==1)-Buffer_cote
        mat = np.zeros((inds.shape[0],4))
        mat[:,:2] = inds
        mat[:,2] = Csize*np.sqrt(mat[:,0]**2+mat[:,1]**2)
        ind = np.lexsort((mat[:,1],mat[:,2]))
        mat = mat[ind]
        mat = ligh_line(mat,Dmin)
        nb_pix=mat.shape[0]
        Row_line[az,0:nb_pix]=mat[:,0]
        Col_line[az,0:nb_pix]=mat[:,1]
        D_line[az,0:nb_pix]=mat[:,2]
        Nbpix_line[az] = nb_pix
        #Fill extent info
        mask_arr=point_line_to_line_ext(0,0,X1,Y1,Lhor_max,xmin,xmax,ymin,ymax,Csize)        
        inds=np.argwhere(mask_arr==1)-Buffer_cote
        mat = np.zeros((inds.shape[0],inds.shape[1]+2))
        mat[:,:-2] = inds
        mat= get_car_dist2(Row_line,Col_line,D_line,Nbpix_line,az,mat)
        ind = np.lexsort((mat[:,1],mat[:,2]))               
        nb_pix=inds.shape[0]
        Row_ext[az,0:nb_pix]=mat[ind,0]
        Col_ext[az,0:nb_pix]=mat[ind,1]
        D_ext[az,0:nb_pix]=mat[ind,2]
        D_lat[az,0:nb_pix]=mat[ind,3]*Csize
        Nbpix_ext = max(nb_pix,Nbpix_ext)
    NbpixmaxL = np.max(Nbpix_line)
    return Row_line[:,0:NbpixmaxL],Col_line[:,0:NbpixmaxL],D_line[:,0:NbpixmaxL],Nbpix_line, Row_ext[:,0:Nbpix_ext],Col_ext[:,0:Nbpix_ext],D_ext[:,0:Nbpix_ext],D_lat[:,0:Nbpix_ext],Dir_list
       
def get_ligne(coordX,coordY,posiX,posiY,az,MNT,Forest,Fin_ligne_forcee,Aspect,Pente,Lmax,Lmin,Csize,
              Row_line,Col_line,D_line,Nbpix_line,angle_transv,slope_trans,ncols,nrows,Lsans_foret):
    npix = Nbpix_line[az]
    npix = fc.get_npix(az,npix,coordY,coordX,ncols,nrows,Row_line,Col_line)   
    if D_line[az,npix-1]>Lmin:  
        Line=np.zeros((npix,10),dtype=np.float)
        inds = (Row_line[az,0:npix]+coordY,Col_line[az,0:npix]+coordX)        
        Line[:,0],Line[:,1],Line[:,2]=D_line[az,0:npix],MNT[inds],Forest[inds]
        Line[:,3],Line[:,4]=Csize*Col_line[az,0:npix]+posiX,-Csize*Row_line[az,0:npix]+posiY
        Line[:,5],Line[:,6]=Col_line[az,0:npix]+coordX,Row_line[az,0:npix]+coordY 
        Line[:,7],Line[:,8],Line[:,9]=Fin_ligne_forcee[inds],np.abs(((az-np.int_(Aspect[inds]))+180)%360-180),(np.int_(Pente[inds])<slope_trans)*1
        Line[:,8] = (Line[:,8]>(90+angle_transv))*1+(Line[:,8]<(90-angle_transv))*1
        test,indmax,Lline=fc.check_line(Line,Lmax,Lmin,nrows,ncols,Lsans_foret)    
        return test,Lline,Line[0:indmax,0:7]
    else:
        return 0,0,0

def create_az_rules(angle_transv):
    matrice = np.zeros((360,360),dtype=np.int8)
    b1 = 90-angle_transv
    b2 = 90+angle_transv
    b3 = 270-angle_transv
    b4 = 270+angle_transv
    matrice[0,0:b1]=1
    matrice[0,b2:b3]=1
    matrice[0,b4:360]=1
    for expo in range(1,360,1):
        matrice[expo,0]=matrice[expo-1,359]
        for azi in range(1,360,1):
            matrice[expo,azi]=matrice[expo-1,azi-1]
    return matrice    

def Line_to_shapefile(Tab,Cable_line_Path,source_src,prelevement,language):
    """
    Convertit les segments en shapefile pour les lignes qui n'ont pas trouve de correspondance
    ----------
    Parametres
    ----------
    newTab:                 ndarray float    Matrice contenant les caracteristiques des segments
    Line_Planarized_Path:   string           Nom complet du fichier shapefile en sortie
    source_src:             string           Definition de la projection
    """
    # Get driver
    driver = ogr.GetDriverByName('ESRI Shapefile')
    fieldvol = 'Volume_'+str(int(100*prelevement))
    # Create output shapefile
    if os.path.exists(Cable_line_Path):driver.DeleteDataSource(Cable_line_Path)
    target_ds = driver.CreateDataSource(Cable_line_Path)
    layerName = os.path.splitext(os.path.split(Cable_line_Path)[1])[0]
    layer = target_ds.CreateLayer(layerName, source_src, ogr.wkbLineString)
    layerDefinition = layer.GetLayerDefn()  
    new_field = ogr.FieldDefn('IdLine', ogr.OFTInteger)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Xstart', ogr.OFTReal)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Ystart', ogr.OFTReal)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Xend', ogr.OFTReal)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Yend', ogr.OFTReal)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Id_route', ogr.OFTInteger)
    layer.CreateField(new_field)
    if language=='FR':
        new_field = ogr.FieldDefn('Desserte', ogr.OFTString) 
        
    else:
        new_field = ogr.FieldDefn('Road', ogr.OFTString)                              
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('AzimuthDeg', ogr.OFTInteger)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Long', ogr.OFTInteger)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Config', ogr.OFTString)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('NbIntSup', ogr.OFTInteger)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Surface', ogr.OFTReal)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn(fieldvol, ogr.OFTInteger)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('IPC', ogr.OFTReal)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Dmoy', ogr.OFTInteger)
    layer.CreateField(new_field)
    if language=='FR':
        cneg = "Debardage vers l'aval"
        cpos = "Debardage vers l'amont"
        proj = "En projet"
        exis = "Existant"
    else:
        cneg = "Downhill yarding"
        cpos = "Uphill yarding"   
        proj = "Project"
        exis = "Existing"
    
    for ind,S in enumerate(Tab):
        line = ogr.Geometry(ogr.wkbLineString)
        line.AddPoint(float(S[2]),float(S[3]))
        line.AddPoint(float(S[6]),float(S[7]))
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(line)
        feature.SetFID(ind)
        feature.SetField('IdLine',ind+1)
        feature.SetField('Xstart',float(S[2]))
        feature.SetField('Ystart',float(S[3]))
        feature.SetField('Xend',float(S[6]))
        feature.SetField('Yend',float(S[7]))
        if int(S[10])==2: 
            if language=='FR':
                feature.SetField('Desserte',exis)
            else:
                feature.SetField('Road',exis)
        else:
            if language=='FR':
                feature.SetField('Desserte',proj)
            else:
                feature.SetField('Road',proj)
        feature.SetField('Id_route',int(S[0]))
        feature.SetField('AzimuthDeg',int(S[1]))
        feature.SetField('Long',int(S[11]))
        if int(S[12])>0:
            feature.SetField('Config',cpos)
        else:
            feature.SetField('Config',cneg)
        feature.SetField('NbIntSup',int(S[17]))
        feature.SetField('Surface',S[13]/10000.)
        feature.SetField('Dmoy',int(S[14]))
        feature.SetField(fieldvol,int(S[15]*prelevement))
        feature.SetField('IPC',S[15]*prelevement/float(S[11]))
        layer.CreateFeature(feature)
        line.Destroy()
        feature.Destroy()
    target_ds.Destroy()
    
def Pylone_in_shapefile(Tab,Cable_line_Path,source_src,prelevement):
    """
    Convertit les segments en shapefile pour les lignes qui n'ont pas trouve de correspondance
    ----------
    Parametres
    ----------
    newTab:                 ndarray float    Matrice contenant les caracteristiques des segments
    Line_Planarized_Path:   string           Nom complet du fichier shapefile en sortie
    source_src:             string           Definition de la projection
    """
    # Get driver
    driver = ogr.GetDriverByName('ESRI Shapefile')
    # Create output shapefile
    if os.path.exists(Cable_line_Path):driver.DeleteDataSource(Cable_line_Path)
    target_ds = driver.CreateDataSource(Cable_line_Path)
    layerName = os.path.splitext(os.path.split(Cable_line_Path)[1])[0]    
    layer = target_ds.CreateLayer(layerName, source_src, ogr.wkbPoint)
    layerDefinition = layer.GetLayerDefn()  
    new_field = ogr.FieldDefn('IdLine', ogr.OFTInteger)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Xpyl', ogr.OFTReal)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Ypyl', ogr.OFTReal)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Altitude', ogr.OFTReal)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Hcable', ogr.OFTReal)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Pression', ogr.OFTReal)
    layer.CreateField(new_field)
    new_field = ogr.FieldDefn('Pyl_pos', ogr.OFTInteger)
    layer.CreateField(new_field)
    idi = 0
    for ind,S in enumerate(Tab):
        nb=1
        for pyl in range(int(S[17])):  
            point = ogr.Geometry(ogr.wkbPoint)
            point.SetPoint(0, float(S[18+5*pyl]),float(S[19+5*pyl]))         
            feature = ogr.Feature(layerDefinition)
            feature.SetGeometry(point)
            feature.SetFID(idi)
            feature.SetField('IdLine',ind+1)
            feature.SetField('Xpyl',float(S[18+5*pyl]))
            feature.SetField('Ypyl',float(S[19+5*pyl]))
            feature.SetField('Altitude',float(S[20+5*pyl]))
            feature.SetField('Hcable',float(S[21+5*pyl]))
            feature.SetField('Pression',float(S[22+5*pyl]))
            feature.SetField('Pyl_pos',int(nb)             )    
            layer.CreateFeature(feature)
            point.Destroy()
            feature.Destroy()
            idi+=1
            nb+=1
    target_ds.Destroy()

def create_coord_pixel_center_raster(names,values,nline,ncol,Csize,Dir_temp):
    """
    Creer deux rasters correspondant aux coordonnees geographiques des centres de pixel
    """
    Xcoord = np.zeros((ncol),dtype=np.float)
    Ycoord = np.zeros((nline),dtype=np.float)
    y= values[3]+Csize*0.5
    for i in range(nline-1,-1,-1):
        Ycoord[i] = y
        y+= Csize
    x= values[2]+Csize*0.5
    for j in range(0,ncol,1):
        Xcoord[j]=x
        x+= Csize
    np.save(Dir_temp+'TableX.npy',Xcoord)
    np.save(Dir_temp+'TableY.npy',Ycoord)
    return Xcoord,Ycoord

def create_coord_pixel_center_raster2(names,values,nline,ncol,Csize):
    """
    Creer deux rasters correspondant aux coordonnees geographiques des centres de pixel
    """
    Xcoord = np.zeros((ncol),dtype=np.float)
    Ycoord = np.zeros((nline),dtype=np.float)
    y= values[3]+Csize*0.5
    for i in range(nline-1,-1,-1):
        Ycoord[i] = y
        y+= Csize
    x= values[2]+Csize*0.5
    for j in range(0,ncol,1):
        Xcoord[j]=x
        x+= Csize
    return Xcoord,Ycoord
    
# Process road network for cable analysis
def prepa_desserte_cable(Desserte_shapefile_name,MNT_file_name,Dir_temp,Pond_pente):
    """
    Process road network for cable analysis
    ----------
    Parameters
    ----------
    Desserte_shapefile_name:    string          Complete name of the road network shapefile
    MNT_file_name:              string          Complete name of the DTM raster
    Wspace:                     string          Workspace

    Returns
    -------
    Route :                     ndarray int8    Forest road location
    Az_route :                  ndarray int32   Forest road segments Azimuth 
    Fin_ligne :                 ndarray int8    ID of end of line 
    ----------
    Examples
    --------
    >>> import os,ogr,gdal,math
    >>> import numpy as np
    >>> Route,Az_route,Fin_ligne = prepa_desserte_cable("Desserte.shp","MNT.txt")
    """
    ### Get info on the area
    names,values,proj,Extent = raster_get_info(MNT_file_name)
    Csize,ncols,nrows = values[4],int(values[0]),int(values[1])  
    ### Get id of pixel of road and distance to public network
    Desserte_temp = shapefile_to_np_array(Desserte_shapefile_name,Extent,Csize,"CL_SVAC","CL_SVAC",'ASC')
    # Public network
    Res_pub = (Desserte_temp==3)*1
    # Forest road
    Route = (Desserte_temp==2)*1
    ID_RF = -9999*np.ones((nrows,ncols),dtype=np.int)
    ID_res_pub = -9999*np.ones((nrows,ncols),dtype=np.int)
    indice_forest_road = 0
    indice_public_road = 0
    pixels = np.argwhere(Desserte_temp>1)
    for pixel in pixels:
        if Res_pub[pixel[0],pixel[1]]==1:
            ID_res_pub[pixel[0],pixel[1]] = indice_public_road
            indice_public_road +=1
        elif Route[pixel[0],pixel[1]]==1:
            ID_RF[pixel[0],pixel[1]] = indice_forest_road
            indice_forest_road +=1
    np.save(Dir_temp+"ID_RF.npy",ID_RF)  
    np.save(Dir_temp+"ID_res_pub.npy",ID_res_pub)
    Dtransp_route, Lien_RF_respub = fc.calcul_distance_de_cout(ID_res_pub,Pond_pente,Route,Csize) 
    ### Get only forest roads
    Dir_temp2 = Dir_temp+"Temp/"
    try:os.mkdir(Dir_temp2)
    except:pass 
    Route_shp = Dir_temp2 + "Route.shp"
    select_in_shapefile(Desserte_shapefile_name,Route_shp,'WHERE CL_SVAC=2')
    ### Calculate azimuth and identify lines extremities
    Points_shp = Dir_temp2 + "Route_points.shp"    
    geoLocations,projection = linestring_to_point(Route_shp,Points_shp)
    # lines extremities
    Fin_ligne = np.int8(shapefile_to_np_array(Points_shp,Extent,Csize,'FIN_LIGNE','FIN_LIGNE','DESC'))
    # Azimuth 
    Az_route_shp = Dir_temp2 + "Az_Route.shp"
    points_to_lineshape(geoLocations,Az_route_shp,projection)
    Az_route = shapefile_to_np_array(Az_route_shp,Extent,Csize,'DIRECTION','DIRECTION','DESC')
    ID_routefor = np.unique(ID_RF)[1:]
    Link_RF_Res_pub = np.zeros((ID_routefor.shape[0],7),dtype=np.int)    
    for ind in ID_routefor:
        Temp = np.argwhere(ID_RF==ind)
        Link_RF_Res_pub[ind,0]=ind
        Link_RF_Res_pub[ind,1]=Temp[0,0]
        Link_RF_Res_pub[ind,2]=Temp[0,1]
        Link_RF_Res_pub[ind,3]=Dtransp_route[Temp[0,0],Temp[0,1]]
        Link_RF_Res_pub[ind,4]=Lien_RF_respub[Temp[0,0],Temp[0,1]]  
        Link_RF_Res_pub[ind,5]=Fin_ligne[Temp[0,0],Temp[0,1]]  
        Link_RF_Res_pub[ind,6]=Az_route[Temp[0,0],Temp[0,1]]
    shutil.rmtree(Dir_temp2)
    np.save(Dir_temp+"Link_RF_Res_pub",Link_RF_Res_pub)
    return Link_RF_Res_pub

def read_raster(file_name):
    source_ds = gdal.Open(file_name)
    source_ds.FlushCache() # Flush 
    Array = source_ds.GetRasterBand(1).ReadAsArray()
    Array[Array==0]=-9999
    return Array

# Create raster of obstacles from directory containing shapefiles 
def prepa_obstacle_cable(Obstacles_directory,file_MNT,Dir_temp):
    """
    Creer un raster contenant 1 sur les zones a obstacles, 0 ailleurs    
    """
    ### Creation d'un repertoire temporaire       
    names,values,rast_proj,Extent = raster_get_info(file_MNT)
    Csize= values[4]
    MNT = read_raster(file_MNT)
    MNT[MNT==values[5]]=-9999
    Pente = fc.pente(np.float_(MNT),Csize,-9999)     
    if Obstacles_directory!="":
        liste_file = os.listdir(Obstacles_directory)
        liste_obs = [] 
        for files in liste_file:
            if files[-4:len(files)]=='.shp':liste_obs.append(Obstacles_directory+files)
        if len(liste_obs)>0:
            Obstacles_cable = shapefile_obs_to_np_array(liste_obs,Extent,Csize)
        else: Obstacles_cable = np.zeros_like(MNT,dtype=np.int8)
    else: Obstacles_cable = np.zeros_like(MNT,dtype=np.int8)    
    Obstacles_cable[MNT==-9999]=1
    values[5]=-9999
    np.save(Dir_temp+'Obstacles_cables.npy',np.int8(Obstacles_cable))
    gc.collect()  
    return Pente

# Modify pond-pente array for cable analasis
def prepa_pond_pente_cable(MNT,Csize,Direct,head_text):
    Pente = fc.pente(MNT,Csize,-9999)
    Pond_pente = np.sqrt((Pente*0.01*Csize)**2+Csize**2)/float(Csize)
    Pond_pente[Pente==-9999] = 10000
    save_float_ascii(Direct+'/pond_pente.asc',head_text,Pond_pente)
    return Pond_pente
    
# Create a pair matrix
def create_pair_matrice(matrice):
    indices = np.indices(matrice.shape)
    pair_l = np.fmod(indices[0],2)
    pair_c = np.fmod(indices[1],2)
    pair = pair_c + pair_l
    pair = np.equal(pair,1)
    return pair

# Make buffer around a pixel
def buffer_emprise(Csize,ct_Lhor_max):
    temp = int(ct_Lhor_max/Csize)
    Ligne_perpendic = np.zeros((360*(1+2*temp),(1+2*temp)),dtype=np.uint8)
    Ind_mask = np.indices(((1+2*temp),(1+2*temp)),dtype=np.int16)
    center_inv = np.ones(((1+2*temp),(1+2*temp)),dtype=np.int16)
    center_inv[temp,temp]=0
    center = np.equal(center_inv,0)*1
    DX = Ind_mask[1]- temp
    DY = temp - Ind_mask[0]
    Dhor_ok = np.less_equal(Csize*np.sqrt(DX**2+DY**2),ct_Lhor_max)
    dmax1,dmin1,dmax2,dmin2 = direction_to_center(DX,DY,Csize,center_inv,center)
    z3_2 = np.less_equal((dmax2-dmin2),120)*1
    z3_1 = np.less_equal((dmax1-dmin1),120)*1
    Direction_list = range(0,360,1)
    for item in Direction_list:
        Dir_value = item+90
        if Dir_value > 359:Dir_value = item-90
        Mask1 = get_dir_area(Dir_value,dmax1,dmin1,dmax2,dmin2,center,z3_2,z3_1)
        Dir_value1 = Dir_value+180
        if Dir_value1 > 359:Dir_value1 = Dir_value-180
        Mask2 = get_dir_area(Dir_value1,dmax1,dmin1,dmax2,dmin2,center,z3_2,z3_1)
        Mask = np.greater((Mask1+Mask2),0)*Dhor_ok*1
        h = item*(1+2*temp)
        b = h+(1+2*temp)
        r = (1+2*temp)
        Ligne_perpendic[h:b,0:r]=Mask
    return Ligne_perpendic

# Calculate azimuth from a central pixel
def direction_to_center(DX,DY,Csize,center_inv,center):
    corner=[[0.5,0.5],[0.5,-0.5],[-0.5,0.5],[-0.5,0.5]]
    dmax1 = -400*np.ones_like(DX, dtype = np.int16)
    dmax2 = -400*np.ones_like(DX, dtype = np.int16)
    dmin1 = 400*np.ones_like(DX, dtype = np.int16)
    dmin2 = 400*np.ones_like(DX, dtype = np.int16)
    ind_center = np.nonzero(center==1)
    for item in corner:
        X = DX*Csize+item[1]*Csize
        Y = DY*Csize+item[0]*Csize
        D = np.sqrt(X**2+Y**2)
        Temp = np.equal(X,np.abs(X))*1
        Fact = Temp - np.less(Temp, 1)*1        
        D[ind_center] = Csize
        Angle = np.degrees(np.arccos(Y/D))
        Angle[ind_center]=0
        Temp = Angle*Fact
        del (X,Y,D,Fact,Angle)
        direction = 360*np.less(Temp, 0)+np.less(Temp, 0)*Temp+np.greater_equal(Temp, 0)*Temp
        dmax1=np.maximum(direction,dmax1)
        dmin1=np.minimum(direction,dmin1)
        dmax2=np.maximum(Temp,dmax2)
        dmin2=np.minimum(Temp,dmin2)
        del (Temp,direction)
    dmax1=np.float_(dmax1*center_inv+np.equal(center_inv,0)*9999)
    dmax2=np.float_(dmax2*center_inv+np.equal(center_inv,0)*9999)
    dmin1=np.float_(dmin1*center_inv+np.equal(center_inv,0)*9999)
    dmin2=np.float_(dmin2*center_inv+np.equal(center_inv,0)*9999)
    return dmax1,dmin1,dmax2,dmin2

#Get area corresponding to an azimuth
def get_dir_area(dir_value,dmax1,dmin1,dmax2,dmin2,center,z3_2,z3_1):
    if dir_value < 90:
        z1 = np.greater_equal(dir_value,dmin2)
        z2 = np.less_equal(dir_value,dmax2)
        zone = np.equal((z1*1+z2*1+z3_2*1),3)*1+center
    elif dir_value > 270:
        new_value = dir_value-360
        z1 = np.greater_equal(new_value,dmin2)
        z2 = np.less_equal(new_value,dmax2)
        zone = np.equal((z1*1+z2*1+z3_2*1),3)*1+center
    else:
        z1 = np.greater_equal(dir_value,dmin1)
        z2 = np.less_equal(dir_value,dmax1)
        zone = np.equal((z1*1+z2*1+z3_1*1),3)*1+center
    return zone

# Create useful buffers
def create_buffers(Csize,Lmax,Lmin):
    Buffer_cote = int(Lmax/Csize+0.5)
    dim_buf = Buffer_cote*2+1
    indices1 = np.indices((dim_buf,dim_buf))
    distX = indices1[1]-Buffer_cote
    distY = Buffer_cote - indices1[0]
    dist_euc = np.uint16(Csize*np.sqrt(distX**2+distY**2)+0.5)
    area = np.less_equal(dist_euc,Lmax)
    center = np.equal(dist_euc,0)
    ind_buffer_center = np.nonzero(center==1)
    center_inv = np.equal(center,0)
    dmax1,dmin1,dmax2,dmin2 = direction_to_center(distX,distY,Csize,center_inv,center)
    z3_2 = np.less_equal((dmax2-dmin2),120)
    z3_1 = np.less_equal((dmax1-dmin1),120)
    del (distX,distY,center_inv)
    Dir_list = range(0,360,1)
    Lident = list(np.zeros(360))
    Az_prev = (get_dir_area(359,dmax1,dmin1,dmax2,dmin2,center,z3_2,z3_1)*area)
    Dir_zone = np.zeros((360*(Buffer_cote+1),Buffer_cote+1),dtype=np.int8)    
    for item in Dir_list:
        if item <=90:
            h = 0
            b = Buffer_cote+1
            l = Buffer_cote
            r = dim_buf
        elif item <=180:
            h = Buffer_cote
            b = dim_buf
            l = Buffer_cote
            r = dim_buf
        elif item <=270:
            h = Buffer_cote
            b = dim_buf
            l = 0
            r = Buffer_cote+1    
        else:
            h = 0
            b = Buffer_cote+1
            l = 0
            r = Buffer_cote+1         
        zone = (get_dir_area(item,dmax1,dmin1,dmax2,dmin2,center,z3_2,z3_1)*area)
        #calculate_Lident
        try:Lident[item]=np.min(dist_euc[(zone+Az_prev)==1])
        except:Lident[item]=0
        Dir_zone[item*(Buffer_cote+1):(item+1)*(Buffer_cote+1)]=zone[h:b,l:r]
        Az_prev = zone        
    return Dir_list,Buffer_cote,Dir_zone,Lident

# Create useful buffers
def mask_buffers(test_coordY,test_coordX,ind_buffer_center,Buffer_cote,ncol,nline):
    if test_coordX < ind_buffer_center[1][0]: mask_l,buf_l = 0,ind_buffer_center[1][0] - test_coordX
    else: mask_l,buf_l = test_coordX - Buffer_cote,0
    if test_coordX + Buffer_cote + 1 > ncol: mask_r,buf_r = ncol,ncol-test_coordX+ind_buffer_center[1][0]
    else: mask_r,buf_r = test_coordX + Buffer_cote + 1,ind_buffer_center[1][0] + Buffer_cote + 1
    if test_coordY < ind_buffer_center[0][0]: mask_h,buf_h = 0,ind_buffer_center[0][0] - test_coordY
    else: mask_h,buf_h = test_coordY - Buffer_cote,0
    if test_coordY + Buffer_cote + 1 > nline: mask_b,buf_b = nline,nline - test_coordY + ind_buffer_center[0][0]
    else: mask_b,buf_b = test_coordY+Buffer_cote+1,ind_buffer_center[0][0]+Buffer_cote+1
    return mask_h,mask_b,mask_l,mask_r,buf_h,buf_b,buf_l,buf_r

# Renvoie les directions qu'il convient de tester sur une ligne
def directions_a_tester(Dir_route,Dir_list,angle_sup,id_fin_ligne):
    # Pixel tout seul
    Dir_list_bis = list(Dir_list)
    # Pixel de route en fin de troncon
    if id_fin_ligne==1:
        D_plus_a = (Dir_route+angle_sup)%360
        D_moins_a = (Dir_route-angle_sup)%360
        if D_moins_a > D_plus_a:valeur_a_suppr = range(D_moins_a,360,1)+range(0,D_plus_a+1,1)
        else:valeur_a_suppr = range(D_moins_a,D_plus_a+1,1)
        for item in valeur_a_suppr:
            try:Dir_list_bis.remove(item)
            except:continue
    # Pixel de route au milieu d'un troncon
    else:
        D_plus_a = (Dir_route+angle_sup)%360
        D_moins_a = (Dir_route-angle_sup)%360
        if D_moins_a > D_plus_a:valeur_a_suppr = range(D_moins_a,360,1)+range(0,D_plus_a+1,1)
        else:valeur_a_suppr = range(D_moins_a,D_plus_a+1,1)
        D_plus_180_moins_a = (Dir_route+180-angle_sup)%360
        D_plus_180_plus_a = (Dir_route+180+angle_sup)%360
        if D_plus_180_plus_a < D_plus_180_moins_a: valeur_a_suppr = valeur_a_suppr + range(D_plus_180_moins_a,360,1)+range(0,D_plus_180_plus_a+1,1)
        else:valeur_a_suppr = valeur_a_suppr + range(D_plus_180_moins_a,D_plus_180_plus_a+1,1)
        for item in valeur_a_suppr:
            try:Dir_list_bis.remove(item)
            except:continue    
    return Dir_list_bis


def get_cable_configs(Rspace,slope_Wliner_up,slope_Wliner_down,slope_grav,Cable_type,Carriage_type,Skid_direction,language='FR'):
    #Get folder
    dirs = [d for d in os.listdir(Rspace) if os.path.isdir(os.path.join(Rspace, d))]
    list_dir = []
    for dire in dirs:
        if dire[:5]=='Cable':
            list_dir.append(dire)
    optnum = len(list_dir)+1
    Rspace_c=Rspace+'Cable_'+str(optnum)        
    filename = Rspace_c+"/"
    ### Define cable type
    if Cable_type==0:
        if language == 'FR':
            filename +="Cable_mat_sur_tracteur_agricole"
        else:
            filename +="Cable_tower_on agricultural_tractor"
    elif Cable_type==1:
        if language=='FR':
            filename +="Cable_mat_sur_remorque"
        else:
            filename +="Cable_tower_on trailer"
    elif Cable_type==2:
        if language == 'FR':
            filename +="Cable_mat_sur_camion"
        else:
            filename +="Cable_tower_on truck"
    else:
        if language == 'FR':
            filename +="Cable_long"
        else:
            filename +="Long_cable"
    ### Define carriage type
    if Carriage_type:
        # self-motorized carriage
        if language == 'FR':
            filename += "_chariot_automoteur"
        else:
            filename += "_self-motorized_carriage"
        if Skid_direction ==0:
            if language == 'FR':
                filename += "_amont&aval"
            else:
                filename += "_uphill&downhill"
            slope_min_up = -atan(slope_Wliner_up*0.01)
            slope_max_up = 0.1
            slope_max_down = atan(slope_Wliner_down*0.01) 
            slope_min_down = -0.1        
        elif Skid_direction ==1:
            if language == 'FR':
                filename += "_amont"
            else:
                filename += "_uphill"
            slope_min_up = -atan(slope_Wliner_up*0.01)
            slope_max_up = 0.1
            slope_max_down = 0
            slope_min_down = 0
        else:
            if language == 'FR':
                filename += "_aval"
            else:
                filename += "_downhill"
            slope_min_up = 0
            slope_max_up = 0
            slope_max_down = atan(slope_Wliner_down*0.01) 
            slope_min_down = -0.1 
    else:
        if language == 'FR':
            filename += "_chariot_classique"
        else:
            filename += "_classical_carriage"
        # classical carriage on cable tower
        if Cable_type<3:
            if Skid_direction ==0:
                if language == 'FR':
                    filename += "_amont&aval"
                else:
                    filename += "_uphill&downhill"
                slope_min_up = -1.4
                slope_max_up = 0.1
                slope_min_down = -0.1
                slope_max_down = 1.4   
            elif Skid_direction ==1:
                if language == 'FR':
                    filename += "_amont"
                else:
                    filename += "_uphill"
                slope_min_up = -1.4
                slope_max_up = -atan(slope_grav*0.01)
                slope_min_down = 0
                slope_max_down = 0  
            else:
                if language == 'FR':
                    filename += "_aval"
                else:
                    filename += "_downhill"
                slope_min_up = 0
                slope_max_up = 0
                slope_min_down = -0.1
                slope_max_down = 1.4 
        else:
        # Long (conventional) cable 
            if Skid_direction ==0:
                if language == 'FR':
                    filename+= "_amont_aval"
                else:
                    filename += "_uphill&downhill"
                slope_min_up = -1.4
                slope_max_up = -atan(slope_grav*0.01)
                slope_max_down = 1.4
                slope_min_down = atan(slope_grav*0.01)  
            elif Skid_direction ==1:
                if language == 'FR':
                    filename += "_amont"
                else:
                    filename += "_uphill"
                slope_min_up = -1.4
                slope_max_up = -atan(slope_grav*0.01)
                slope_min_down = 0
                slope_max_down = 0
            else:
                if language == 'FR':
                    filename += "_aval"
                else:
                    filename += "_downhill"
                slope_min_up = 0
                slope_max_up = 0
                slope_max_down = 1.4
                slope_min_down = atan(slope_grav*0.01) 
    filename+=".txt"
    return Rspace_c,filename,slope_min_up,slope_max_up,slope_min_down,slope_max_down

###### Functions for best line selection

#Generer un tableau de type
# Crit_col Valeur_limite Poids

def gen_sel_table(w_list,lim_list,sup_max):
    col =     np.array([13       ,17     ,12         ,11    ,15    ,18+5*sup_max,16          ,14          ,18+5*sup_max+1])
    sens =    np.array([1        ,-1     ,0          ,1     ,1     ,1           ,1           ,-1          ,-1])
    #sens : 1:maximize,-1 minimize,0 NA
    report =  np.array([10000.   ,1.     ,1.         ,1.    ,1.    ,100.        ,10.         ,1.          ,1.])
    FR_name = np.array(['Surface','NBsup','SensDeb'  ,'Long','Vtot','IPC'       ,'VAM'       ,'Dchar'     ,'Cout'])
    EN_name = np.array(['Area'   ,'NBsup','Direction','Leng','Vtot','VperMeter' ,'AveTreeVol','AveCarDist','Cost'])
    #quant =   np.array[[99,      , 100   , 100       ,99    ,99    ,99          ,99          ,100          ,100]]
    Tab_crit=np.zeros((9,4))
    Tab_crit[:,0]=w_list
    Tab_crit[:,1]=lim_list*report    
    Tab_crit[:,2]=col
    Tab_crit[:,3]=sens
    FR_n = ""  
    EN_n = ""
    for i,crit in enumerate(Tab_crit):
        if crit[0]>0:
            FR_n+= '_'+str(FR_name[i])+'('+str(round(crit[0],1))+')'
            EN_n+= '_'+str(EN_name[i])+'('+str(round(crit[0],1))+')'
        
    return Tab_crit[Tab_crit[:,0]>0],FR_n,EN_n


def create_best_table(Tab2,w_list,lim_list,sup_max): 
    Tab_crit,FR_name,EN_name=gen_sel_table(w_list,lim_list,sup_max)
    #Trie en fonction des critere avec un poids et de la limite
    for crit in Tab_crit:
        if crit[3]>0:
            tp = (Tab2[:,int(crit[2])]-crit[1])>=0
        elif crit[3]<0:
            tp = (Tab2[:,int(crit[2])]-crit[1])<=0
        else:
            continue
        Tab2 = Tab2[tp]
        
    #identifier si le sens de debardage a ete choisi dans les criteres   
    liste =range(Tab_crit.shape[0])
    idsensdeb=np.argwhere(Tab_crit[:,2]==12)
    if idsensdeb.shape[0]>0:
        idsensdeb=idsensdeb[0,0]
        liste = [x for x in liste if x != idsensdeb]
    else:
        idsensdeb=-1
    #Transform variable to stick in the range [0-1+]
    nbcol = len(liste)+2    
    Tab = np.zeros((Tab2.shape[0],nbcol))    
    col=1   
    for crit in Tab_crit[liste]:
        if crit[3] < 1 : #all values contribute to transformation
            Tab[:,col]= (1-1.0*Tab2[:,int(crit[2])]/np.max(Tab2[:,int(crit[2])]))*crit[0]
        else:
            Tab[:,col]= (1.0*Tab2[:,int(crit[2])]/np.percentile(Tab2[:,int(crit[2])],98))*crit[0]
        col+=1
    
    for i in range(Tab2.shape[0]):
        Tab[i,0]=i                      #first col is idline of Tab2
        Tab[i,col]=np.sum(Tab[i,1:col]) #last col is the total weight
    #classify
    ordre = np.zeros((Tab2.shape[0],),dtype=np.int)   
    if idsensdeb>0:
        #first the best direction
        tp = Tab2[:,12]==Tab_crit[idsensdeb,1]        
        inds = np.lexsort((Tab[tp,0],-Tab[tp,col]))
        for i,ind in enumerate(inds):
            ordre[i]=int(Tab[tp][ind][0])
        ligne=i+1
        #then the othe direction
        tp = Tab2[:,12]==-Tab_crit[idsensdeb,1]        
        inds = np.lexsort((Tab[tp,0],-Tab[tp,col]))
        for i,ind in enumerate(inds):
            ordre[i+ligne]=int(Tab[tp][ind][0])
    else:
        inds = np.lexsort((Tab[:,0],-Tab[:,col]))
        for i,ind in enumerate(inds):
            ordre[i]=int(Tab[ind,0])
    return Tab2[ordre],FR_name,EN_name


def select_best_lines(w_list,lim_list,Tab2,nrows,ncols,Csize,Row_ext,Col_ext,D_ext,D_lat,Lhor_max,sup_max):        
    # Reorder Tab to fit with criteria
    Tabbis,FR_name,EN_name=create_best_table(Tab2,w_list,lim_list,sup_max)
    nb_line,nb_cols = Tabbis.shape
    Rast_couv=np.zeros((nrows,ncols),dtype=np.int8)
    vals = range(0,nb_line)
    ### Parameter to validate a line
    recouv = min(0.6*Lhor_max,Lhor_max-Csize) #distance from the axis of the line where crossing is not allowed
    # Select best lines
    vals2 = []
    for id_tab in vals:
        coordX,coordY = Tabbis[id_tab,-2],Tabbis[id_tab,-1]
        az,Lline=Tabbis[id_tab,1],sqrt((Tabbis[id_tab,2]-Tabbis[id_tab,6])**2+(Tabbis[id_tab,3]-Tabbis[id_tab,7])**2)  
        test_free,Rast_couv=fc.Check_line2(coordX,coordY,az,ncols,nrows,Lline,Row_ext,Col_ext,D_ext,D_lat,Rast_couv,recouv,0)
        if test_free:
            vals2.append(id_tab)
    # Check taht line contribute to total impacted surface
    Tab_result = np.zeros((len(vals2),2),dtype=np.int)
    id_line = 0    
    for id_tab in vals2:
        coordX,coordY = Tabbis[id_tab,-2],Tabbis[id_tab,-1]
        az,Lline=Tabbis[id_tab,1],sqrt((Tabbis[id_tab,2]-Tabbis[id_tab,6])**2+(Tabbis[id_tab,3]-Tabbis[id_tab,7])**2)  
        prop = fc.get_prop(coordX,coordY,az,ncols,nrows,Lline,Row_ext,Col_ext,D_ext,D_lat,Rast_couv)  
        Tab_result[id_line]=id_tab,prop*1000
        id_line+=1
    Tab_result=Tab_result[np.lexsort((Tab_result[:,0],Tab_result[:,1]))]  
    # Remove lines that does not contribute significantly to impacted surface
    nb_line = Tab_result.shape[0]
    Tab_result2 = np.zeros((nb_line,nb_cols-2),dtype=np.int)
    id_line = 0    
    for id_tab in Tab_result[:,0]:
        coordX,coordY = Tabbis[id_tab,-2],Tabbis[id_tab,-1]
        az,Lline=Tabbis[id_tab,1],sqrt((Tabbis[id_tab,2]-Tabbis[id_tab,6])**2+(Tabbis[id_tab,3]-Tabbis[id_tab,7])**2) 
        test_free,Rast_couv=fc.Check_line3(coordX,coordY,az,ncols,nrows,Lline,Row_ext,Col_ext,D_ext,D_lat,Rast_couv,0.6)         
        if test_free:
            Tab_result2[id_line]=Tabbis[id_tab,0:-2]
            id_line+=1    
    Tab_result2=Tab_result2[Tab_result2[:,11]>0]
    return Rast_couv,Tab_result2,FR_name,EN_name           

def return_crit_text(w_list,lim_list,language):    
    FR_name =["Surface de forêt impactée [ha] (maximiser)                  Minimum : ", 
              "Nombre de support intermédiaires (minimiser)                Maximum : ",
              "Privilégier le débardage vers ",
              "Longueur de ligne [m] (maximiser)                           Minimum : ", 
              "Volume total par ligne [m3] (maximiser)                     Minimum : ",
              "Indice de prélèvement câble [m3/ml] (maximiser)             Minimum : ",
              "Volume de l\'arbre moyen [m3] (maximiser)                   Minimum : ",
              "Longueur moyenne parcourue par le charriot [m] (minimiser)  Maximum : ",
              "Coût du débardage [€/m3] (minimiser)                        Maximum : "]                
    EN_name = ["Forest area impacted [ha] (maximize)                 Minimum: ",
               "Number of intermediate supports (minimize)           Maximum: ",
               "Favour ", 
               "Line length [m] (maximize)                           Minimum: ",
               "Total volume per line [m3] (maximize)                Minimum: ",
               "Volume per meter of line  [m3/ml] (maximize)         Minimum: ", 
               "Average tree volume [m3] (maximize)                  Minimum: ",
               "Carriage average distance [m] (minimize)             Maximum: ", 
               "Yarding cost [€/ml] (minimize)                       Maximum: "]    
    units = ["ha","","","m","m3","m3/ml","m3","m","€/m3"]
    if language=='FR':
        namelist=FR_name
        pname = "(Poids: "        
    else:
        namelist=EN_name
        pname = "(Weight: "        
    Tab = np.empty((np.sum(np.array(w_list)>0),2),dtype='|U73')
    j=-1
    for i,w in enumerate(w_list):
        if w!=0:
            j+=1
            if i!=2:
                if round(lim_list[i],0)==lim_list[i]:
                    lim=int(lim_list[i])
                else:
                    lim=round(lim_list[i],1)
                Tab[j]=namelist[i],str(lim)+" "+units[i]+" "+pname+str(w)+')' 
            else:
                if lim_list[i]==-1:
                    if language=='FR':
                        Tab[j]= FR_name[i]+"l\'aval",pname+str(w)+')' 
                    else:
                        Tab[j]= EN_name[i]+"downhill yarding",pname+str(w)+')' 
                elif lim_list[i]==1:
                    if language=='FR':
                        Tab[j]= FR_name[i]+"l\'amont",pname+str(w)+')' 
                    else:
                        Tab[j]= EN_name[i]+"uphill yarding",pname+str(w)+')' 
                else:
                    continue                    
    return Tab


def generate_info_ligne(Dir_result,w_list,lim_list,Tab,language,Rast_couv,Vol_ha,Vol_AM,Csize,prelevement,Lhor_max):
    if language=='FR':
        filename = Dir_result+"Bilan_selection.txt"
    else:
        filename = Dir_result+"Summary_selection.txt"
    pix_area = Csize*Csize/10000.
    Proj = np.copy(Rast_couv)
    Proj[Rast_couv==2]=0
    Rast_couv[Rast_couv==2]=1
    Surface = round(np.sum(Rast_couv)*pix_area,1)
    Surface_proj = round(np.sum(Proj)*pix_area,1)
    if Surface_proj>0:
        testProj=1
    else:
        testProj=0
    nb_ligne = Tab.shape[0]
    nb_ligne_amont = int(np.sum(Tab[:,12]>0))
    try:
        nb_moy_pyl = round(np.sum(Tab[:,17])/nb_ligne,1)
        long_moy_ligne = int(np.sum(Tab[:,11])/nb_ligne)
    except:
        if language=="FR":
            print("Aucune ligne n'a ete selectionnee")
        else:
            print("No cable line selected")
    Vol_ha[np.isnan(Vol_ha)]=0
    Vol_AM[np.isnan(Vol_AM)]=0
    tp =   Vol_ha>0
    if np.sum(tp)>0:      
        vtot = np.sum(Rast_couv[tp]*Vol_ha[tp])*pix_area*prelevement
    else:
        vtot=0
    tp =   Vol_AM>0 
    if np.sum(tp)>0:
        vam = round(np.mean(Rast_couv[tp]*Vol_AM[tp]),1)
    else:
        vam=0
    if np.sum(Tab[:,11])!=0:
        ipc_moy = round(float(vtot)/np.sum(Tab[:,11]),2)
    else:
        if language=="FR":
            print("Aucune ligne n'a ete selectionnee")
        else:
            print("No cable line selected")
    vtot = int(vtot)
    lim_list[4]=lim_list[4]*prelevement
    
    Table = np.empty((19+np.sum(np.array(w_list)>0),2+testProj),dtype='|U73')
    
    if language == 'FR':
        Table[1,0]= "BILAN DE LA SELECTION DE LIGNE"
        Table[3,1]= "\t\t\t\t\t\t\tDepuis tous les départs\t\t"
        if testProj:
            Table[3,2]="Seulement depuis les projets"
        Table[4,0]= "Surface totale de forêt traitée [ha]:\t\t\t"
        Table[5,0]= "Nombre total de ligne:\t\t\t\t\t"
        Table[6,0]= "     + Dont ligne avec débardage vers l'amont:\t\t"
        Table[7,0]= "     + Dont ligne avec débardage vers l'aval:\t\t"
        Table[8,0]= "Nombre moyen de pylône intermédiaire par ligne:\t\t"
        Table[9,0]= "Longueur moyenne des lignes [m]:\t\t\t"
        Table[10,0]="Volume total prélevé (estimation) [m3]:\t\t\t"
        Table[11,0]="Indice de prélevement câble moyen (estimation) [m3/m]:\t"
        Table[12,0]="Volume de l'arbre moyen (estimation) [m3]:\t\t"
        Table[15,0]="Critère(s) pris en compte dans la sélection des lignes:"
        Table[17,0]="Distance laterale de pechage des bois:                      "
        Table[18,0]="Taux de prelevement du volume sur pied:                     "
    else:
        Table[1,0]= "SUMMARY OF THE LINE SELECTION"
        Table[3,0]= "                                                         "
        Table[3,1]= "From all cable starts\t\t"
        if testProj:
            Table[3,2]="Only from projects"
        Table[4,0]= "   - Total forest area impacted [ha]:                    "
        Table[5,0]= "   - Total number of lines:                              "
        Table[6,0]= "         + With uphill yarding:                          "
        Table[7,0]= "         + With downhill yarding:                        "
        Table[8,0]= "   - Average number of intermediate support per line:    "
        Table[9,0]= "   - Average length of the line [m]:                     "
        Table[10,0]="   - Total harvested volume (estimate) [m3]:             "
        Table[11,0]="   - Average volume per meter of line (estimate) [m3/m]: "
        Table[12,0]="   - Average tree volume (estimate) [m3]                 "
        Table[18,0]="Criteria taken into account for the selection:           "
        Table[17,0]="Lateral yarding distance                             "
        Table[18,0]="Proportion of stand volume removed                   "
    
    
    Table[4,1]= str(Surface)
    Table[5,1]= str(nb_ligne)
    Table[6,1]= str(nb_ligne_amont)
    Table[7,1]= str(nb_ligne-nb_ligne_amont)
    Table[8,1]= str(nb_moy_pyl)
    Table[9,1]= str(long_moy_ligne)
    Table[10,1]=str(vtot)
    Table[11,1]=str(ipc_moy)
    Table[12,1]=str(vam)
    Table[17,1]=str(int(Lhor_max))+" m"
    Table[18,1]=str(int(prelevement*100))+" %"
    
    Tabcrit = return_crit_text(w_list,lim_list,language)
    
    for i,crit in enumerate(Tabcrit):
        Table[19+i,0]=crit[0]
        Table[19+i,1]=crit[1]
        
    
    if testProj:
        Tab= Tab[Tab[:,10]==1]
        Rast_couv = Proj
        nb_ligne = Tab.shape[0]
        nb_ligne_amont = int(np.sum(Tab[:,12]>0))
        try:
            nb_moy_pyl = round(np.sum(Tab[:,17])/nb_ligne,1)
            long_moy_ligne = int(np.sum(Tab[:,11])/nb_ligne)
        except:
            if language=="FR":
                print("Aucune ligne n'a ete selectionnee")
            else:
                print("No cable line selected")
        tp =   Vol_ha>0
        if np.sum(tp)>0:      
            vtot = np.sum(Rast_couv[tp]*Vol_ha[tp])*pix_area*prelevement
        else:
            vtot=0
        tp =   Vol_AM>0 
        if np.sum(tp)>0:
            vam = round(np.mean(Rast_couv[tp]*Vol_AM[tp]),1)
        else:
            vam=0
        if np.sum(Tab[:,11])!=0:
            ipc_moy = round(float(vtot)/np.sum(Tab[:,11]),2)
        else:
            if language=="FR":
                print("Aucune ligne n'a ete selectionnee")
            else:
                print("No cable line selected")
        vtot = int(vtot)
        
        Table[4,2]= "\t\t\t\t"+str(Surface_proj)
        Table[5,2]= "\t\t\t\t"+str(nb_ligne)
        Table[6,2]= "\t\t\t\t"+str(nb_ligne_amont)
        Table[7,2]= "\t\t\t\t"+str(nb_ligne-nb_ligne_amont)
        Table[8,2]= "\t\t\t\t"+str(nb_moy_pyl)
        Table[9,2]= "\t\t\t\t"+str(long_moy_ligne)
        Table[10,2]="\t\t\t\t"+str(vtot)
        Table[11,2]="\t\t\t\t"+str(ipc_moy)
        Table[12,2]="\t\t\t\t"+str(vam)
           
    np.savetxt(filename, Table,fmt='%s', delimiter='')




#def generate_info_ligne(Dir_result,w_list,lim_list,Tab,language,Rast_couv,Vol_ha,Vol_AM,Csize,prelevement,Lhor_max):
#    if language=='FR':
#        filename = Dir_result+"Bilan_selection.txt"
#    else:
#        filename = Dir_result+"Summary_selection.txt"
#    pix_area = Csize*Csize/10000.
#    Surface = round(np.sum(Rast_couv)*pix_area,1)
#    nb_ligne = Tab.shape[0]
#    nb_ligne_amont = int(np.sum(Tab[:,12]>0))
#    try:
#        nb_moy_pyl = round(np.sum(Tab[:,17])/nb_ligne,1)
#        long_moy_ligne = int(np.sum(Tab[:,11])/nb_ligne)
#    except:
#        if language=="FR":
#            print("Aucune ligne n'a ete selectionnee")
#        else:
#            print("No cable line selected")
#    tp =   Vol_ha>0
#    if np.sum(tp)>0:      
#        vtot = np.sum(Rast_couv[tp]*Vol_ha[tp])*pix_area*prelevement
#    else:
#        vtot=0
#    tp =   Vol_AM>0 
#    if np.sum(tp)>0:
#        vam = round(np.mean(Rast_couv[tp]*Vol_AM[tp]),1)
#    else:
#        vam=0
#    if np.sum(Tab[:,11])!=0:
#        ipc_moy = round(float(vtot)/np.sum(Tab[:,11]),2)
#    else:
#        if language=="FR":
#            print("Aucune ligne n'a ete selectionnee")
#        else:
#            print("No cable line selected")
#    vtot = int(vtot)
#    lim_list[4]=lim_list[4]*prelevement
#    if language == 'FR':
#        info_ligne = "\nBILAN DE LA SELECTION DE LIGNE\n\n"        
#        info_ligne += "   - Surface totale de forêt traitée:                    "+str(Surface)+" ha\n"
#        info_ligne += "   - Nombre total de ligne:                              "+str(nb_ligne)+"\n"
#        info_ligne += "         + Dont ligne avec débardage vers l'amont:       "+str(nb_ligne_amont)+"\n"
#        info_ligne += "         + Dont ligne avec débardage vers l'aval:        "+str(nb_ligne-nb_ligne_amont)+"\n"
#        info_ligne += "   - Nombre moyen de pylône intermédiaire par ligne:     "+str(nb_moy_pyl)+"\n"
#        info_ligne += "   - Longueur moyenne des lignes:                        "+str(long_moy_ligne)+" m\n"
#        info_ligne += "   - Volume total prélevé (estimation):                  "+str(vtot)+" m3\n"
#        info_ligne += "   - Indice de prélevement câble moyen (estimation):     "+str(ipc_moy)+" m3/m\n"
#        info_ligne += "   - Volume de l'arbre moyen (estimation):               "+str(vam)+" m3\n"    
#        info_ligne +=return_crit_text(w_list,lim_list,language)       
#        info_ligne +="Distance laterale de pechage des bois                          - "+str(int(Lhor_max))+" m\n"  
#        info_ligne +="Taux de prelevement du volume sur pied                         - "+str(int(prelevement*100))+" %\n"                       
#    else:
#        info_ligne = "\nSUMMARY OF THE LINE SELECTION\n\n"        
#        info_ligne += "   - Total forest area impacted:                      "+str(Surface)+" ha\n"
#        info_ligne += "   - Total number of lines:                           "+str(nb_ligne)+"\n"
#        info_ligne += "         + With uphill yarding:                       "+str(nb_ligne_amont)+"\n"
#        info_ligne += "         + With downhill yarding:                     "+str(nb_ligne-nb_ligne_amont)+"\n"
#        info_ligne += "   - Average number of intermediate support per line: "+str(nb_moy_pyl)+"\n"
#        info_ligne += "   - Average length of the line:                      "+str(long_moy_ligne)+" m\n"
#        info_ligne += "   - Total harvested volume (estimate):               "+str(vtot)+" m3\n"
#        info_ligne += "   - Average volume per meter of line (estimate):     "+str(ipc_moy)+" m3/m\n"
#        info_ligne += "   - Average tree volume (estimate):                  "+str(vam)+" m3\n"           
#        info_ligne +=return_crit_text(w_list,lim_list,language)
#        info_ligne +="Lateral yarding distance                                       - "+str(int(Lhor_max))+" m\n"  
#        info_ligne +="Proportion of stand volume removed                             - "+str(int(prelevement*100))+" %\n" 
#        
#        
#    fichier = open(filename, "w")
#    fichier.write(info_ligne)
#    fichier.close()   
    
    
def generate_info_cable_simu(Dir_result,Tab,language,Rast_couv,Vol_ha,Csize,Forest,Pente,Pente_max_bucheron):
    if language=='FR':
        filename = Dir_result+"Resume_resultat_sylvaccess_cable.txt"
    else:
        filename = Dir_result+"Summary_results_sylvaccess_cable.txt"
    Pente_max = fc.focal_stat_max(np.float_(Pente),-9999,1)
    Pente_ok_buch = np.int8((Pente_max<=Pente_max_bucheron))
    del Pente_max
    gc.collect()    
    pix_area = Csize*Csize/10000.
    Rast_couv[Forest==0]=0
    Surface_exis = round(np.sum(Rast_couv==2)*pix_area,1)
    Surface_proj = round(np.sum(Rast_couv==1)*pix_area,1)
    Surface_foret = round(np.sum(Forest==1)*pix_area,1)    
    Surface_nonbuch = round(np.sum((Forest==1)*(Pente_ok_buch==0))*pix_area,1)
        
    Vol_ha[np.isnan(Vol_ha)]=0
    Vol_ha[Forest==0]=0
    tp =  Vol_ha>0
    if np.sum(tp)>0:   
        tp2 = (tp*(Rast_couv==2))>0
        vtot_exis = int(np.sum(Vol_ha[tp2])*pix_area+0.5)
        tp2 = (tp*(Rast_couv==1))>0
        vtot_proj = int(np.sum(Vol_ha[tp2])*pix_area+0.5)     
        tp2 = (tp*(Forest==1))>0
        vtot_forest = int(np.sum(Vol_ha[tp2])*pix_area+0.5)   
        tp2 = (tp*(Forest==1)*(Pente>Pente_max_bucheron))>0
        vtot_nonbuch = int(np.sum(Vol_ha[tp2])*pix_area+0.5)   
    else:
        vtot_exis = 0  
        vtot_proj = 0
        vtot_forest = 0
        vtot_nonbuch = 0
        
    nb_ligne = Tab.shape[0]
    nb_ligne_amont = int(np.sum(Tab[:,12]>0))
    nb_moy_pyl = round(np.sum(Tab[:,17])/nb_ligne,1)
    long_moy_ligne = int(np.sum(Tab[:,11])/nb_ligne)    
        
        
    Table = np.empty((17,5),dtype='|U39')
    if language == 'EN':   
        Table[0] = np.array(["","Surface area (ha)","Surface (%)","Volume (m3)","Volume (%)"])
        Table[1,0] = "From existing cable starts"
        Table[2,0] = "From potential cable start"
        Table[4,0] = "Total accessible forest"
        Table[5,0] = "Total unaccessible forest"
        Table[6,0] = "    including impossible manual felling"
        Table[8,0] = "Total forest area"
        Table[11,0] = "Total number of lines"
        Table[12,0] = "    + With uphill yarding"
        Table[13,0] = "    + With downhill yarding"
        Table[15,0] = "Average length of the line (m)"
        Table[16,0] = "Average number of intermediate support"
        
    else:
        Table[0] = np.array(["","Surface (ha)","Surface (%)","Volume sur pied (m3)","Volume (%)"])
        Table[1,0] = "Depuis les departs de cable existant"
        Table[2,0] = "Depuis les departs de cable en projet"
        Table[4,0] = "Total foret accessible"
        Table[5,0] = "Total foret inaccessible"
        Table[6,0] = "    dont non bucheronnable"
        Table[8,0] = "Superficie totale de la foret"
        Table[11,0] = "Nombre total de ligne"
        Table[12,0] = "    + Dont debardage vers l'amont"
        Table[13,0] = "    + Dont debardage vers l'aval"
        Table[15,0] = "Longueur moyenne des lignes (m)"
        Table[16,0] = "Nombre moyen de pylone intermediaire"
        
    #Create recap per distance class 
    if vtot_forest>0:
        Table[1,1:] = np.array([str(Surface_exis),str(int(Surface_exis/Surface_foret*100+0.5)),
                                str(vtot_exis),str(int(vtot_exis/vtot_forest*100+0.5))])
        Table[2,1:] = np.array([str(Surface_proj),str(int(Surface_proj/Surface_foret*100+0.5)),
                                str(vtot_proj),str(int(vtot_proj/vtot_forest*100+0.5))])
        
        Table[4,1:] = np.array([str(Surface_exis+Surface_proj),str(int((Surface_proj+Surface_exis)/Surface_foret*100+0.5)),
                                str(vtot_proj+vtot_exis),str(int((vtot_exis+vtot_proj)/vtot_forest*100+0.5))])
        Table[5,1:] = np.array([str(round(Surface_foret-(Surface_exis+Surface_proj),1)),str(int((Surface_foret-(Surface_proj+Surface_exis))/Surface_foret*100+0.5)),
                                str(vtot_forest-(vtot_proj+vtot_exis)),str(int((vtot_forest-(vtot_exis+vtot_proj))/vtot_forest*100+0.5))])
        Table[6,1:] = np.array([str(Surface_nonbuch),str(int(Surface_nonbuch/Surface_foret*100+0.5)),
                                str(vtot_nonbuch),str(int(vtot_nonbuch/vtot_forest*100+0.5))])
    else:
        Table[1,1:] = np.array([str(Surface_exis),str(int(Surface_exis/Surface_foret*100+0.5)),'0','0'])
        Table[2,1:] = np.array([str(Surface_proj),str(int(Surface_proj/Surface_foret*100+0.5)),'0','0'])
                            
        
        Table[4,1:] = np.array([str(Surface_exis+Surface_proj),str(int((Surface_proj+Surface_exis)/Surface_foret*100+0.5)),'0','0'])
        Table[5,1:] = np.array([str(round(Surface_foret-(Surface_exis+Surface_proj),1)),str(int((Surface_foret-(Surface_proj+Surface_exis))/Surface_foret*100+0.5)),'0','0'])
        Table[6,1:] = np.array([str(Surface_nonbuch),str(int(Surface_nonbuch/Surface_foret*100+0.5)),'0','0'])
    
    Table[8,1:] = np.array([str(Surface_foret),"",str(vtot_forest),""])
    
    Table[11,1] = str(nb_ligne)
    Table[12,1] = str(nb_ligne_amont)
    Table[13,1] = str(nb_ligne-nb_ligne_amont)
    Table[15,1] = str(long_moy_ligne)
    Table[16,1] = str(nb_moy_pyl)
    
    np.savetxt(filename, Table,fmt='%s', delimiter=';')
            

def calculate_azimut(x1,y1,x2,y2):
    """
    Calculate the azimuth between two points from their coordinates
    """
    DX = x2-x1
    DY = y2-y1
    Deuc = math.sqrt(DX**2+DY**2)
    if x2>x1:Fact=1
    else:Fact=-1
    Angle = math.degrees(math.acos(DY/Deuc))
    Angle *=Fact
    return Angle%360        

def create_rast_couv(Tab_result,Dir_result,source_src,Extent,Csize,Lhor_max):
    drv = ogr.GetDriverByName("ESRI Shapefile")    
    layer_name = "Extent"
    dst_ds = drv.CreateDataSource( Dir_result+layer_name+".shp" )
    dst_layer = dst_ds.CreateLayer(layer_name, source_src , ogr.wkbPolygon)
    raster_field = ogr.FieldDefn('EXIST', ogr.OFTInteger)
    dst_layer.CreateField(raster_field)
    layerDefinition = dst_layer.GetLayerDefn()  
    for idi,line in enumerate(Tab_result):
        Xstart,Ystart,Xend,Yend = line[2],line[3],line[6],line[7]
        az = calculate_azimut(Xstart,Ystart,Xend,Yend)
        conv = radians(90)
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddPoint(Xstart+Lhor_max*cos(az-conv), Ystart+Lhor_max*sin(az-conv))
        ring.AddPoint(Xend+Lhor_max*cos(az-conv), Yend+Lhor_max*sin(az-conv))
        ring.AddPoint(Xend+Lhor_max*cos(az+conv), Yend+Lhor_max*sin(az+conv))
        ring.AddPoint(Xstart+Lhor_max*cos(az+conv), Ystart+Lhor_max*sin(az+conv))
        ring.AddPoint(Xstart+Lhor_max*cos(az-conv), Ystart+Lhor_max*sin(az-conv))
        poly = ogr.Geometry(ogr.wkbPolygon)
        poly.AddGeometry(ring)
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(poly)
        feature.SetFID(idi)        
        feature.SetField('EXIST',int(line[10]))
        dst_layer.CreateFeature(feature)
        ring.Destroy()
        poly.Destroy()
        feature.Destroy()
    dst_ds.Destroy()
    Rast_couv = np.int8(shapefile_to_np_array(Dir_result+layer_name+".shp",Extent,Csize,"EXIST","EXIST",'ASC') )
    for extension in [".shp",".prj",".shx",".dbf"]:
        os.remove(Dir_result+layer_name+extension)
    return Rast_couv
        

################################################################################################################################################
### Functions for sylvaccess skidder
################################################################################################################################################
def create_new_road_network(file_shp_Desserte,Wspace):
    Dir_temp = Wspace+"Temp/"    
    try:os.mkdir(Dir_temp)
    except:pass 
    File_fin = Dir_temp+"Existing_roads.shp"    
    source_ds = ogr.Open(file_shp_Desserte)
    source_layer = source_ds.GetLayer()
    source_src = source_layer.GetSpatialRef()
    driver = ogr.GetDriverByName('ESRI Shapefile')
    target_ds = driver.CreateDataSource(File_fin)
    layerName = os.path.splitext(os.path.split(File_fin)[1])[0]
    layer = target_ds.CreateLayer(layerName, source_src, ogr.wkbLineString)
    layerDefinition = layer.GetLayerDefn()  
    new_field = ogr.FieldDefn("CL_SVAC", ogr.OFTInteger)
    layer.CreateField(new_field)
    for feat in source_layer:
        geometry = feat.GetGeometryRef()
        label = feat.GetField('EXIST')
        if label == 2:
            feature = ogr.Feature(layerDefinition)
            feature.SetGeometry(geometry)
            feature.SetField("CL_SVAC",feat.GetField("CL_SVAC"))
            layer.CreateFeature(feature)
            feature.Destroy()
    source_ds.Destroy()
    target_ds.Destroy()
    return File_fin
        
####MODIF HERE
def make_summary_surface_vol(Debclass,file_Vol_ha,Surf_foret,Surf_foret_non_access,Csize,Dtotal,Vtot,Vtot_non_buch,Rspace_s,modele_name,language):
    Skid_list = Debclass.split(";")
    nbclass = len(Skid_list)
            
    Table = np.empty((nbclass+7,9),dtype='|U39')
    if language == 'EN':
        Table[0] = np.array(["Total yarding distance",
                             "Surface area (ha)","Surface per class (%)","Cumulative surface (ha)","Cumulative surface (%)",
                             "Volume (m3)","Volume per class (%)","Cumulative volume (m3)","Cumulative volume (%)"])
        
        Table[nbclass+2:nbclass+7,0]=["Total accessible forest","Total unaccessible forest",
                                      "    including impossible manual felling","",
                                      "Total area of forest"]
        file_name = str(Rspace_s)+"Summary_results_Sylvaccess_"+modele_name+".txt"   
    else:
        Table[0] = np.array(["Distance totale de debardage",
                             "Surface (ha)","Surface par classe (%)","Surface cumulee (ha)","Surface cumulee (%)",
                             "Volume (m3)","Volume par classe (%)","Volume cumule (m3)","Volume cumule (%)"])    
        Table[nbclass+2:nbclass+7,0]=["Total foret accessible","Total foret inaccessible",
                                      "    dont non bucheronnable","",
                                      "Superficie totale de la foret"]
        file_name = str(Rspace_s)+"Resume_resultats_Sylvaccess_"+modele_name+".txt"  
        
    #SURFACE
    Surf_Cum = 0 
    for i in range(1,nbclass):
        dmin,dmax = int(Skid_list[i-1]),int(Skid_list[i])
        class_text = str(Skid_list[i-1])+" - "+str(Skid_list[i])+" m"
        Temp = np.sum((Dtotal>=dmin)*(Dtotal<dmax)*Csize*Csize*0.0001)
        Table[i,0:5] = np.array([class_text,str(round(Temp,1)),
                                str(round(Temp/Surf_foret*100,1)),
                                str(round((Temp+Surf_Cum),1)),
                                str(round((Temp+Surf_Cum)/Surf_foret*100,1))])
        Surf_Cum += Temp
    #add infinite distance class 
    dmin = int(Skid_list[nbclass-1])
    Temp = np.sum((Dtotal>=dmin)*Csize*Csize*0.0001)
    Table[nbclass,0:5] = np.array(["> "+str(dmin)+" m",str(round(Temp,1)),
                                   str(round(Temp/Surf_foret*100,1)),
                                   str(round((Temp+Surf_Cum),1)),
                                   str(round((Temp+Surf_Cum)/Surf_foret*100,1))])
    Surf_Cum += Temp  
         
    Table[-5,1] = str(round(Surf_Cum,1))+" ha"
    Table[-5,2] = str(round(Surf_Cum/Surf_foret*100,1))+" %"
    Table[-4,1] = str(round(Surf_foret-Surf_Cum,1))+" ha"
    Table[-4,2] = str(round((Surf_foret-Surf_Cum)/Surf_foret*100,1))+" %"
    Table[-3,1] = str(round(Surf_foret_non_access,1))+" ha"
    Table[-3,2] = str(round(Surf_foret_non_access/Surf_foret*100,1))+" %"        
    Table[-1,1] = str(round(Surf_foret,1))+" ha"
        
    #VOLUME            
    if file_Vol_ha != "":
        Vol_ha = load_float_raster_simple(file_Vol_ha)
        Vol_ha[np.isnan(Vol_ha)]=0
                
        #Create recap per distance class 
        Vol_Cum = 0
        for i in range(1,nbclass):
            dmin,dmax = int(Skid_list[i-1]),int(Skid_list[i])            
            Temp = ((Dtotal>=dmin)*(Dtotal<dmax)*(Vol_ha>=0))>0
            if np.sum(Temp>0):
                Vclass = np.mean(Vol_ha[Temp])*np.sum(Temp)*Csize*Csize*0.0001
            else:
                Vclass = 0
            Table[i,5:9] = np.array([str(int(Vclass+0.5)),str(round(Vclass/Vtot*100,1)),
                                    str(int(Vclass+Vol_Cum+0.5)),
                                    str(round(100*(Vclass+Vol_Cum)/Vtot,1))])
            Vol_Cum +=Vclass
        #add infinite distance class 
        dmin = int(Skid_list[nbclass-1])
        Temp = ((Dtotal>=dmin)*(Vol_ha>=0))>0
        if np.sum(Temp>0):
            Vclass = np.mean(Vol_ha[Temp])*np.sum(Temp)*Csize*Csize*0.0001
        else:
            Vclass = 0
        Table[nbclass,5:9] = np.array([str(int(Vclass+0.5)),str(round(Vclass/Vtot*100,1)),
                                    str(int(Vclass+Vol_Cum+0.5)),
                                    str(round(100*(Vclass+Vol_Cum)/Vtot,1))])
          
        Vol_Cum +=Vclass        
        Table[-5,5] = str(int(Vol_Cum+0.5))+" m3"
        Table[-5,6] = str(round(100*Vol_Cum/Vtot,1))+" %"
        Table[-4,5] = str(int(Vtot-Vol_Cum+0.5))+" m3"
        Table[-4,6] = str(round(100*(Vtot-Vol_Cum)/Vtot,1))+" %"
        Table[-3,5] = str(int(Vtot_non_buch+0.5))+" m3"
        Table[-3,6] = str(round(100*(Vtot_non_buch)/Vtot,1))+" %"  
        Table[-1,5] = str(int(Vtot+0.5))+" m3"
      
        np.savetxt(file_name, Table,fmt='%s', delimiter=';')
        
    else:
        np.savetxt(file_name, Table[:,0:5],fmt='%s', delimiter=';')

# def make_dif_files(language,Rspace,idmod):#idmod 0 : Skidder, 1 : Porteur
#     ModeleFr = ["Skidder","Porteur"]
#     ModeleEn = ["Skidder","Forwarder"]
    
#     if language=="FR":
#         Rspace_s = Rspace+ModeleFr[idmod]+"/"
#         filename = "Resume_resultats_Sylvaccess_"+ModeleFr[idmod]+".txt"
#         rastname = "Distance_totale_foret_route_forestiere.tif"
#         foldExist = Rspace_s+"1_Existant/"
#         foldProj = Rspace_s+"2_Projet/"
#         txtname = Rspace_s+"Comparaison_avant_apres_projet.txt"
#         shpname = "Surface_impactee"
#     else:
#         Rspace_s = Rspace+ModeleEn[idmod]+"/"
#         filename = "Summary_results_Sylvaccess_"+ModeleEn[idmod]+".txt"
#         rastname = "Total_distance.tif"
#         foldExist = Rspace_s+"1_Existing/"
#         foldProj = Rspace_s+"2_Project/"
#         txtname = Rspace_s+"Comparison_before_after_project.txt"
#         shpname = "Impacted_area"
        
        
#     #Shapefile des differences
#     names,values,proj,Extent = raster_get_info(foldExist+rastname)
#     Csize = values[4]
#     DExist =  load_float_raster_simple(foldExist+rastname)
#     DProj =  load_float_raster_simple(foldProj+rastname)
#     nrows,ncols = DProj.shape
#     Diff2 = DExist-DProj
#     Diff2[DExist==-9999]=0
#     Diff2[DProj==-9999]=0
#     Diff = np.zeros_like(DExist,dtype=np.int8)
#     Diff[((DExist==-9999)*(DProj>=0))>0] = 1 #Nouvel accessible
#     Diff[((Diff2>0)*(Diff2<500))>0] = 2 
#     Diff[((Diff2>500)*(Diff2<1000))>0] = 3
#     Diff[((Diff2>1000)*(Diff2<1500))>0] = 4
#     Diff[Diff2>1500] = 5
#     Surf_impact = round(np.sum(Diff>0)*Csize*Csize/10000,1)
    
#     if language=="FR":
#         road_network_proj=get_proj_from_road_network(foldExist+ModeleFr[idmod]+"_recap_accessibilite.shp")
#         source_src=get_source_src(foldExist+ModeleFr[idmod]+"_recap_accessibilite.shp") 
#     else:
#         road_network_proj=get_proj_from_road_network(foldExist+ModeleEn[idmod]+"_recap_accessibility.shp")
#         source_src=get_source_src(foldExist+ModeleEn[idmod]+"_recap_accessibility.shp") 
       
#     ArrayToGtiff(Diff,Rspace_s+"Recap",Extent,nrows,ncols,Csize,road_network_proj,0,raster_type='UINT8')
#     del DExist,DProj,Diff2,Diff
#     gc.collect()
    
#     labelFR = ["","0_Nouvellement accessible","4_Distance raccourcie de 1 a 500m",
#                "3_Distance raccourcie de 500 a 999m", "2_Distance raccourcie de 1000 a 1499m",
#                "1_Distance raccourcie d'au moins 1500m"]
#     labelEN = ["","0_New accessible area","4_Shortened distance from 1 to 500m",
#                "3_Shortened distance from 500 to 999m", "2_Shortened distance from 1000 to 1499m",
#                "1_Shortened distance of at least 1500m"]
    
#     ds = gdal.Open(Rspace_s+"Recap.tif")
#     srcband = ds.GetRasterBand(1)
#     drv = ogr.GetDriverByName("ESRI Shapefile")
#     dst_ds = drv.CreateDataSource( Rspace_s+shpname+".shp")
#     dst_layer = dst_ds.CreateLayer(shpname, source_src , ogr.wkbPolygon)
#     raster_field = ogr.FieldDefn('Class', ogr.OFTInteger)
#     dst_layer.CreateField(raster_field)
#     gdal.Polygonize( srcband, None, dst_layer, 0, [], callback=None)
#     ds = None
#     raster_field = ogr.FieldDefn('Cat', ogr.OFTString)
#     dst_layer.CreateField(raster_field)
#     for feat in dst_layer:
#         i=feat.GetField("Class")  
#         if i==0:
#             dst_layer.DeleteFeature(feat.GetFID())
#         if language=="FR":
#             feat.SetField('Cat',labelFR[i])
#         else:
#             feat.SetField('Cat',labelEN[i])
#         dst_layer.SetFeature(feat)
#     # Cleanup
#     dst_ds.Destroy()    
    
#     os.remove(Rspace_s+"Recap.tif")
    
    
#     #Tableau des differences   
#     TabExist = np.loadtxt(foldExist+filename,dtype='|U39',delimiter=";")
#     TabProj = np.loadtxt(foldProj+filename,dtype='|U39',delimiter=";")
    
#     if TabExist.shape[1]<6:
#         Table = np.empty((TabExist.shape[0]-4+1,3),dtype='|U39')
#         withvol=0
#         col_list = [1,2]
#         j_corresp = [1,2]
#     else:
#         Table = np.empty((TabExist.shape[0]-4+1,5),dtype='|U39')
#         withvol=1
#         col_list = [1,2,4]
#         j_corresp = [1,2,3]
          
#     sumsurf = 0
#     sumvol = 0
#     sufprop = 0
#     propvol = 0
    
#     Table[:-1,0]=TabProj[:-4,0]
#     Table[0,1]="Difference Surface (ha)"
#     Table[0,2]="Difference Surface (%)"
        
#     for idj,j in enumerate(col_list):
#         for i in range(1,Table.shape[0]-3):            
#             Table[i,j_corresp[idj]]=str(round(float(TabProj[i,j])-float(TabExist[i,j]),1))  
#             if j==1:                
#                 sumsurf += float(TabProj[i,j])-float(TabExist[i,j]) 
#             if j==2:
#                 sufprop += float(TabProj[i,j])-float(TabExist[i,j]) 
#             if j==4:
#                 sumvol += float(TabProj[i,j])-float(TabExist[i,j])
    
#     Table[-2,1:3]=round(sumsurf,1),round(sufprop,1)
                                        
#     if withvol:
#         percvol = float(TabProj[i,5])/float(TabProj[i,6])
#         for i in range(1,Table.shape[0]-3):
#             Table[i,4]=str(round(float(Table[i,3])/percvol,1)) 
#             propvol += float(Table[i,3])/percvol
#         Table[0,3]="Difference Volume (m3)"
#         Table[0,4]="Difference Volume (%)"
#         Table[-2,3:]=int(sumvol),round(propvol,1)
    
#     if language=="FR":
#         Table[-2,0]="Total foret accessible supplementaire"
#         Table[-1,0]="Surface de foret impactee"
#     else:
#         Table[-2,0]="Total supplementary forest area"
#         Table[-1,0]="Impacted forest area"
#     Table[-1,1:3]=Surf_impact,round(Surf_impact*sufprop/sumsurf,1)   
#     np.savetxt(txtname, Table,fmt='%s', delimiter=';') 

def make_dif_files(language,Rspace,idmod):#idmod 0 : Skidder, 1 : Porteur
    ModeleFr = ["Skidder","Porteur"]
    ModeleEn = ["Skidder","Forwarder"]
    
    if language=="FR":
        Rspace_s = Rspace+ModeleFr[idmod]+"/"
        filename = "Resume_resultats_Sylvaccess_"+ModeleFr[idmod]+".txt"
        rastname = "Distance_totale_foret_route_forestiere.tif"
        foldExist = Rspace_s+"1_Existant/"
        foldProj = Rspace_s+"2_Projet/"
        txtname = Rspace_s+"Comparaison_avant_apres_projet.txt"
        shpname = "Surface_impactee"
    else:
        Rspace_s = Rspace+ModeleEn[idmod]+"/"
        filename = "Summary_results_Sylvaccess_"+ModeleEn[idmod]+".txt"
        rastname = "Total_distance.tif"
        foldExist = Rspace_s+"1_Existing/"
        foldProj = Rspace_s+"2_Project/"
        txtname = Rspace_s+"Comparison_before_after_project.txt"
        shpname = "Impacted_area"
        
        
    #Shapefile des differences
    names,values,proj,Extent = raster_get_info(foldExist+rastname)
    Csize = values[4]
    DExist =  load_float_raster_simple(foldExist+rastname)
    DProj =  load_float_raster_simple(foldProj+rastname)
    nrows,ncols = DProj.shape
    Diff2 = DExist-DProj
    Diff2[DExist==-9999]=0
    Diff2[DProj==-9999]=0
    Diff = np.zeros_like(DExist,dtype=np.int8)
    Diff[((DExist==-9999)*(DProj>=0))>0] = 1 #Nouvel accessible
    Diff[((Diff2>0)*(Diff2<500))>0] = 2 
    Diff[((Diff2>500)*(Diff2<1000))>0] = 3
    Diff[((Diff2>1000)*(Diff2<1500))>0] = 4
    Diff[Diff2>1500] = 5
    Surf_impact = round(np.sum(Diff>0)*Csize*Csize/10000,1)
    
    if language=="FR":
        road_network_proj=get_proj_from_road_network(foldExist+ModeleFr[idmod]+"_recap_accessibilite.shp")
        source_src=get_source_src(foldExist+ModeleFr[idmod]+"_recap_accessibilite.shp") 
    else:
        road_network_proj=get_proj_from_road_network(foldExist+ModeleEn[idmod]+"_recap_accessibility.shp")
        source_src=get_source_src(foldExist+ModeleEn[idmod]+"_recap_accessibility.shp") 
       
    ArrayToGtiff(Diff,Rspace_s+"Recap",Extent,nrows,ncols,Csize,road_network_proj,0,raster_type='UINT8')
    del DExist,DProj,Diff2,Diff
    gc.collect()
    
    labelFR = ["","0_Nouvellement accessible","4_Distance raccourcie de 1 a 500m",
               "3_Distance raccourcie de 500 a 999m", "2_Distance raccourcie de 1000 a 1499m",
               "1_Distance raccourcie d'au moins 1500m"]
    labelEN = ["","0_New accessible area","4_Shortened distance from 1 to 500m",
               "3_Shortened distance from 500 to 999m", "2_Shortened distance from 1000 to 1499m",
               "1_Shortened distance of at least 1500m"]
    
    ds = gdal.Open(Rspace_s+"Recap.tif")
    srcband = ds.GetRasterBand(1)
    drv = ogr.GetDriverByName("ESRI Shapefile")
    dst_ds = drv.CreateDataSource( Rspace_s+shpname+".shp")
    dst_layer = dst_ds.CreateLayer(shpname, source_src , ogr.wkbPolygon)
    raster_field = ogr.FieldDefn('Class', ogr.OFTInteger)
    dst_layer.CreateField(raster_field)
    gdal.Polygonize( srcband, None, dst_layer, 0, [], callback=None)
    ds = None
    raster_field = ogr.FieldDefn('Cat', ogr.OFTString)
    dst_layer.CreateField(raster_field)
    for feat in dst_layer:
        i=feat.GetField("Class")  
        if i==0:
            dst_layer.DeleteFeature(feat.GetFID())
        if language=="FR":
            feat.SetField('Cat',labelFR[i])
        else:
            feat.SetField('Cat',labelEN[i])
        dst_layer.SetFeature(feat)
    # Cleanup
    dst_ds.Destroy()    
    
    os.remove(Rspace_s+"Recap.tif")
    
    
    #Tableau des differences   
    TabExist = np.loadtxt(foldExist+filename,dtype='|U39',delimiter=";")
    TabProj = np.loadtxt(foldProj+filename,dtype='|U39',delimiter=";")
    if TabExist.shape[1]<6:        
        colrecap=[1,2]       
    else:        
        colrecap=[1,2,5,6] 
    Table = np.empty((TabExist.shape[0]-3,TabExist.shape[1]),dtype='|U39')    
    Table[0:-3,0]=TabExist[0:-6,0]
    for i in range(1,Table.shape[1]):
        Table[0,i]=str("Diff. ")+TabExist[0,i]
    
    for i in range(1,TabExist.shape[0]-6):
        for j in range(1,Table.shape[1]):
            Table[i,j]=round(float(TabProj[i,j])-float(TabExist[i,j]),1)
    
    if language=="FR":
        Table[-2,0]="Total foret accessible supplementaire"
        Table[-1,0]="Surface de foret impactee"
    else:
        Table[-2,0]="Total supplementary forest area"
        Table[-1,0]="Impacted forest area"
            
    for i in colrecap:
        Table[-2,i]=round(np.sum(np.float32(Table[1:-3,i])),1)
        
    Table[-1,1:3]=Surf_impact,round(Surf_impact/float(TabExist[-1,1][:-3])*100,1)   
    np.savetxt(txtname, Table,fmt='%s', delimiter=';') 
    

def prepa_obstacle_skidder(Obstacles_directory,Extent,Csize,ncols,nrows,Rast_desserte_ok):
    """
    Create raster with where there are obstacles, 0 anywhere else   
    """
    liste_file = os.listdir(Obstacles_directory)
    liste_obs = []
    for files in liste_file:
        if files[-4:len(files)]=='.shp':liste_obs.append(Obstacles_directory+files)
    if len(liste_obs)>0:
        Obstacles_skidder = shapefile_obs_to_np_array(liste_obs,Extent,Csize)
        Temp = (Rast_desserte_ok>0)
        Obstacles_skidder[Temp] = 0
    else: Obstacles_skidder = np.zeros((nrows,ncols),dtype=np.int8)
    return Obstacles_skidder

def create_buffer_skidder(Csize,Dtreuil_max_up,Dtreuil_max_down):
    Lcote = max(Dtreuil_max_up,Dtreuil_max_down)+1.5*Csize
    xmin,xmax,ymin,ymax = -Lcote,Lcote,-Lcote,Lcote
    Buffer_cote = int((max(Dtreuil_max_up,Dtreuil_max_down)/Csize+1.5))
    Dir_list = range(0,360,1)
    Row_line = np.zeros((len(Dir_list),3*Buffer_cote),dtype=np.int16)
    Col_line = np.zeros((len(Dir_list),3*Buffer_cote),dtype=np.int16)
    D_line = np.ones((len(Dir_list),3*Buffer_cote),dtype=np.float)*-1
    Nbpix_line = np.zeros((len(Dir_list),),dtype=np.int16)    
    for az in Dir_list:
        #Fill line info
        X1,Y1,mask_arr=from_az_to_arr(xmin,xmax,ymin,ymax,Csize,Lcote,az)        
        inds=np.argwhere(mask_arr==1)-Buffer_cote
        mat = np.zeros((inds.shape[0],3))
        mat[:,:2] = inds
        mat[:,2] = Csize*np.sqrt(mat[:,0]**2+mat[:,1]**2)
        ind = np.lexsort((mat[:,1],mat[:,2]))
        mat = mat[ind]
        nb_pix=mat.shape[0]
        Row_line[az,0:nb_pix]=mat[:,0]
        Col_line[az,0:nb_pix]=mat[:,1]
        D_line[az,0:nb_pix]=mat[:,2]
        Nbpix_line[az] = nb_pix        
    NbpixmaxL = np.max(Nbpix_line)
    return Row_line[:,0:NbpixmaxL],Col_line[:,0:NbpixmaxL],D_line[:,0:NbpixmaxL],Nbpix_line


def create_access_shapefile(Dtotal,Rspace_s,zone_accessible,Foret,
                            HarvClass_list,language,road_network_proj,source_src,Csize,
                            Dir_temp,Extent,nrows,ncols,layer_name):
    Recap = np.copy(Foret)
    #0: no Forest
    #1: inaccessible forest
    #2: Non_buch
    #Then harvesting classes
    labelFR = ["Zone hors foret","Inaccessible","Zone non exploitable (pente trop elevee)"]
    labelEN = ["Non-forest area","Inaccessible forest","Non harvestable (too steep slope)"]
    nbclass = len(HarvClass_list)    
    for i in range(1,nbclass):
        dmin,dmax = int(HarvClass_list[i-1]),int(HarvClass_list[i])
        labelFR.append("Accessible - Classe de debardage "+str(i)+' : '+str(HarvClass_list[i-1])+" - "+str(HarvClass_list[i])+" m")
        labelEN.append("Accessible - Skidding class "+str(i)+': '+str(HarvClass_list[i-1])+" - "+str(HarvClass_list[i])+" m")
        Temp = ((Dtotal>=dmin)*(Dtotal<dmax)*(Foret==1))>0
        Recap[Temp]=2+i
    #add infinite distance class 
    dmin = int(HarvClass_list[nbclass-1])
    labelFR.append("Accessible - Classe de debardage "+str(nbclass)+" : > "+str(dmin)+" m")
    labelEN.append("Accessible - Skidding class "+str(nbclass)+": > "+str(dmin)+" m")
    Temp = ((Dtotal>=dmin)*(Foret==1))>0
    Recap[Temp]=2+nbclass
    #Get area too slopy for harvesting
    if language == 'FR':
        Temp = load_float_raster_simple(Rspace_s+'Pente_ok_buch.tif')
    else:
        Temp = load_float_raster_simple(Rspace_s+'Manual_harvesting.tif')
    Temp = ((Temp!=1)*(Foret==1))>0
    Recap[Temp]=2
    #Save as Gtiff
    ArrayToGtiff(Recap,Dir_temp+"Recap",Extent,nrows,ncols,Csize,road_network_proj,0,raster_type='UINT8')
    #Convert to shape
    type_mapping = { gdal.GDT_Byte: ogr.OFTInteger,
                 gdal.GDT_UInt16: ogr.OFTInteger,   
                 gdal.GDT_Int16: ogr.OFTInteger,    
                 gdal.GDT_UInt32: ogr.OFTInteger,
                 gdal.GDT_Int32: ogr.OFTInteger,
                 gdal.GDT_Float32: ogr.OFTReal,
                 gdal.GDT_Float64: ogr.OFTReal,
                 gdal.GDT_CInt16: ogr.OFTInteger,
                 gdal.GDT_CInt32: ogr.OFTInteger,
                 gdal.GDT_CFloat32: ogr.OFTReal,
                 gdal.GDT_CFloat64: ogr.OFTReal}

    ds = gdal.Open(Dir_temp+"Recap.tif")
    srcband = ds.GetRasterBand(1)
    drv = ogr.GetDriverByName("ESRI Shapefile")
    dst_ds = drv.CreateDataSource( Rspace_s+layer_name+".shp" )
    dst_layer = dst_ds.CreateLayer(layer_name, source_src , ogr.wkbPolygon)
    raster_field = ogr.FieldDefn('Class', type_mapping[srcband.DataType])
    dst_layer.CreateField(raster_field)
    gdal.Polygonize( srcband, None, dst_layer, 0, [], callback=None)
    ds.FlushCache()
    raster_field = ogr.FieldDefn('Cat', ogr.OFTString)
    dst_layer.CreateField(raster_field)
    for feat in dst_layer:
        i=feat.GetField("Class")  
        if i==0:
            dst_layer.DeleteFeature(feat.GetFID())
        if language=="FR":
            feat.SetField('Cat',labelFR[i])
        else:
            feat.SetField('Cat',labelEN[i])
        dst_layer.SetFeature(feat)
    # Cleanup
    dst_ds.Destroy()
    
def create_arrays_from_roads(source_shapefile,Extent,Csize):
    """
    Select a part of the shapefile and make new shapefile with it
    ----------
    Parameters
    ----------
    source_shapefile:    string      Complete name of the source shapefile
    out_Shape_Path:      string      Complete name of the output shapefile
    expression:          string      SQL expression used for the selection
    -------
    Examples
    --------
    >>> import ogr,gdal
    >>> select_in_shapefile("Desserte.shp","Route.shp",'WHERE Importance=2')
    """
    #Recupere les dimensions du raster ascii
    xmin,xmax,ymin,ymax = Extent[0],Extent[1],Extent[2],Extent[3]
    nrows,ncols = int((ymax-ymin)/float(Csize)+0.5),int((xmax-xmin)/float(Csize)+0.5)    
    #Recupere le driver
    driver = ogr.GetDriverByName('ESRI Shapefile')
    #Get information from source shapefile
    source_ds = ogr.Open(source_shapefile)
    source_layer = source_ds.GetLayer()    
    source_srs = source_layer.GetSpatialRef()
    source_type = source_layer.GetGeomType()
    source_srs = source_layer.GetSpatialRef()
    ###################################################
    ### Reseau public    
    ###################################################
    expression = '"CL_SVAC" = 3'
    source_layer_bis = source_ds.ExecuteSQL('SELECT * FROM '+str(source_layer.GetName())+' WHERE '+expression) 
    # Initialize the new memory raster
    driver = ogr.GetDriverByName('Memory')
    target_ds = driver.CreateDataSource("test")
    layerName = "test"
    layer = target_ds.CreateLayer(layerName, source_srs, source_type)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('Route', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind=0
    for feat in source_layer_bis:
        geometry = feat.GetGeometryRef()
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(geometry)
        feature.SetFID(ind)
        feature.SetField('Route',1)
        # Save feature
        layer.CreateFeature(feature)
        # Cleanup
        feature.Destroy()
        ind +=1
    # Cleanup
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)    
    target_ds2 = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
    target_ds2.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds2.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds2.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds2, [maskvalue], layer,options=["ATTRIBUTE=Route","ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds2.FlushCache()
        Res_pub = np.int8(target_ds2.GetRasterBand(1).ReadAsArray())
    target_ds.Destroy()
    target_ds2.FlushCache()
    ###################################################
    ### Route forestiere  
    ###################################################
    expression = '"CL_SVAC" = 2'
    source_layer_bis = source_ds.ExecuteSQL('SELECT * FROM '+str(source_layer.GetName())+' WHERE '+expression) 
    # Initialize the new memory raster
    driver = ogr.GetDriverByName('Memory')
    target_ds = driver.CreateDataSource("test")
    layerName = "test"
    layer = target_ds.CreateLayer(layerName, source_srs, source_type)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('Route', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind=0
    for feat in source_layer_bis:
        geometry = feat.GetGeometryRef()
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(geometry)
        feature.SetFID(ind)
        feature.SetField('Route',1)
        # Save feature
        layer.CreateFeature(feature)
        # Cleanup
        feature.Destroy()
        ind +=1
    # Cleanup
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)    
    target_ds2 = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
    target_ds2.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds2.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds2.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds2, [maskvalue], layer,options=["ATTRIBUTE=Route","ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds2.FlushCache()
        Route_For = np.int8(target_ds2.GetRasterBand(1).ReadAsArray())
    target_ds.Destroy()
    target_ds2.FlushCache()
    ###################################################
    ### Piste forestiere    
    ###################################################
    expression = '"CL_SVAC" = 1'
    source_layer_bis = source_ds.ExecuteSQL('SELECT * FROM '+str(source_layer.GetName())+' WHERE '+expression) 
    # Initialize the new memory raster
    driver = ogr.GetDriverByName('Memory')
    target_ds = driver.CreateDataSource("test")
    layerName = "test"
    layer = target_ds.CreateLayer(layerName, source_srs, source_type)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('Route', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind=0
    for feat in source_layer_bis:
        geometry = feat.GetGeometryRef()
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(geometry)
        feature.SetFID(ind)
        feature.SetField('Route',1)
        # Save feature
        layer.CreateFeature(feature)
        # Cleanup
        feature.Destroy()
        ind +=1
    # Cleanup
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)    
    target_ds2 = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
    target_ds2.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds2.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds2.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds2, [maskvalue], layer,options=["ATTRIBUTE=Route","ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds2.FlushCache()
        Piste = np.int8(target_ds2.GetRasterBand(1).ReadAsArray())
    target_ds.Destroy()
    target_ds2.FlushCache()
    source_ds.Destroy()    
    return Res_pub,Route_For,Piste

